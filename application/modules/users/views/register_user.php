<?php $this->load->view("header"); ?>

<div style="padding-top: 100px;">
    <div class="container">
        <div class="row centered">
            <div class="col-md-12">
                <form class="" action="user_register" method="post" enctype="multipart/form-data">
                    <h1 class="page_title"><i><img src="<?= IMG ?>header_wrap_icon.png"></i>أضف حسابك</h1>
                    <p class="page_desc">قم بتعبئة البيانات التالية بدقة واحصل على حساب مجاني في العروسة وادخل مباشرةً
                        إلى لوحة التحكم الخاصة بك وأضف المعلومات المطلوبة لتفعيل حسابك</p>
                    <?php if (isset($error)):
                        echo $error;
                    endif;
                    ?>
                    <div class="user_data">
                        <h1 class="page_title">أضف صورة شخصية</h1>
                        <div style="position: relative;height: 200px;" class="div_profile">
                            <img src="<?= IMG ?>avatar.png" style="position: absolute" id="profile"/>
                            <input type="file" name="profile" id="profilePhoto"
                                   style="opacity: 0; position: absolute; height: 200px;cursor: pointer; " required>
                        </div>
                        <div class="form_data">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">الإسم</label>
                                    <input type="text" name="name" value="<?= @htmlspecialchars(trim($_POST['name'])) ?>" class="form-control" required autofocus/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">البريد الإلكتروني</label>
                                    <input type="email" name="email" value="<?= @htmlspecialchars(trim($_POST['email'])) ?>" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">اسم المستخدم</label>
                                    <input type="text" name="username" value="<?= @htmlspecialchars(trim($_POST['username'])) ?>" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">الرقم السرى</label>
                                    <input type="password" name="password" class="form-control password1" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" id="confirm_password">
                                    <label for="name">تاكيد الرقم السرى</label>
                                    <div class="alert alert-danger" id="message_error" style="display: none">
                                        <strong>خطاء!</strong> الرقم التاكيدي غلط.
                                    </div>
                                    <input type="password" name="password" class="form-control password2 " required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">رقم الهاتف</label>

                                    <input type="tel" name="tel" value="<?= @htmlspecialchars(trim($_POST['tel'])) ?>" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">حسابات التواصل الاجتماعي</label>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" value="<?= @htmlspecialchars(trim($_POST['social_facebook'])) ?>" name="social_facebook" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-facebook fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" value="<?= @htmlspecialchars(trim($_POST['social_twitter'])) ?>" name="social_twitter" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-twitter fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" value="<?= @htmlspecialchars(trim($_POST['social_linkedin'])) ?>" name="social_linkedin" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-linkedin fa-stack-1x"></i></span></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 select">
                                <div class="form-group">
                                    <label for="name">اختار البلد</label>
                                    <select class="form-control country_select" name="country_select">
                                        <option>البلد</option>
                                        <?php foreach ($countries as $key => $country): ?>

                                            <option value="<?= $country->country_id ?>" <?php if (@$_POST['country_select'] == $country->country_id) echo "selected" ?>><?= $country->country_name ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="city"></div>


                            <input type="submit" value="تسجيل مستخدم" class="registerInput">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(".file_button_container").click(function () {
        $("#hidden_upload_input").trigger('click');
    });

    $('#hidden_upload_input').on('change', function () {
        var val = $(this).val();
        $(this).siblings('#upload_file_name').text(val);
    })
</script>

<script>

    $(function () {
        // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $($.parseHTML('<img class="col-md-4">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#filePhoto').on('change', function () {
            imagesPreview(this, 'div.gallery');
        });
    });

    $(function () {
        // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $('#profile').attr('src', event.target.result).appendTo(placeToInsertImagePreview).css({
                            'height': '200px',
                            'width': '200px',
                            'border-radius': '200px'
                        });
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#profilePhoto').on('change', function () {
            imagesPreview(this, 'div.div_profile');
        });
    });

</script>

<script>
    $(function () {
        $('body').on('change', '.country_select', function () {
            var row_id = $(this).val();
            var data_ajax = {};
            data_ajax.country_id = row_id;
            var url = window.location.origin;
            $.ajax({
                url: '<?= site_url()?>getCity',
                type: 'GET',
                data: data_ajax,
                success: function (data) {
                    $('.city').html(data);
                }
            });
        })

        $('body').on('change', '.city_select', function () {
            var row_id = $(this).val();
            var data_ajax = {};
            data_ajax.city_id = row_id;
            var url = window.location.origin;
            $.ajax({
                url: '<?= site_url()?>getDistrict',
                type: 'GET',
                data: data_ajax,
                success: function (data) {
                    $('.district').html(data);
                }
            });
        })



        $('body').on('blur', '.password2', function () {
            if ($('.password1').val()!=$('.password2').val()) {
                $('.password2').focus()
                $('#confirm_password').addClass('has-error')
                $('#message_error').show()
            }
            else {
                $('#confirm_password').removeClass('has-error')
                $('#message_error').hide();
            }


        })

    });
</script>


<?php $this->load->view("footer"); ?>
