-- phpMyAdmin SQL Dump
-- version 4.3.8
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 05, 2017 at 04:44 PM
-- Server version: 5.6.32-78.1-log
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `elthraro_elarousa`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(128) NOT NULL,
  `category_image` varchar(256) DEFAULT NULL,
  `category_parent` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_image`, `category_parent`) VALUES
(1, 'مصور فوتوغرافي وفيديو', '1498663483.jpg', 0),
(2, 'خبير تجميل', '1498672451.jpg', 0),
(3, 'مراكز تجميل و سبا', '1498672419.jpg', 0),
(4, 'منظم حفلات', '1498669805.jpg', 0),
(5, 'مصفف شعر و قص و صبغه', '1498671176.jpg', 0),
(6, 'محلات ورود', '1498669751.jpg', 0),
(7, 'فساتين الافراح', '1498672509.jpg', 0),
(8, 'ليله الحنه', '1498672907.jpg', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `country_id`, `city_name`) VALUES
(1, 1, 'القاهرة'),
(2, 1, 'الجيزة'),
(5, 1, 'المنيا'),
(6, 1, 'الإسكندرية'),
(7, 1, 'الإسماعيلية'),
(8, 1, 'أسوان'),
(9, 1, 'أسيوط'),
(10, 1, 'الأقصر'),
(11, 1, 'الغردقة'),
(12, 1, 'دمنهور'),
(13, 1, 'بنى سويف'),
(14, 1, 'بورسعيد'),
(15, 1, 'الطور'),
(16, 1, 'المنصورة'),
(17, 1, 'دمياط'),
(18, 1, 'سوهاج'),
(19, 1, 'السويس'),
(20, 1, 'الزقازيق'),
(21, 1, 'العريش'),
(22, 1, 'طنطا'),
(23, 1, 'الفيوم'),
(24, 1, 'بنها'),
(25, 1, 'قنا'),
(26, 1, 'كفر الشيخ'),
(27, 1, 'مرسى مطروح'),
(28, 1, 'شبين الكوم'),
(29, 1, 'الخارجة');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE IF NOT EXISTS `company` (
  `comp_id` int(11) NOT NULL,
  `facebook_id` varchar(300) NOT NULL,
  `comp_name` varchar(200) NOT NULL,
  `comp_email` varchar(200) NOT NULL,
  `comp_image` varchar(200) NOT NULL,
  `comp_slider` text NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `comp_country_id` int(11) NOT NULL,
  `comp_city_id` int(11) NOT NULL,
  `comp_district_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `comp_des` text NOT NULL,
  `working_hours` text,
  `comp_tel` varchar(15) NOT NULL,
  `comp_facebook` varchar(200) NOT NULL,
  `comp_twitter` varchar(200) NOT NULL,
  `comp_linkedin` varchar(200) NOT NULL,
  `comp_pinterest` varchar(256) DEFAULT NULL,
  `comp_active` int(11) NOT NULL DEFAULT '0',
  `comp_rate` int(11) NOT NULL DEFAULT '0',
  `is_sent_verification` tinyint(1) NOT NULL COMMENT 'هل تم إرسال verification من قبل ليه',
  `is_verified` tinyint(1) NOT NULL,
  `verification_token` text NOT NULL,
  `login_provider` varchar(100) NOT NULL,
  `lat` varchar(300) NOT NULL,
  `lng` varchar(300) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`comp_id`, `facebook_id`, `comp_name`, `comp_email`, `comp_image`, `comp_slider`, `category_id`, `comp_country_id`, `comp_city_id`, `comp_district_id`, `username`, `password`, `comp_des`, `working_hours`, `comp_tel`, `comp_facebook`, `comp_twitter`, `comp_linkedin`, `comp_pinterest`, `comp_active`, `comp_rate`, `is_sent_verification`, `is_verified`, `verification_token`, `login_provider`, `lat`, `lng`) VALUES
(18, '', 'Sally rabie ', 'sallyashraf86@hotmail.com', '36f91da77aa9372a59ee3aa828716ecf.jpg', '', 2, 1, 2, 0, 'Sally rabie', '654230d8350051026a9eb24214d6d2f69f469e9958631941b2346a7c8f1386c7c04ccbc7a082a7912e166ccb2bb4214ca17578d8dc93ea19a0b4e30abee25f19', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"1-pm","timw1_from2":"6-pm","timw2_from1":"7-pm","timw2_from2":"10-pm"}', '01009828828', 'https://www.facebook.com/sallyrabiemakeup/', '', 'https://instagram.com/download/?r=46268069', '', 1, 5, 1, 1, '3f319b45c6f23e562350230a89a093a8', '', '29.980271291784238', '31.320703849196434'),
(20, '', 'NaDa Ismaiel', 'butterfly010.ni@gmail.com', 'company_1497664477_1744106518', '', 2, 1, 2, 1, 'NaDa Ismaiel', '4f21aa9624501e242f0df9613c3a38e05416329bbb68b7bc91060cfad05217d2c7fb93d534f0100be9c38a21729cbcf0cf905f079dc43c35f6e93cfe16b425bc', '', '{"days":["\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621"],"timw1_from1":"2-pm","timw1_from2":"6-pm","timw2_from1":"3-am","timw2_from2":"1-am"}', '01005832121', 'https://www.facebook.com/Nadaismaelmorsy/', '', '', NULL, 1, 0, 0, 1, '', '', '29.9763158', '30.948191899999983'),
(22, '', 'test provider', 'remo.alking@gmail.com3', 'company_1498664334_2133936712.jpg', '', 1, 1, 1, 2, 'remon3', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '', '{"days":["\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621"],"timw1_from1":"2-pm","timw1_from2":"6-pm","timw2_from1":"1-am","timw2_from2":"1-am"}', '01093237545', '', '', '', '', 1, 0, 1, 1, '123220c0c42f8e92cb8da565b746a974', '', '29.9809061', '31.318960899999997'),
(23, '', 'NaDa Ismaiel', 'butterfly010@live.co.uk', 'company_1499014198_1617286345.jpg', '', 2, 1, 2, 0, 'NaDa Ismaiel makeup artist', '4f21aa9624501e242f0df9613c3a38e05416329bbb68b7bc91060cfad05217d2c7fb93d534f0100be9c38a21729cbcf0cf905f079dc43c35f6e93cfe16b425bc', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621","\\u0627\\u0644\\u0627\\u0631\\u0628\\u0639\\u0627\\u0621","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"12-pm","timw1_from2":"4-pm","timw2_from1":"4-pm","timw2_from2":"8-pm"}', '01005832121', 'https://www.facebook.com/Nadaismaelmorsy/', '', '', '', 0, 0, 1, 0, '41f0a6ce375c7b4cc8f04a1892cce503', '', '29.9763158', '30.948191899999983'),
(24, '', 'Sally', 'sallyashraf86@gmail.com', 'company_1499200782_143532909.jpg', '', 2, 1, 2, 0, 'Sally ashraf', '654230d8350051026a9eb24214d6d2f69f469e9958631941b2346a7c8f1386c7c04ccbc7a082a7912e166ccb2bb4214ca17578d8dc93ea19a0b4e30abee25f19', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621","\\u0627\\u0644\\u0627\\u0631\\u0628\\u0639\\u0627\\u0621","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"1-am","timw1_from2":"3-am","timw2_from1":"7-am","timw2_from2":"10-am"}', '01093915000', '', '', '', '', 0, 0, 1, 0, '81b72befcf75f22d45e6bb009eb0794d', '', '29.9809061', '31.318960899999997'),
(25, '', 'Nada ismaiel', 'butterfly010.ny@gmail.com', 'company_1499201539_1440397242.jpg', '', 2, 1, 2, 0, 'Nada Ismail', '654230d8350051026a9eb24214d6d2f69f469e9958631941b2346a7c8f1386c7c04ccbc7a082a7912e166ccb2bb4214ca17578d8dc93ea19a0b4e30abee25f19', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621","\\u0627\\u0644\\u0627\\u0631\\u0628\\u0639\\u0627\\u0621","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"3-am","timw1_from2":"5-am","timw2_from1":"6-am","timw2_from2":"7-am"}', '01005832121', '', '', '', '', 1, 0, 1, 1, '98955c822460071d1f3114c850c382a2', '', '29.977631962025598', '31.321476325392723'),
(26, '', 'donia aboud', 'donia.sameh31@hotmail.com', '', '', 2, 1, 1, 0, 'donia', '1959396d049cbc2ae6b48405b090799e44a2d63c3323554c4e35fef14fd75f3e242ce789838eada935cea0b832ffb1488fa2f62c3c459268c12d0c826e543e4b', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621","\\u0627\\u0644\\u0627\\u0631\\u0628\\u0639\\u0627\\u0621","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"","timw1_from2":"","timw2_from1":"","timw2_from2":""}', '01158832283', '', '', '', '', 1, 0, 1, 0, 'ec9322b0cc31c2d613455d35c8555135', '', '29.979193264158635', '31.320624388754368'),
(27, '', 'Rana alaa', 'rooka_94@hotmail.com', '', '', 2, 1, 2, 0, 'Rana', '5da568ffb6e614af588f2e5de809d3988341e8c533d8d74133843f15b246a676eecedf78f8c44c3860aedb04e2e6ae5c937a6960fbea71bd47bd2abdb01cf180', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621","\\u0627\\u0644\\u0627\\u0631\\u0628\\u0639\\u0627\\u0621","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"12-am","timw1_from2":"9-pm","timw2_from1":"12-am","timw2_from2":"9-pm"}', '01100544494', 'https://m.facebook.com/Rana-alaa-makeup-artist-veil-designer-925061510874828/', '', '', '', 1, 0, 1, 0, 'e8ef8ac745b0263fd936f0c97d36a025', '', '29.97884011463346', '31.32128957659006'),
(34, '', 'remon adel', 'remo.alking@gmail.com', 'company_1499230884_752187799.jpg', '', 1, 1, 0, 0, 'remon', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '', '{"days":["\\u0627\\u0644\\u0633\\u0628\\u062a","\\u0627\\u0644\\u0627\\u062d\\u062f","\\u0627\\u0644\\u0627\\u062b\\u0646\\u064a\\u0646","\\u0627\\u0644\\u062b\\u0644\\u0627\\u062b\\u0627\\u0621","\\u0627\\u0644\\u0627\\u0631\\u0628\\u0639\\u0627\\u0621","\\u0627\\u0644\\u062e\\u0645\\u064a\\u0633","\\u0627\\u0644\\u062c\\u0645\\u0639\\u0647"],"timw1_from1":"3-am","timw1_from2":"3-am","timw2_from1":"10-am","timw2_from2":"11-am"}', '01118643373', '', '', '', '', 1, 0, 1, 1, '16f807342d6738e2b9047b3cd3be869a', '', '29.9809061', '31.318960899999997');

-- --------------------------------------------------------

--
-- Table structure for table `company_portofolio`
--

CREATE TABLE IF NOT EXISTS `company_portofolio` (
  `img_id` int(11) NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `img_title` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `company_portofolio`
--

INSERT INTO `company_portofolio` (`img_id`, `img_name`, `comp_id`, `img_title`) VALUES
(4, 'company_1496761293_359411593.jpg', 19, 0),
(5, 'company_1496761293_1078346978.JPG', 19, 0),
(6, '44c47e8202b7a74b4244e5d7784e87cc.jpg', 19, 0),
(7, '6e69cacb44c8225ba62608b1fa5dbcde.jpg', 18, 0),
(8, '3ddf294e15faefd96cbe8b6da721b024.jpg', 18, 0),
(9, '89c5440bdbca3a5fa1d14fad2cfecbbe.jpg', 18, 0),
(10, '78b6b3c7b25de24cdc025e0706bd8ae6.jpg', 18, 0),
(15, '64d97412718866663b4806503a619a14.JPG', 13, 0),
(16, 'bac3d75d28f81b620f4cddb38a282d7c.JPG', 13, 0),
(17, '0', 20, 0),
(18, 'company_1497993406_2029778917.png', 21, 0),
(19, 'company_1497993406_1112348767.png', 21, 0),
(20, 'company_1497993406_30701294.png', 21, 0),
(21, 'company_1497993406_314636562.png', 21, 0),
(22, 'company_1498664334_1782013912.jpg', 22, 0),
(23, 'company_1498664334_666648463.jpg', 22, 0),
(24, 'company_1498664334_1208604831.jpg', 22, 0),
(25, 'company_1499014198_892803547.jpg', 23, 0),
(26, 'company_1499200782_2042845145.jpg', 24, 0),
(27, 'company_1499201539_557750673.jpg', 25, 0),
(28, '<p>The filetype you are attempting to upload is not allowed.</p>', 26, 0),
(29, 'company_1499206713_1244271466.jpg', 28, 0),
(30, 'company_1499206713_1307906390.JPG', 28, 0),
(31, 'company_1499208886_395355075.jpg', 29, 0),
(32, 'company_1499208886_1411562626.JPG', 29, 0),
(33, 'company_1499210361_1152257749.jpg', 30, 0),
(34, 'company_1499210361_1005009623.JPG', 30, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company_services`
--

CREATE TABLE IF NOT EXISTS `company_services` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL,
  `service_price` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=174 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_services`
--

INSERT INTO `company_services` (`id`, `company_id`, `service_id`, `service_price`) VALUES
(1, 12, 1, 0),
(3, 14, 1, 0),
(4, 15, 1, 0),
(45, 19, 1, 0),
(46, 19, 4, 0),
(79, 13, 1, 0),
(132, 18, 4, 550),
(133, 18, 5, 1850),
(134, 18, 6, 3000),
(135, 23, 4, 0),
(136, 23, 5, 0),
(137, 23, 6, 0),
(138, 24, 4, 30),
(139, 24, 5, 40),
(140, 24, 6, 50),
(141, 25, 2, 40),
(142, 25, 3, 50),
(143, 25, 4, 30),
(144, 26, 4, 0),
(145, 26, 5, 0),
(146, 26, 6, 0),
(147, 27, 1, 0),
(148, 27, 2, 0),
(149, 27, 3, 0),
(150, 27, 4, 0),
(151, 27, 5, 0),
(152, 27, 6, 0),
(153, 27, 7, 0),
(154, 27, 8, 0),
(155, 27, 9, 0),
(156, 27, 10, 0),
(157, 27, 11, 0),
(158, 27, 13, 0),
(159, 27, 14, 0),
(160, 27, 15, 0),
(161, 27, 16, 0),
(162, 27, 17, 0),
(163, 27, 18, 0),
(164, 28, 1, 100),
(165, 28, 3, 500),
(166, 29, 5, 50),
(167, 29, 7, 500),
(168, 30, 5, 50),
(169, 30, 7, 500),
(170, 33, 1, 10),
(171, 33, 3, 1110),
(172, 34, 1, 10),
(173, 34, 3, 1110);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(200) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`) VALUES
(1, 'مصر'),
(2, 'السعودية'),
(3, 'الإمارات'),
(4, 'الأردن'),
(5, 'البحرين');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE IF NOT EXISTS `district` (
  `district_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`district_id`, `city_id`, `district_name`) VALUES
(2, 1, 'الدقي2'),
(3, 1, 'المعادى'),
(4, 1, 'السادس من أكتوبر - الحصرى'),
(5, 1, 'السادس من أكتوبر - طريق الواحات'),
(6, 1, 'العاشر من رمضان'),
(7, 1, '15 مايو'),
(8, 1, 'العباسية'),
(9, 1, 'أبو رواش'),
(10, 1, 'العجوزة'),
(11, 1, 'عين شمس'),
(12, 1, 'المنيل'),
(13, 1, 'أرض الجولف'),
(14, 1, 'مطار القاهرة'),
(15, 1, 'الدقي'),
(16, 1, 'وسط البلد'),
(17, 1, 'الضاهر'),
(18, 1, 'القطامية'),
(19, 1, 'مدينة العبور'),
(20, 1, 'مدينة الرحاب'),
(21, 1, 'الشيخ زايد'),
(22, 1, 'مدينة الشروق'),
(23, 1, 'الزيتون'),
(24, 1, 'فيصل'),
(25, 1, 'جاردن سيتي'),
(26, 1, 'الجيزة'),
(27, 1, 'حدائق الاهرام'),
(28, 1, 'حدائق القبة'),
(29, 1, 'الهرم'),
(30, 1, 'مصر الجديدة'),
(31, 1, 'حلوان'),
(32, 1, 'كوبري القبة'),
(33, 1, 'المعادي'),
(34, 1, 'مدينتي'),
(35, 1, 'مساكن شيراتون'),
(36, 1, 'المطرية'),
(37, 1, 'مصر القديمة'),
(38, 1, 'المهندسين'),
(39, 1, 'المقطم'),
(40, 1, 'مدينة ناصر'),
(41, 1, 'النزهة الجديدة'),
(42, 1, 'بيراميدز، هيتس'),
(43, 1, 'شبرا'),
(44, 1, 'شبرا الخيمة'),
(45, 1, 'التجمع الاول'),
(46, 1, 'التجمع الثالث'),
(47, 1, 'التجمع الخامس'),
(48, 1, 'الزمالك'),
(49, 6, 'العجمى'),
(50, 6, 'العصافرة'),
(51, 6, 'الأزاريطة'),
(52, 6, 'باب شرق'),
(53, 1, 'بيكوس'),
(54, 6, 'بولكلي'),
(55, 6, 'كامب شيزار'),
(56, 6, 'كارفور'),
(57, 6, 'كليوباترا'),
(58, 6, 'جليم'),
(59, 6, 'الإبراهيمية'),
(60, 6, 'كفر عبده'),
(61, 6, 'لوران'),
(62, 6, 'المعمورة'),
(63, 6, 'المندرة'),
(64, 6, 'المنشية'),
(65, 6, 'ميامي'),
(66, 6, 'محرم بك'),
(67, 6, 'المنتزه'),
(68, 6, 'مصطفى كامل'),
(69, 6, 'محطة الرمل'),
(70, 6, 'رشدي'),
(71, 6, 'سان ستيفانو'),
(72, 6, 'سموحة'),
(73, 6, 'الشاطبي'),
(74, 6, 'سيدي بشر'),
(75, 6, 'ستانلي'),
(76, 6, 'سيدي جابر'),
(77, 9, 'شارع الجمهورية'),
(78, 9, 'مدينة المعلمين'),
(79, 9, 'شارع النعيم'),
(80, 9, 'كورنيش النيل - الثورة'),
(81, 9, 'مدينة مبارك'),
(82, 9, 'شارع يسرى راغب'),
(83, 11, 'منطقة عربية'),
(84, 11, 'منطقة الأحيا'),
(85, 11, 'منطقة الدهار'),
(86, 11, 'الهضبة'),
(87, 11, 'منطقة الهلال'),
(88, 11, 'الكوثار'),
(89, 11, 'شارع المدارس'),
(90, 11, 'الممشة السياحي'),
(91, 11, 'المركز السياحي'),
(92, 11, 'ميدان الصقالة'),
(93, 11, 'مناطق مبارك (1-8)'),
(94, 11, 'شيري ست'),
(95, 11, 'طريق شيرتون'),
(96, 16, 'شارع أحمد ماهر'),
(97, 16, 'شارع البحر'),
(98, 16, 'منطقة العيساوي'),
(99, 16, 'شارع الجيش'),
(100, 16, 'المجزر'),
(101, 16, 'جامعة المنصورة'),
(102, 16, 'المشيا'),
(103, 16, 'شارع الجلاء'),
(104, 16, 'شارع جيهان'),
(105, 16, 'مغماح المحكم'),
(106, 16, 'طريق بورسعيد'),
(107, 16, 'شارع سامية الجمل'),
(108, 16, 'ستاديوم-إل ميرور أريا'),
(109, 16, 'منطقة توريل'),
(110, 14, 'قضاء العرب'),
(111, 14, 'حي الدواحي'),
(112, 14, 'قضاء الجمهورية'),
(113, 14, 'حي المناخ'),
(114, 14, 'حي الشرق'),
(115, 14, 'حي الزهور'),
(116, 14, 'بور فؤاد'),
(117, 22, 'شارع الحلو'),
(118, 22, 'المحطة'),
(119, 22, 'شارع النادى'),
(120, 22, 'شارع النحاس'),
(121, 22, 'حسن رضوان'),
(122, 22, 'كفر عصام'),
(123, 22, 'شارع سعيد'),
(124, 22, 'منطقة الاستاد');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(300) NOT NULL,
  `page_slug` varchar(300) NOT NULL,
  `page_body` text NOT NULL,
  `page_meta_title` text NOT NULL,
  `page_meta_desc` text NOT NULL,
  `page_meta_keywords` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_title`, `page_slug`, `page_body`, `page_meta_title`, `page_meta_desc`, `page_meta_keywords`) VALUES
(1, 'عن الموقع', 'aboutus', '<p><strong>موقع &quot;العروسه &quot; أحد أكبر المواقع النسائية العربية التي تخاطب المرأة. ويهتم بكل ما يخص المرأة و مساعدتها في حجز كل ما تحتاجه من اجل جمالها و تالقها.</strong></p>\n\n<p>وما يميّز موقع العروسه هو سهوله البحث لكل ما تحتاجيه من أجل جمالك في معظم أنحاء الوطن العربي.</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n\n<p>&nbsp;</p>\n', 'عن الموقع', 'عن الموقع', 'العروسة،عن الموقع، about us'),
(3, 'اتصل بنا', 'contact', '<p><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />\n&nbsp;info@el3arousa.com &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />\nmarketing@el3arousa.com &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />\ninvestorrelation@el3arousa.com &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br />\nsales@el3arousa.com &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong></p>\n', 'اتصل بنا العروسة', 'اتصل بنا العروسة', 'العروسة،اتصل،اتصال'),
(4, 'TermsService', 'terms', '<p><strong>عن الموقععن الموقععن الموقععن الموقععن الموقععن الموقع</strong></p>\r\n', 'عن الموقععن الموقع4', 'عن الموقععن الموقععن الموقععن الموقع56', 'عن الموقععن الموقععن الموقععن الموقععن الموقع789'),
(5, 'Privacy Policy', 'privacy', '<p><strong>عن الموقععن الموقععن الموقععن الموقععن الموقععن الموقع</strong></p>\r\n', 'عن الموقععن الموقع4', 'عن الموقععن الموقععن الموقععن الموقع56', 'عن الموقععن الموقععن الموقععن الموقععن الموقع789'),
(6, 'Support & FAQ', 'support', '<p><strong>عن الموقععن الموقععن الموقععن الموقععن الموقععن الموقع</strong></p>\r\n', 'عن الموقععن الموقع4', 'عن الموقععن الموقععن الموقععن الموقع56', 'عن الموقععن الموقععن الموقععن الموقععن الموقع789');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE IF NOT EXISTS `rates` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `company_id`, `user_id`, `rate`) VALUES
(0, 18, 18, 5),
(1, 13, 13, 4),
(2, 12, 2, 4),
(3, 15, 2, 5),
(4, 14, 2, 4),
(5, 12, 13, 3),
(6, 14, 13, 2),
(7, 15, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE IF NOT EXISTS `reservations` (
  `res_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `services_id` varchar(256) DEFAULT NULL,
  `total_price` int(11) NOT NULL DEFAULT '0',
  `time` varchar(200) NOT NULL,
  `date` varchar(100) NOT NULL,
  `day` varchar(64) NOT NULL,
  `first_reminder` tinyint(4) NOT NULL DEFAULT '0',
  `second_reminder` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`res_id`, `user_id`, `comp_id`, `services_id`, `total_price`, `time`, `date`, `day`, `first_reminder`, `second_reminder`, `created_at`) VALUES
(17, 12, 12, NULL, 0, '1-am الى 2-am', '', '', 0, 0, '2017-05-27 00:37:35'),
(18, 12, 12, NULL, 0, '1-am الى 2-am', '', '', 0, 0, '2017-05-27 00:42:51'),
(19, 12, 12, NULL, 0, '1-am الى 2-am', '', '', 0, 0, '2017-05-27 00:42:55'),
(20, 12, 12, NULL, 0, '1-am الى 2-am', '', '', 0, 0, '2017-05-27 00:43:41'),
(21, 12, 12, NULL, 0, '1-am الى 2-am', '', '', 0, 0, '2017-05-27 00:44:33'),
(22, 12, 12, NULL, 0, '1-am الى 2-am', '', '', 0, 0, '2017-05-27 00:47:10'),
(23, 12, 12, NULL, 0, '1-am الى 2-am', '2017-05-18', '', 0, 0, '2017-05-27 16:19:14'),
(24, 2, 13, NULL, 0, 'السبت-1-am الى 2-am', '2017-06-08', '', 0, 0, '2017-06-05 13:57:56'),
(25, 2, 18, NULL, 0, 'السبت-8-am الى 9-pm', '2017-06-21', '', 0, 0, '2017-06-20 17:27:34'),
(26, 2, 15, NULL, 0, 'السبت-1-am الى 2-am', '2017-06-23', '', 0, 0, '2017-06-20 17:29:11'),
(27, 2, 22, '["1","3"]', 500, '2 مساءً6 مساءً', 'الاحد ،2017-06-05', '', 0, 0, '2017-06-29 22:48:05'),
(28, 2, 22, '["3"]', 0, '2 مساءً6 مساءً', 'الاحد ،2017-06-04', '', 0, 0, '2017-06-29 22:50:02');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(200) NOT NULL,
  `service_image` varchar(200) NOT NULL,
  `service_description` text NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_image`, `service_description`, `category_id`) VALUES
(1, 'تصوير سيشن  قبل الزفاف', '1498663732.jpg', '', 1),
(2, 'تصوير خارجي', '1498663778.jpg', '', 1),
(3, 'تصوير داخلي', '1498663792.jpg', '', 1),
(4, 'ميكاب سواريه', '1498663812.jpg', '', 2),
(5, 'ميكاب خطوبه', '1498663828.jpg', '', 2),
(6, 'ميكاب زفاف', '1498663842.jpg', '', 2),
(7, 'مراكز تجميل الاظافر', '1498673296.jpg', '', 3),
(8, 'مراكز ازاله الشعر بالليزر', '1498673227.jpg', '', 3),
(9, 'الحمام المغربي', '1498673169.jpg', '', 3),
(10, 'حفلات زفاف', '1498673072.jpg', '', 4),
(11, 'حفلات خطوبه', '1498673053.jpg', '', 4),
(13, 'تصفيف شعر', '1498663965.jpg', '', 5),
(14, 'قص شعر', '1498672975.jpg', '', 5),
(15, 'صبغة', '1498663991.jpg', '', 5),
(16, 'ورد طبيعي', '1498664006.jpg', '', 6),
(17, 'ورد صناعي', '1498664020.jpg', '', 6),
(18, 'شراء و تأجير الفساتين', '1498672777.jpg', '', 7);

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE IF NOT EXISTS `time` (
  `time_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `day_name` varchar(100) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `facebook_id` varchar(300) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_image` varchar(200) NOT NULL,
  `user_country_id` int(11) NOT NULL,
  `user_city_id` int(11) NOT NULL,
  `user_district_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_tel` varchar(100) NOT NULL,
  `user_facebook` varchar(200) NOT NULL,
  `user_twitter` varchar(200) NOT NULL,
  `user_linkedin` varchar(200) NOT NULL,
  `is_sent_verification` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `verification_token` text NOT NULL,
  `login_provider` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `facebook_id`, `user_name`, `user_email`, `user_image`, `user_country_id`, `user_city_id`, `user_district_id`, `username`, `password`, `user_tel`, `user_facebook`, `user_twitter`, `user_linkedin`, `is_sent_verification`, `is_verified`, `verification_token`, `login_provider`) VALUES
(0, '', 'Yusuf', 'yhamdy3@gmail.com', 'user_1496788332_334845054', 1, 0, 0, 'Yhamdy', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '01110124244', '', '', '', 1, 0, '328475cc290d9dd5314e592e7881cfce', ''),
(2, '', 'Remon Adel', 'remo.alking@gmail.com', '', 1, 1, 0, 'remon', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '01093237545', '', 'adel', '', 0, 1, '', ''),
(3, '', 'tod', 'tod@tod.com', '0', 1, 0, 0, 'tod', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', '123-1234-1234', 'a', 'b', 'c', 1, 1, '7d1cab5f0a798f6902861b4d85c5d301', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_details`
--

CREATE TABLE IF NOT EXISTS `users_details` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `picture` varchar(128) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_details`
--

INSERT INTO `users_details` (`id`, `name`, `picture`, `username`, `password`, `mobile`, `email`) VALUES
(2, 'Administrator', '2_1493433066.jpg', 'admin', '$2y$10$KgxnDF0tiPHZUwL7na.Pv.QmRvgB.l7IVMn/edsdNoJcl.1aYl/4u', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `company_portofolio`
--
ALTER TABLE `company_portofolio`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `company_services`
--
ALTER TABLE `company_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`time_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_details`
--
ALTER TABLE `users_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `company_portofolio`
--
ALTER TABLE `company_portofolio`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `company_services`
--
ALTER TABLE `company_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=174;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
