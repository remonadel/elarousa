<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */
class Company_services_m extends CI_Model
{
    protected $table_name = "company_services";
    protected $primary_key = "id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function getServices($provider_id)
    {
        return $this->db->query("SELECT * FROM `$this->table_name` where `company_id` = '$provider_id' ")->result();
    }

    public function getServicesData($cond = "")
    {
        $userid = $this->session->userdata('userid');
        return $this->db->query("
                SELECT * FROM `services`
                JOIN  `$this->table_name` ON $this->table_name.service_id = services.service_id  
                AND $this->table_name.company_id = '$cond'")->result();
    }

    public function save($data)
    {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function delete_company_services($company_services)
    {
        if (!empty($company_services)) {

            foreach ($company_services as $key => $value) {
//                $this->db->query("delete from $this->table_name where  $this->primary_key=$value->id ");
            }
            return true;
        }
        return false;
    }

    public function delete_old_company_services($company_id)
    {
        $this->db->query("delete from $this->table_name where  `company_id` = $company_id ");
    }


}