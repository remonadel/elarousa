<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model
{


    protected $table_name = "users";
    protected $primary_key = "user_id";
    protected $fields = "";
    protected $order_by = "";
    protected $primary_filter = "intval";
    protected $timestamps = false;


    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null, $single = FALSE){

        if($id != null)
        {
            $filter = $this->primary_filter;
            $id = $filter($id); // intval($id)
            $this->db->where($this->primary_key,$id);
            $method = 'row';
        }
        else if($single == TRUE)
        {
            $method = 'row';
        }

        else
        {
            $method = 'result';
        }

        $this->db->order_by($this->order_by);

        return $this->db->get($this->table_name)->$method();
    }

    // frunction that select single row or all result from table name of specific condition
    public function get_by($where , $single = FALSE){

        $this->db->where($where);
        return $this->get(null , $single);
    }


    public function tod_save($data , $id=null){

        //set time stamps
        if($this->timestamps == TRUE)
        {
            $now = date('Y-m-d H:i:s');
            $id || $data['created'] = $now;
            $data['modified'] = $now;
        }

        //insert
        if($id == null)
        {
            !isset($data[$this->primary_key]) || $data[$this->primary_key] = null; // autoincreament in data base
            $this->db->set($data);
            $this->db->insert($this->table_name);
            $id = $this->db->insert_id();
        }

        //update
        else
        {
            $filter = $this->primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->primary_key , $id);
            $this->db->update($this->table_name);
        }

        return $id;
    }


    public function login($username, $password)
    {

        $hash_password = $this->hash($password);

        $users = $this->db->query("SELECT * FROM `$this->table_name` where `username` = '$username'
        and `password` = '$hash_password' and `is_verified` = 1 ")->row();

        if (count($users) && !empty($users) && isset($users)) {
            $session_data = array(
                'userid' => $users->user_id,
                'name' => $users->user_name,
                'loggedin' => TRUE,
                'user_type' => 'user'
            );
            $this->session->set_userdata($session_data);
            return true;//if user exist
        } else {

            return false;//if user not exist
        }

    }

    public function hash($string)
    {
        return hash('sha512', $string, FALSE);
    }

    public function save($data)
    {
         $this->db->insert($this->table_name,$data);
        return $this->db->insert_id();
    }



    public function get_user_data()
    {
        $userid = $this->session->userdata('userid');
        $users = $this->db->query("SELECT * FROM `$this->table_name` where `user_id` = '$userid' ")->row();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }

    public function get_user_with_conditions($conditions)
    {

        $users = $this->db->query("SELECT * FROM `$this->table_name` $conditions ")->row();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }



    public function update($id,$data)
    {
        $this->db->where($this->primary_key, $id);
        $this->db->update($this->table_name, $data);
    }

}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */