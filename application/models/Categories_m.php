<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */


class Categories_m extends CI_Model
{
    protected $table_name = "categories";
    protected $primary_key = "category_id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function getCategories()
    {
        return $this->db->get_where($this->table_name, array('category_parent' => 0))->result();
    }




}