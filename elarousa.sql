-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 05, 2017 at 08:41 PM
-- Server version: 5.7.18-0ubuntu0.16.04.1
-- PHP Version: 7.0.18-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `elarousa`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(128) NOT NULL,
  `category_parent` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `category_name`, `category_parent`) VALUES
(1, 'خبير تجميل', 0),
(2, 'مصفف شعر', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `city_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `country_id`, `city_name`) VALUES
(1, 1, 'القاهرة'),
(2, 1, 'الجيزة');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `comp_id` int(11) NOT NULL,
  `facebook_id` varchar(300) NOT NULL,
  `comp_name` varchar(200) NOT NULL,
  `comp_email` varchar(200) NOT NULL,
  `comp_image` varchar(200) NOT NULL,
  `comp_slider` text NOT NULL,
  `category_id` int(11) NOT NULL DEFAULT '0',
  `comp_country_id` int(11) NOT NULL,
  `comp_city_id` int(11) NOT NULL,
  `comp_district_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `comp_des` text NOT NULL,
  `working_hours` text,
  `comp_tel` varchar(15) NOT NULL,
  `comp_facebook` varchar(200) NOT NULL,
  `comp_twitter` varchar(200) NOT NULL,
  `comp_linkedin` varchar(200) NOT NULL,
  `comp_pinterest` varchar(256) DEFAULT NULL,
  `comp_active` int(11) NOT NULL DEFAULT '0',
  `comp_rate` int(11) NOT NULL DEFAULT '0',
  `is_sent_verification` tinyint(1) NOT NULL COMMENT 'هل تم إرسال verification من قبل ليه',
  `is_verified` tinyint(1) NOT NULL,
  `verification_token` text NOT NULL,
  `login_provider` varchar(100) NOT NULL,
  `lat` varchar(300) NOT NULL,
  `lng` varchar(300) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`comp_id`, `facebook_id`, `comp_name`, `comp_email`, `comp_image`, `comp_slider`, `category_id`, `comp_country_id`, `comp_city_id`, `comp_district_id`, `username`, `password`, `comp_des`, `working_hours`, `comp_tel`, `comp_facebook`, `comp_twitter`, `comp_linkedin`, `comp_pinterest`, `comp_active`, `comp_rate`, `is_sent_verification`, `is_verified`, `verification_token`, `login_provider`, `lat`, `lng`) VALUES
(12, '', 'خبيرة المكياج يمني شافعي', 'a.todary@yellow.com.eg', '922edff96d6cf739228707cd1cbdbd82.jpg', '["9eef740c62a62b52017dc197d7a34793.jpg","a61168e78a505d63b8770b4dea5eedd0.jpg","4aeb3df07a16ad7763214e521b20e5fe.jpg","bffed1dd141829e1539b86adc5105b8d.jpg"]', 1, 1, 2, 1, 'todary', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '', '{"day1_from1":"1-am","day1_to1":"2-am","day1_from2":"","day1_to2":"","day2_from1":"4-pm","day2_to1":"5-pm","day2_from2":"1-am","day2_to2":"2-am","day3_from1":"","day3_to1":"","day3_from2":"2-am","day3_to2":"3-am","day4_from1":"","day4_to1":"","day4_from2":"3-am","day4_to2":"4-am","day5_from1":"","day5_to1":"","day5_from2":"","day5_to2":"","day6_from1":"","day6_to1":"","day6_from2":"","day6_to2":"","day7_from1":"","day7_to1":"","day7_from2":"","day7_to2":"","submit":"\\u062a\\u062d\\u062f\\u064a\\u062b \\u0623\\u0648\\u0642\\u0627\\u062a \\u0627\\u0644\\u0639\\u0645\\u0644"}', '012-2930-5555', '', 'bb2', 'ccsd3', 'pint', 0, 4, 1, 1, '7763a690ee6aed84866702497a95359b', '', '29.973614499999996', '31.253578599999997'),
(13, '', 'خبيرة المكياج سلمى عادل', 'remo.alking@gmail.com', '1.jpg', '', 1, 1, 1, 2, 'remon', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '', '{"day1_from1":"1-am","day1_to1":"2-am","day1_from2":"","day1_to2":"","day2_from1":"4-pm","day2_to1":"5-pm","day2_from2":"1-am","day2_to2":"2-am","day3_from1":"","day3_to1":"","day3_from2":"2-am","day3_to2":"3-am","day4_from1":"","day4_to1":"","day4_from2":"3-am","day4_to2":"4-am","day5_from1":"","day5_to1":"","day5_from2":"","day5_to2":"","day6_from1":"","day6_to1":"","day6_from2":"","day6_to2":"","day7_from1":"","day7_to1":"","day7_from2":"","day7_to2":"","submit":"\\u062a\\u062d\\u062f\\u064a\\u062b \\u0623\\u0648\\u0642\\u0627\\u062a \\u0627\\u0644\\u0639\\u0645\\u0644"}', '011-1864-33', 'FB', 'Twitter', 'LI', 'pinterest', 0, 4, 0, 1, '', '', '', ''),
(14, '', 'خبيرة المكياج مي حجازي', 'remo.alking@gmail.com', '2.jpg', '', 1, 1, 1, 1, 'remon1', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '', NULL, '011-1864-33', '', '', '', NULL, 0, 3, 0, 1, '', '', '', ''),
(15, '', 'خبيرة المكياج سلمى عادل 2', 'remo.alking@gmail.com', '3.jpg', '', 1, 1, 2, 2, 'remon2', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '', NULL, '011-1864-33', '', '', '', NULL, 0, 3, 0, 1, '', '', '', ''),
(16, '', '123', 'admin@adminaa.com', '', '', 1, 1, 0, 0, '123', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', '', NULL, '123-1231-1231', '#', '#', '#', NULL, 0, 0, 0, 0, '', '', '', ''),
(17, '', 'awdfghnm', 'a@a.com', '', '', 0, 0, 0, 0, 'asdf`', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', '', NULL, '012-9300-9322', '', '', '', NULL, 0, 0, 0, 0, '', '', '29.973614499999996', '31.253578599999997');

-- --------------------------------------------------------

--
-- Table structure for table `company_portofolio`
--

CREATE TABLE `company_portofolio` (
  `img_id` int(11) NOT NULL,
  `img_name` varchar(200) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `img_title` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `company_services`
--

CREATE TABLE `company_services` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `service_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company_services`
--

INSERT INTO `company_services` (`id`, `company_id`, `service_id`) VALUES
(1, 12, 1),
(2, 13, 1),
(3, 14, 1),
(4, 15, 1);

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE `country` (
  `country_id` int(11) NOT NULL,
  `country_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`country_id`, `country_name`) VALUES
(1, 'Egypt'),
(2, 'AUS\r\n'),
(3, 'KWA'),
(4, 'USA');

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `district_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL,
  `district_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`district_id`, `city_id`, `district_name`) VALUES
(2, 1, 'الدقي2'),
(3, 1, 'المعادى');

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `page_id` int(11) NOT NULL,
  `page_title` varchar(300) NOT NULL,
  `page_slug` varchar(300) NOT NULL,
  `page_body` text NOT NULL,
  `page_meta_title` text NOT NULL,
  `page_meta_desc` text NOT NULL,
  `page_meta_keywords` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`page_id`, `page_title`, `page_slug`, `page_body`, `page_meta_title`, `page_meta_desc`, `page_meta_keywords`) VALUES
(2, 'عن الموقع', 'عنالموقع', '<p><strong>عن الموقععن الموقععن الموقععن الموقععن الموقععن الموقع</strong></p>\r\n', 'عن الموقععن الموقع4', 'عن الموقععن الموقععن الموقععن الموقع56', 'عن الموقععن الموقععن الموقععن الموقععن الموقع789');

-- --------------------------------------------------------

--
-- Table structure for table `rates`
--

CREATE TABLE `rates` (
  `id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rates`
--

INSERT INTO `rates` (`id`, `company_id`, `user_id`, `rate`) VALUES
(1, 13, 13, 4),
(2, 12, 2, 4),
(3, 15, 2, 5),
(4, 14, 2, 4),
(5, 12, 13, 3),
(6, 14, 13, 2),
(7, 15, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `res_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `date` varchar(200) NOT NULL,
  `reservation_data` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservations`
--

INSERT INTO `reservations` (`res_id`, `user_id`, `comp_id`, `date`, `reservation_data`, `created_at`) VALUES
(17, 12, 12, '1-am الى 2-am', '', '2017-05-27 00:37:35'),
(18, 12, 12, '1-am الى 2-am', '', '2017-05-27 00:42:51'),
(19, 12, 12, '1-am الى 2-am', '', '2017-05-27 00:42:55'),
(20, 12, 12, '1-am الى 2-am', '', '2017-05-27 00:43:41'),
(21, 12, 12, '1-am الى 2-am', '', '2017-05-27 00:44:33'),
(22, 12, 12, '1-am الى 2-am', '', '2017-05-27 00:47:10'),
(23, 12, 12, '1-am الى 2-am', '2017-05-18', '2017-05-27 16:19:14');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL,
  `service_name` varchar(200) NOT NULL,
  `service_image` varchar(200) NOT NULL,
  `service_description` text NOT NULL,
  `category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_image`, `service_description`, `category_id`) VALUES
(1, 'مكياج للزفاف', '1493516640.jpg', '', 1),
(2, 'مكياج للخطوبة', '1493516664.jpg', '', 1),
(3, 'مكياج للحنة', '1493516693.jpg', '', 1),
(4, 'مكياج صباحى', '1493516733.jpg', '', 1),
(5, 'مكياج للعمل', '1493516756.jpg', '', 1),
(6, 'مكياج سهرات', '1493516772.jpg', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `time`
--

CREATE TABLE `time` (
  `time_id` int(11) NOT NULL,
  `comp_id` int(11) NOT NULL,
  `day_name` varchar(100) NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `facebook_id` varchar(300) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_image` varchar(200) NOT NULL,
  `user_country_id` int(11) NOT NULL,
  `user_city_id` int(11) NOT NULL,
  `user_district_id` int(11) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `user_tel` varchar(100) NOT NULL,
  `user_facebook` varchar(200) NOT NULL,
  `user_twitter` varchar(200) NOT NULL,
  `user_linkedin` varchar(200) NOT NULL,
  `is_sent_verification` tinyint(1) NOT NULL,
  `is_verified` tinyint(1) NOT NULL,
  `verification_token` text NOT NULL,
  `login_provider` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `facebook_id`, `user_name`, `user_email`, `user_image`, `user_country_id`, `user_city_id`, `user_district_id`, `username`, `password`, `user_tel`, `user_facebook`, `user_twitter`, `user_linkedin`, `is_sent_verification`, `is_verified`, `verification_token`, `login_provider`) VALUES
(2, '', 'Remon Adel', 'remo.alking@gmail.com', '', 1, 1, 0, 'remon', 'ba3253876aed6bc22d4a6ff53d8406c6ad864195ed144ab5c87621b6c233b548baeae6956df346ec8c17f5ea10f35ee3cbc514797ed7ddd3145464e2a0bab413', '011-1864-3373', '', 'adel', '', 0, 1, '', ''),
(3, '', 'tod', 'tod@tod.com', '0', 1, 0, 0, 'tod', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', '123-1234-1234', 'a', 'b', 'c', 1, 1, '7d1cab5f0a798f6902861b4d85c5d301', '');

-- --------------------------------------------------------

--
-- Table structure for table `users_details`
--

CREATE TABLE `users_details` (
  `id` int(11) NOT NULL,
  `name` varchar(64) NOT NULL,
  `picture` varchar(128) NOT NULL,
  `username` varchar(256) NOT NULL,
  `password` varchar(256) NOT NULL,
  `mobile` varchar(16) NOT NULL,
  `email` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_details`
--

INSERT INTO `users_details` (`id`, `name`, `picture`, `username`, `password`, `mobile`, `email`) VALUES
(2, 'ريمون عادل', '2_1493433066.jpg', 'remon', '$2y$10$KgxnDF0tiPHZUwL7na.Pv.QmRvgB.l7IVMn/edsdNoJcl.1aYl/4u', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `company_portofolio`
--
ALTER TABLE `company_portofolio`
  ADD PRIMARY KEY (`img_id`);

--
-- Indexes for table `company_services`
--
ALTER TABLE `company_services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`district_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`page_id`);

--
-- Indexes for table `rates`
--
ALTER TABLE `rates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`res_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `time`
--
ALTER TABLE `time`
  ADD PRIMARY KEY (`time_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `users_details`
--
ALTER TABLE `users_details`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `comp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `company_portofolio`
--
ALTER TABLE `company_portofolio`
  MODIFY `img_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `company_services`
--
ALTER TABLE `company_services`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `district_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `res_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
