<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Company_model extends CI_Model
{


    protected $table_name = "company";
    protected $primary_key = "comp_id";
    protected $fields = "";
    protected $order_by = "";
    protected $primary_filter = "intval";
    protected $timestamps = false;


    public function __construct()
    {
        parent::__construct();
    }

    public function get($id = null, $single = FALSE){

        if($id != null)
        {
            $filter = $this->primary_filter;
            $id = $filter($id); // intval($id)
            $this->db->where($this->primary_key,$id);
            $method = 'row';
        }
        else if($single == TRUE)
        {
            $method = 'row';
        }

        else
        {
            $method = 'result';
        }

        $this->db->order_by($this->order_by);

        return $this->db->get($this->table_name)->$method();
    }

    // frunction that select single row or all result from table name of specific condition
    public function get_by($where , $single = FALSE){

        $this->db->where($where);
        return $this->get(null , $single);
    }


    public function login($username, $password)
    {

        $hash_password = $this->hash($password);

        $users = $this->db->query("SELECT * FROM `$this->table_name` where `username` = '$username'
        and `password` = '$hash_password' ")->row();

        if (count($users)>0 && isset($users) && $users->comp_active == 1 && $users->is_verified == 1) {
            $session_data = array(
                'userid' => $users->comp_id,
                'name' => $users->comp_name,
                'loggedin' => TRUE,
                'isactive' => $users->comp_active,
                'isverified' => $users->is_verified,
                'user_type' => 'company'
            );
            $this->session->set_userdata($session_data);
            return true;//if user exist
        } elseif(count($users)>0 && isset($users) && $users->comp_active == 0 && $users->is_verified == 1) {
            return 2;
        } elseif(count($users)>0 && isset($users) && $users->is_verified == 0 && $users->comp_active == 1) {
            return 3;
        } elseif(count($users)>0 && isset($users) && $users->is_verified == 0 && $users->comp_active == 0) {
            return 4;
        } else {
            return 5;
        }

    }

    public function hash($string)
    {
        return hash('sha512', $string, FALSE);
    }

    public function save($data)
    {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }

    public function update($id,$data)
    {
        $this->db->where($this->primary_key, $id);
        $this->db->update($this->table_name, $data);
    }

    public function logout()
    {
        $this->session->sess_destroy();
    }

    public function get_user_company()
    {
        $userid = $this->session->userdata('userid');
        $users = $this->db->query("SELECT * FROM `$this->table_name` where `comp_id` = '$userid' ")->row();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }


    public function get_company_by_id($company_id)
    {
        $this->db->join('district', 'district.district_id = company.comp_district_id', 'left');
        $this->db->join('cities', 'cities.city_id = company.comp_city_id', 'left');
        $this->db->join('categories', 'categories.category_id = company.category_id', 'left');
        $this->db->where('comp_id', $company_id);
        $query = $this->db->get($this->table_name);
        $users = $query->row();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }

    public function get_company_with_conditions($conditions)
    {

        $users = $this->db->query("SELECT * FROM `$this->table_name` $conditions ")->row();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }

    public function add_company_service($company_id, $services, $prices)
    {
        foreach($services as $key => $service_id)
        {
            $this->Company_services_m->save([
                "company_id" => $company_id,
                "service_id" => intval($service_id),
                "service_price" => intval($prices[$key])
            ]);
        }
    }





    public function tod_save($data , $id=null){

        //set time stamps
        if($this->timestamps == TRUE)
        {
            $now = date('Y-m-d H:i:s');
            $id || $data['created'] = $now;
            $data['modified'] = $now;
        }

        //insert
        if($id == null)
        {
            !isset($data[$this->primary_key]) || $data[$this->primary_key] = null; // autoincreament in data base
            $this->db->set($data);
            $this->db->insert($this->table_name);
            $id = $this->db->insert_id();
        }

        //update
        else
        {
            $filter = $this->primary_filter;
            $id = $filter($id);
            $this->db->set($data);
            $this->db->where($this->primary_key , $id);
            $this->db->update($this->table_name);
        }

        return $id;
    }


    public function getImage($image_id)
    {
        return $this->db->query("SELECT `comp_image` FROM `$this->table_name` where `comp_id` = $image_id")->result();
    }
}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */