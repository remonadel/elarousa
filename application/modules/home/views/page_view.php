<?php $this->load->view("header"); ?>

    <div id="headerwrap" style="background: url(<?= HEADER_IMG . rand(1, 12) . ".jpg" ?>) center center; background-size: cover;min-height: 470px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 header_wrap2">
                    <h3 ><?= $page_data->page_title?></h3>
                </div>
            </div><!-- /row -->
        </div> <!-- /container -->
    </div>



    <div>
        <div class="container">
            <div class="row">
                <?=$page_data->page_body?>
            </div>
        </div>
    </div>





<?php $this->load->view("footer"); ?>