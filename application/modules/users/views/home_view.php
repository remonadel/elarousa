<?php $this->load->view('header'); ?>
    <div id="headerwrap" style="background: url(<?= IMG; ?>home_top_pic.jpg);background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-7 header_wrap">
                    <h1>
                        <i><img src="<?= IMG; ?>header_wrap_icon.png" /></i>
                        الدليل الأكبر
                    </h1>
                    <h1>لأفضل خبراء التجميل في مصر</h1>
                    <h2>نوفر لك كل ما تحتاجينه للتواصل</h2>
                    <h2>مع أرقى وأفخم صالونات التجميل</h2>
                    <h2>وخبراء الجمال والميك اب ارتست اينما كنتي</h2> </div>
                <div class="col-md-12">
                    <form method="post" action="search_result.php" class="clearfix">
                        <div class="col-md-2 white-background">
                            <p class="p-white-bg">انا ابحث عن خبير تجميل</p>
                            <div class="select-question"> <i class="fa fa-t5sos"><img src="<?= IMG; ?>t5sos_03.png"/></i> <span class="select-icon"></span>
                                <select name="section_id" class="select-question-small">
                                    <option value="">اختر التخصص</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 white-background">
                            <p class="p-white-bg">في محافظة</p>
                            <div class="select-question"> <i class="fa fa-map-marker"></i> <span class="select-icon"></span>
                                <select name="section_id" class="select-question-small">
                                    <option value="">اختر المحافظه</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 white-background">
                            <p class="p-white-bg">فى منطقة</p>
                            <div class="select-question"> <i class="fa fa-map-marker"></i> <span class="select-icon"></span>
                                <select name="section_id" class="select-question-small">
                                    <option value="">اختر المنطقة</option>
                                    <option value="">1</option>
                                    <option value="">2</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 white-background">
                            <div class="clearfix">
                                <p class="p-white-bg" style="float: right;">انا ابحث عن خبير تجميل</p>
                                <i class="fa fa-t5sos" style="margin-top: 10px;margin-right: 10px;"><img src="<?= IMG; ?>t5sos_03.png"/></i>
                            </div>
                            <div class="form_data col-md-8" style="width: 100%;">
                                <div class="form-group">
                                    <input name="" class="form-control" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 green-background clearfix">
                            <i class="fa fa-search"></i>
                            <input type="submit" class="done-search" value="إبحث" /> </div>
                    </form>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /headerwrap -->
    <div id="section_boxs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php $boxName = ['مكياج للزفاف', 'مكياج للخطوبة', 'مكياج للحنة', 'مكياج صباحي', 'مكياج للعمل', 'مكياج سهرات',]; ?>
                    <?php foreach (range(0, 5) as $i): ?>
                        <div class="col-md-4">
                            <div class="one_box">
                                <div class="one_box_title">
                                    <h1><?= $boxName[$i]; ?></h1></div>
                                <div class="he-wrap tpl6">
                                    <a href="#">
                                        <img src="<?= IMG; ?>boxs/<?= $i + 1; ?>.jpg" alt="" title="">
                                        <div class="he-view">
                                            <div class="bg a0" data-animate="fadeIn">
                                                <div class="dmbutton a2" data-animate="fadeInUp"><i><img src="assets/img/boxs/boxs_arrow.png" /></i></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!-- ********************************** Big Bg Section ********************************** -->
    <div id="BigBgSection">
        <div class="container centered">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h1>أكبر قاعدة بيانات لأفضل خبراء تجميل</h1>
                    <p>أكبر قاعدة بيانات لأفضل خبراء التجمل سجل بياناتك وتابع حجوزاتك اونلاين وخليك اقرب</p>
                    <a href="" class="register_btn Reg_Big_sec">تسجيل خبير تجميل</a>
                </div>
            </div>
        </div>
    </div>
    <!-- ******************** HOTTEST VOTES ********* -->
    <div id="hottestVotes">
        <div class="container">
            <div class="row centered">
                <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"></i>الأعلى تقييماً</h1>
                <div class="col-md-12">
                    <?php foreach (range(1, 3) as $i): ?>
                        <div class="col-sm-4">
                            <div class="one_vote">
                                <form method="" action=""> <img src="<?= IMG; ?>votes/<?= $i; ?>.jpg" />
                                    <h3>خبيرة المكياج مي حجازي</h3>
                                    <div class="stars clearfix">
                                        <input class="star star-5" name="star" type="radio" disabled="">
                                        <label class="star" for="star-5"></label>
                                        <input class="star star-4" name="star" type="radio" disabled="" checked="">
                                        <label class="star" for="star-4"></label>
                                        <input class="star star-3" name="star" type="radio" disabled="">
                                        <label class="star" for="star-3"></label>
                                        <input class="star star-2" name="star" type="radio" disabled="">
                                        <label class="star" for="star-2"></label>
                                        <input class="star star-1" name="star" type="radio" disabled="">
                                        <label class="star" for="star-1"></label>
                                    </div>
                                    <div class="voteAddress">
                                        <h2>العنوان</h2>
                                        <p>القاهرة الجديدة ، مصر</p>
                                        <p class="voteAddress_num">01234567891 - 01234567891</p>
                                    </div>
                                    <button class="bookNowBtn">احجز الآن</button>
                                    <div class="user_social clearfix">
                                        <a href="https://www.facebook.com" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span> </a>
                                        <a href="https://www.twitter.com" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span> </a>
                                        <a href="https://www.pinterest.com" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span> </a>
                                        <a href="https://www.linkedin.com" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span> </a>
                                    </div>
                            </div>
                            </form>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
<?php $this->load->view('footer'); ?>