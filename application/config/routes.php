<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* start todary Routes */
$route['company_login'] = "users/login";
$route['user_login'] = "users/login_user";
$route['logout'] = "users/logout";

$route['register'] = "users/register";
$route['getCity'] = "City_c/getCity";
$route['getDistrict'] = "District_c/getDistrict";

$route['register'] = "users/register";
$route['user_register'] = "users/register_user";

$route['profile/(:num)'] = "users/profile/$1";
$route['edit_profile_comp'] = "users/edit_profile";
$route['edit_working_hours'] = "users/edit_working_hours";
$route['edit_profile_user'] = "users/edit_profile_user";
$route['category/(:num)'] = "home/category/$1";
$route['category/(:num)/(:num)'] = "home/category/$1/$2";
$route['search'] = "home/search";

$route['active_my_account'] = "users/active_my_account";
$route['active_user_account'] = "users/active_user_account";
$route['facebook_login'] = "users/facebook_login";

$route['user_facebook_login'] = "users/user_facebook_login";
$route['company_facebook_login'] = "users/company_facebook_login";
$route['company/delete_protofolio_img'] = "users/delete_protofolio_img";


$route['resarvation'] = "Reservations_c/insert_res";
$route['sms_confirmation'] = "home/sms_confirmation";
$route['reservations'] = "users/get_comp_reservations";
$route['my_reservations'] = "users/user_reservations";
$route['remove_reservation'] = "users/remove_reservation";

$route['send'] = "Reservations_c/SendSMS";


$route['page/(:any)'] = "home/Home/view_page";

$route['reset_password/user'] = "home/reset_password/reset";
$route['reset_password/company'] = "home/reset_password/reset";

$route['user/add_new_password'] = "home/reset_password/add_new_password";
$route['company/add_new_password'] = "home/reset_password/add_new_password";


/* end todary Routes */