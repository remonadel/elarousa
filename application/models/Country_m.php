<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */


class Country_m extends CI_Model
{
    protected $table_name = "country";
    protected $primary_key = "country_id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function getCountry()
    {
        return $this->db->get($this->table_name)->result();

    }


}