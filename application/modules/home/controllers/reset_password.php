<?php
/**
 * Created by PhpStorm.
 * User: remon
 * Date: 4/4/17
 * Time: 8:28 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class reset_password extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

    }

    public $data=[];


    private function _send_email_to_user($subject,$body,$email) {
        $this->load->helper('email');
        //get all site admins email

        $this->load->library('email',array("mailtype"=>"html"));

        $this->email->from("info@elarousa.com", 'elarousa');
        $this->email->to([$email]);

        $this->email->subject($subject);
        $this->email->message($body);

        return $this->email->send();
    }


    public function reset(){

        $this->load->model("users/User_model");
        $this->load->model("users/Company_model");

        //check user_type if company or user
        $user_type=$this->uri->segment(2);

        $this->data["user_type"]=$user_type;


        if($this->input->server('REQUEST_METHOD') == 'POST'){

            $email=$this->input->post("email");


            $reset_password_date="";
            $reset_password_code="";

            if($user_type=="user"){

                //get user from user table

                $user_obj=$this->User_model->get_by(["user_email"=>"$email"],true);

                if(is_object($user_obj)){
                    //generate reset_password_date,reset_password_code
                    $reset_password_code=md5($user_obj->user_id."re#@!#@!mon_alking%#$%#".$user_obj->user_email."remon_al#@@!#@!!king".time()."i@m@frie#ndo@&frem%ona^l(king");
                    $reset_password_date=date("Y-m-d H:i:s",time());

                    $this->User_model->tod_save([
                        "reset_password_code"=>"$reset_password_code",
                        "reset_password_date"=>"$reset_password_date",
                    ],$user_obj->user_id);
                }
            }
            elseif($user_type=="company"){

                //get user from user table
                $comp_obj=$this->Company_model->get_by(["comp_email"=>"$email"],true);

                if(is_object($comp_obj)){
                    //generate reset_password_date,reset_password_code
                    $reset_password_code=md5($comp_obj->comp_id."re#@!#@!mon_alking%#$%#".$comp_obj->comp_email."remon_al#@@!#@!!king".time()."i@m@frie#ndo@&frem%ona^l(king");
                    $reset_password_date=date("Y-m-d H:i:s",time());

                    $this->Company_model->tod_save([
                        "reset_password_code"=>"$reset_password_code",
                        "reset_password_date"=>"$reset_password_date",
                    ],$comp_obj->comp_id);
                }
            }


            $link=base_url("$user_type/add_new_password?token=$reset_password_code");


            $email_body="
                <h1>Reset Password</h1>
                <p>this link will expire within 30 minutes</p>
                <a href='$link'>Reset Password</a>
            ";

            $this->_send_email_to_user($subject="Reset Password",$email_body,$email);

            $this->data["msg"]="<div class='alert alert-success'>تحقق من البريد الالكتروني</div>";
        }



        return $this->load->view("reset/demand_reset_pass",$this->data);
    }


    public function add_new_password(){
        $this->load->model("users/User_model");
        $this->load->model("users/Company_model");


        if($this->input->server('REQUEST_METHOD') == 'GET') {

            //get user type
            $user_type=$this->uri->segment(1);

            //get token
            $token=$this->input->get("token");

            if(empty($token)){
                redirect(base_url());
            }


            $this->data["user_type"]=$user_type;
            $this->data["token"]=$token;


            if($user_type=="user") {
                //get token and get user from it
                $user_obj=$this->User_model->get_by([
                    "reset_password_code"=>$token
                ],true);

                $dteStart = strtotime($user_obj->reset_password_date);
                $dteEnd   = time();

                //check if token is not expired
                if($dteEnd-$dteStart>1800) {
                    redirect(base_url());
                }
            }
            elseif($user_type=="company") {
                //get token and get user from it
                $comp_obj=$this->Company_model->get_by([
                    "reset_password_code"=>$token
                ],true);

                $dteStart = strtotime($comp_obj->reset_password_date);
                $dteEnd   = time();

                //check if token is not expired
                if($dteEnd-$dteStart>1800) {
                    redirect(base_url());
                }
            }

        }


        if($this->input->server('REQUEST_METHOD') == 'POST') {

            $user_type=$this->input->post("user_type");
            $token=$this->input->post("token");

            if($user_type=="user"){
                //get token and get user from it
                $user_obj=$this->User_model->get_by([
                    "reset_password_code"=>$token
                ],true);

                $dteStart = strtotime($user_obj->reset_password_date);
                $dteEnd   = time();


                //check if token is not expired
                if($dteEnd-$dteStart<1800){

                    //then allow user to set new password
                    //after update password remove token

                    $this->User_model->tod_save([
                        "password"=>$this->User_model->hash($this->input->post("new_pass")),
                        "reset_password_code"=>"",
                        "reset_password_date"=>"",
                    ],$user_obj->user_id);
                }

            }
            elseif($user_type=="company"){

                //get token and get user from it
                $comp_obj=$this->Company_model->get_by([
                    "reset_password_code"=>$token
                ],true);

                $dteStart = strtotime($comp_obj->reset_password_date);
                $dteEnd   = time();


                //check if token is not expired
                if($dteEnd-$dteStart<1800){

                    //then allow user to set new password
                    //after update password remove token

                    $this->Company_model->tod_save([
                        "password"=>$this->Company_model->hash($this->input->post("new_pass")),
                        "reset_password_code"=>"",
                        "reset_password_date"=>"",
                    ],$comp_obj->user_id);
                }

            }


            redirect(base_url("/"));
        }



        return $this->load->view("reset/add_new_password",$this->data);
    }




}