<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Common_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }


    public function log_action($user_id, $username, $action_description, $subject_type, $subject_id, $subject_info_1 = "", $subject_info_2 = "")
    {
        /* Used throughout the application to log different actions.
         * $subject_info_1 and $subject_info_2 are optional varchar fields and their values depend on the action being logged. */

        $sql = "INSERT INTO `logs` (`user_id`, `username`, `action_description`, `subject_type`, `subject_id`, `subject_info_1`, `subject_info_2`)
                VALUES (?, ?, ?, ?, ?, ?, ?)";
        $query = $this->db->query($sql, array($user_id, $username, $action_description, $subject_type, $subject_id, $subject_info_1, $subject_info_2));
    }


    public function get_info_by_token($table, $info, $token_type, $token_value)
    {
        /* This is a highly abstracted function that can return a specific piece of
         * info ($info) from a specific table given a token to identify
         * the subject with ($token_type and $token_value).
         * Returns a string. */

        $sql = "SELECT `$info` FROM `$table` WHERE `$token_type` = ?";
        $query = $this->db->query($sql, array($token_value));

        if ($query->num_rows() >= 1) {
            $row = $query->row_array();
            return $row[$info];
        } else {
            return FALSE;
        }
    }


    public function subject_exists($table, $token_type, $token_value)
    {
        /* Checks if a subject exists given a token type and value to identify it with.
         * Subject can be from any table.
         * Returns TRUE or FALSE. */

        $sql = "SELECT EXISTS(SELECT 1 FROM `$table` WHERE `$token_type` = ?)";
        $query = $this->db->query($sql, array($token_value));

        $row = $query->row_array();
        $keys = array_keys($row);
        $key = $keys[0];
        $result = $row[$key];

        return $result == 1;
    }


    public function get_subject_with_token($table, $token_type, $token_value)
    {
        /* Returns a subject's info given a token type and value to identify it with.
         * Subject can be from any table.
         * Returns a single row (not a multi-dimensional array) */

        $sql = "SELECT * FROM `$table` WHERE `$token_type` = ?";
        $query = $this->db->query($sql, array($token_value));

        if ($query->num_rows() >= 1) {
            return $query->row_array();
        } else {
            return FALSE;
        }
    }


    public function get_all_subjects_with_token($table, $token_type, $token_value)
    {
        /* Returns all subjects' info given a token type and value to identify them with.
         * Subjects can be from any table.
         * Returns a multi-dimensional array */

        $sql = "SELECT * FROM `$table` WHERE `$token_type` = ?";
        $query = $this->db->query($sql, array($token_value));

        if ($query->num_rows() >= 1) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }


    public function search($table, $token_type, $token_value)
    {
        /* Returns all subjects' info LIKE a value given a token type and value.
         * Subjects can be from any table.
         * Returns a multi-dimensional array */

        $sql = "SELECT * FROM `$table` WHERE `$token_type` LIKE CONCAT('%', ?, '%')";
        $query = $this->db->query($sql, array($token_value));

        if ($query->num_rows() >= 1) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }


    public function get_all_from_table($table, $options = array())
    {
        /* Returns all rows from any table.
         * Can be passed an array of options to limit or order the data.
         * Returns a multi-dimensional array. */

        $sql = "SELECT * FROM `$table`";

        if (array_key_exists("order_by", $options) && !array_key_exists("limit", $options)) {
            $order_by = $options["order_by"];
            $direction = $options["direction"];
            $sql = "SELECT * FROM `$table` ORDER BY `$order_by` $direction";
        } elseif (!array_key_exists("order_by", $options) && array_key_exists("limit", $options)) {
            $limit = $options["limit"];
            $sql = "SELECT * FROM `$table` LIMIT $limit";
        } elseif (array_key_exists("order_by", $options) && array_key_exists("limit", $options)) {
            $order_by = $options["order_by"];
            $direction = $options["direction"];
            $limit = $options["limit"];
            $sql = "SELECT * FROM `$table` ORDER BY `$order_by` $direction LIMIT $limit";
        }

        $query = $this->db->query($sql);

        if ($query->num_rows() >= 1) {
            return $query->result_array();
        } else {
            return FALSE;
        }
    }


    public function get_table_rows_count($table)
    {
        /* Returns count of all rows in a table. Returns a number. */

        $sql = "SELECT COUNT(`id`) AS `count` FROM `$table`";
        $query = $this->db->query($sql);
        $count = $query->row_array();
        return $count["count"];
    }


    public function get_search_rows_count($table, $search_clause, $search_value)
    {
        /* Returns count of all rows in a table that match a search clause. Returns a number. */

        $sql = "SELECT COUNT(`id`) AS `count` FROM `$table` WHERE `$search_clause` LIKE CONCAT('%', ?, '%')";
        $query = $this->db->query($sql, array($search_value));
        $count = $query->row_array();
        return $count["count"];
    }


    public function get_table_max_id($table)
    {
        /* Returns the last id in a table, a number. */

        $sql = "SELECT MAX(id) AS `max_id` FROM `$table`";
        $query = $this->db->query($sql);

        $row = $query->row_array();
        return $row["max_id"];
    }


    public function delete_subject($table, $token_type, $token_value)
    {
        /* Deletes a subject's info given a token type and value to identify it with.
         * Subject can be from any table */

        $sql = "DELETE FROM `$table` WHERE `$token_type` = ?";
        $query = $this->db->query($sql, array($token_value));
    }


    public function insert_keyword($keyword, $original_id)
    {
        /* Inserts a keyword and an original id in the keywords table after fetching them from youm7.com. */

        $sql = "INSERT INTO `news_keywords` (`keyword`, `original_id`) VALUES (?, ?)";
        $query = $this->db->query($sql, array($keyword, $original_id));
    }


    public function get_user_unseen_message_count($user_id)
    {
        /* Gets count of unseen user messages. */

        $sql = "SELECT COUNT(`id`) AS `count` FROM `messages` WHERE `receiver_id` = ? AND `seen` = 0 AND `hide_from_receiver` = 0";
        $query = $this->db->query($sql, array($user_id));

        $row = $query->row_array();
        return $row["count"];
    }


    public function get_user_news_this_week_count($user_id, $published_flag)
    {
        $sql = "SELECT COUNT(`id`) AS `count` FROM `news` WHERE `created_by_id` = ? AND `published` = ? AND `created_at` <= NOW() AND `created_at` >= DATE_SUB(NOW(), INTERVAL 7 DAY)";
        $query = $this->db->query($sql, array($user_id, $published_flag));
        $count = $query->row_array();
        return $count["count"];
    }


    public function authorized_to_view_page($page_session_variable)
    {
        /* This method gets called on every method of every controller with permissions enabled, and either returns FALSE
         * and shows a permission denied page or else returns TRUE to allow controllers' methods execution to carry on. */

        if ($this->session->userdata($page_session_variable) == 0) {
            $this->load->view("permission_denied_view");
            return FALSE;
        }

        return TRUE;
    }


    public function user_authorized_to_view_page($allowed_users)
    {
        /* This method does the same functionality as the previous one but while passed an array of authorized users */

        $user_id = $this->session->userdata("id");
        if (!in_array($user_id, $allowed_users)) {
            $this->load->view("permission_denied_view");
            return FALSE;
        }

        return TRUE;
    }


    /**
     * this function will upload files
     * @param $path is the location that will be upload
     * @param $title is the title that will before name file
     * @param $path is the array files
     * @throws \Exception
     * @return image_name
     */

    public function upload_files($path, $title, $files)
    {

        $config = array(
            'upload_path' => $path,
            'allowed_types' => 'jpg|gif|png',
            'overwrite' => 1,
        );

        $this->load->library('upload', $config);

        if (is_array($files['name'])) {

            $images = array();

            foreach ($files['name'] as $key => $image) {
                $_FILES['images[]']['name'] = $files['name'][$key];
                $_FILES['images[]']['type'] = $files['type'][$key];
                $_FILES['images[]']['tmp_name'] = $files['tmp_name'][$key];
                $_FILES['images[]']['error'] = $files['error'][$key];
                $_FILES['images[]']['size'] = $files['size'][$key];
                $array = explode('.', $_FILES['images[]']['name']);
                $extension = end($array);
                $fileName = $title . '_' . time() . '_' . rand() . "." . $extension;

                $images[] = $fileName;

                $config['file_name'] = $fileName;


                $this->upload->initialize($config);

                if ($this->upload->do_upload('images[]')) {
                    $this->upload->data();
                } else {
                    return $this->upload->display_errors();
                }
            }

            return $images;
        } else {

            $_FILES['profile']['name'] = $files['name'];
            $_FILES['profile']['type'] = $files['type'];
            $_FILES['profile']['tmp_name'] = $files['tmp_name'];
            $_FILES['profile']['error'] = $files['error'];
            $_FILES['profile']['size'] = $files['size'];
            $array = explode('.', $_FILES['profile']['name']);
            $extension = end($array);

            $fileName = $title . '_' . time() . '_' . rand() . "." . $extension;;

            $images = $fileName;

            $config['file_name'] = $fileName;


            $this->upload->initialize($config);

            if ($this->upload->do_upload('profile')) {
                $this->upload->data();
                return $images;
            } else {
                return false;
            }
        }
    }


    public function sendMail($email,$message){
        $this->load->library('email');

        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'sendgridusername',
            'smtp_pass' => 'sendgridpassword',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n"
        ));

        $this->email->from('your@example.com', 'Your Name');
        $this->email->to('someone@example.com');
        $this->email->cc('another@another-example.com');
        $this->email->bcc('them@their-example.com');
        $this->email->subject('Email Test');
        $this->email->message($message);
        if($this->email->send())
        {
            echo 'Email sent.';
        }
        else
        {
            show_error($this->email->print_debugger());
        }

    }

    public function select_rates($company_id, $user_id, $return_multi = TRUE)
    {
        $sql = "SELECT * FROM `rates` WHERE `company_id` = $company_id AND `user_id` = $user_id";
        $query = $this->db->query($sql);
        if($return_multi)
        {
            return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
        }
        return ($query->num_rows() >= 1) ? $query->row_array() : FALSE;
    }

    public function update_rates($company_id, $user_id, $rate)
    {
        $sql = "UPDATE `rates` SET `rate` = '$rate' WHERE `company_id` = '$company_id' AND `user_id` = '$user_id'";

        $this->db->query($sql, array($company_id, $user_id, $rate));
    }

    public function insert_into_rates($company_id, $user_id, $rate)
    {
        $sql = "INSERT INTO `rates` (`company_id`, `user_id`, `rate`)
                                    VALUES (?, ?, ?)";
        $this->db->query($sql, array($company_id, $user_id, $rate));
        return true;
    }

    public function update_total_rate($company_id, $rate)
    {
        $sql = "UPDATE `company` SET `comp_rate` = '$rate' WHERE `comp_id` = '$company_id'";

        $this->db->query($sql, array($rate, $company_id));
    }

    public function get_company_rates($company_id, $return_multi = TRUE)
    {
        $sql = "SELECT * FROM `rates` WHERE `company_id` = $company_id";
        $query = $this->db->query($sql);
        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }



}


/* End of file common_model.php */
/* Location: ./application/models/common_model.php */