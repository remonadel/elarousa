<?php
/**
 * Created by PhpStorm.
 * User: remon
 * Date: 4/4/17
 * Time: 8:28 PM
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model("home_model");
        $this->load->model("pages_model");
    }

    public function index()
    {
        $data['categories'] = $this->Common_model->get_all_from_table("categories");
        $data['countries'] = $this->Common_model->get_all_from_table("country");
        $data['cities'] = $this->home_model->get_cities_for_current_country();
        $data['most_rated_companies'] = $this->home_model->get_most_rated_companies();;
        $this->load->view('home_view', $data);
    }

    public function search()
    {
        if (isset($_GET))
        {

            if (isset($_GET['name']) && !empty($_GET['name']))
            {
                $search_txt = $_GET['name'];
            }

            $query = $this->prepare_query();
            $query_string = @explode('search?', $_SERVER['REQUEST_URI'])[1];
            $query_string_without_paging = @explode('&page', $query_string)[0];
            $category = $this->Common_model->get_subject_with_token("categories", "category_id", @$_GET['category']);
            $city = $this->Common_model->get_subject_with_token("cities", "city_id", @$_GET['city']);

        }
        else
        {
            $query = "";
        }
        if (isset($category) AND !empty($category) AND isset($city) AND !empty($city)) $data['search_text'] =  $category['category_name']."  في " . $city['city_name'];
        elseif (isset($category) AND !empty($category))  $data['search_text'] =  "  بحث عن  " .$category['category_name'] ;
        elseif (isset($city) AND !empty($city))  $data['search_text'] =  "  بحث في " .$city['city_name'] ;
        else $data['search_text'] =  "بحث في العروسة";
        $per_page = 12;
        if (isset($_GET['page'])) $offset = @intval($_GET['page'])*$per_page; else $offset = 0;

        $data['companies'] = $this->home_model->get_companies($per_page, $offset, $query);
        $data['top_rated_companies'] = $this->home_model->get_top_rated_companies($query);
        $config["base_url"] = site_url() . "search?$query_string_without_paging";
        $config['uri_segment'] = 3;
        $config["total_rows"] = $this->home_model->get_search_count($query);
        $config["per_page"] = $per_page;
        $config['page_query_string']    = TRUE;
        $config['query_string_segment']= 'page';
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        $cities = $this->home_model->get_cities_for_current_country();
        foreach ($cities as &$city)
        {
            $city['count'] = $this->home_model->get_city_companies_count($city['city_id']);
        }
        $data['cities'] = $cities;

        $districts = $this->home_model->get_districts_for_current_city($_GET['city']);
        foreach ($districts as &$district)
        {
            $district['count'] = $this->home_model->get_area_companies_count($district['district_id']);
        }
        $data['districts'] = $districts;

        $services_names = "";
        if (!empty($_GET['category']) && is_numeric($_GET['category']))
        {
            $services = $this->home_model->get_services_by_category_id($_GET['category']);
        }
        else
        {
            $services = $this->home_model->get_services_by_category_id("");
        }

        if (isset($services))
        {
            foreach ($services as &$service)
            {
                $services_names .= " ،" . $service['service_name'];
                $service['count'] = $this->home_model->get_service_companies_count($service['service_id']);
            }
            $data['services'] = $services;
        }

        $data["meta_title"] = $data['search_text'];
        $data["meta_keywords"] = "العروسة،خبراء تجميل، عرائس، مكياج،ميك اب ،خبراء تصفيف، شعر، تصفيف، تصفيف شعرك اب ،خبراء تصفيف، شعر، تصفيف، تصفيف شعر" . $services_names;
        $data["meta_description"] =   " بحث في العروسة العروسة الدليل الأكبر لخبراء التجميل ومصففي الشعر في مصر".$data['search_text'];

        $this->load->view('search_view', $data);
    }

    public function prepare_query()
    {
        $category = @intval(htmlspecialchars(trim($_GET['category'])));
        $city = @intval(htmlspecialchars(trim($_GET['city'])));
        $area = @intval(htmlspecialchars(trim($_GET['area'])));
        $name = @htmlspecialchars(trim($_GET['name']));
        $services = @htmlspecialchars(trim($_GET['service']));
        $services = str_replace('-', ',', $services);

        $query = array();
        if ($category && $category!=0) $query[] = 'company.category_id = '.$category;
        if ($city && $city!=0) $query[] = 'company.comp_city_id = '.$city;
        if ($area && $area!=0) $query[] = 'company.comp_district_id ='.$area;
        if (!empty($name)) $query[] = 'company.comp_name like "%'.$name.'%"';
        if ($services) $query[] = 'company_services.service_id IN ('.$services.')';
        /*if ($gender) $query[] = 'doctors.gender = "'.$gender.'"';
        if ($price AND $price == 1) $query[] = 'doctors.price <= 100';
        elseif ($price AND $price == 2) $query[] = 'doctors.price >100 AND doctors.price <=200';
        elseif ($price AND $price == 3) $query[] = 'doctors.price >200 AND doctors.price <= 500';
        elseif ($price AND $price == 4) $query[] = 'doctors.price >500';*/

        $query_str = implode(' AND ', $query);
        return $query_str;
    }

    public function category($id)
    {
        $data['service'] = $this->Common_model->get_subject_with_token("categories", "category_id", $id);
        if (!$id OR !$data['service']) redirect(site_url());
        $current_page = (int) $this->uri->segment(3);
        $per_page = 12;
        $companies_count = $this->home_model->get_category_companies_count($id);
        $config["base_url"] = site_url() . "category/$id/";
        $config['uri_segment'] = 3;
        $config["total_rows"] = $companies_count;
        $config["per_page"] = $per_page;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';
        $config['first_link'] = false;
        $config['last_link'] = false;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';
        $config['prev_link'] = '&laquo';
        $config['prev_tag_open'] = '<li class="prev">';
        $config['prev_tag_close'] = '</li>';
        $config['next_link'] = '&raquo';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li class="active"><a href="#">';
        $config['cur_tag_close'] = '</a></li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
        $companies = $this->home_model->get_category_companies($id, $per_page, $current_page);
        $data['companies'] = $companies;
        $data["meta_title"] = $data['service']['category_name'];
        $data["meta_keywords"] = $data['service']['category_name']." ،خدمات مكياج، ميك اب، كوافير ، عروسة، عرائس ، العروسة،خبراء تجميل، عرائس، مكياج،ميك اب ،خبراء تصفيف، شعر، تصفيف، تصفيف شعرك اب ،خبراء تصفيف، شعر، تصفيف، تصفيف شعر ";
        $data["meta_description"] = $data['service']['category_name'] . " العروسة الدليل الأكبر لخبراء التجميل ومصففي الشعر في مصر ";

        $this->load->view('service_view', $data);
    }

    private function average($avr_array)
    {
        if($avr_array)
        {
            $rate = array();
            foreach($avr_array as $value)
            {
                foreach($value as $key => $number)
                {
                    (!isset($res[$key])) ? $res[$key] = $number : $res[$key] += $number;
                }
            }
            $average =  round($res['rate'] / count($avr_array));
            return "$average";
        }
        else
        {
            return "0";
        }
    }

    public function sms_confirmation()
    {
        $ch = curl_init('http://api.cequens.com/cequens/api/v1/signIn');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "userName=REC&apiKey=cf1e2323-9c2f-492b-97db-0bfb96ac3f4d");
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        $access_token = $response['data']['access_token'];
        if (isset($_POST['com_id']) AND !empty($_POST['com_id']))
        {
            $company = $this->home_model->get_company_by_id($_POST['com_id']);
            $user = $this->Common_model->get_subject_with_token('users', "user_id", $_POST['user_id']);
            $sms_text = "العروسة تؤكد حجزك مع " . $company['comp_name'] . " يوم  " .  $_POST['day'] . " الموافق "  . $_POST['date'] . "الموعد" . $_POST['time']. " للتواصل: " . $company['comp_tel'];
            if (!empty($company['lat']) AND !empty($company['lng']))
            {
                $lat = $company['lat'];
                $lng = $company['lng'];
                $sms_text .= " على العنوان التالى " . "https://www.google.com/maps?daddr=$lat,$lng&amp;ll";
            }
            if (!empty($company['comp_tel']))
            {
                $sms_text .= " هاتف: " . $company['comp_tel'];
            }

            $sender_name = "Al3arousa";
            $number = $user['user_tel'];

            if (!empty($number))
            {
                $ch = curl_init('http://api.cequens.com/cequens/api/v1/messaging');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "messageText=$sms_text&senderName=$sender_name&messageType=text&recipients=$number&access_token=$access_token");
                $response = curl_exec($ch);
                echo $response;
            }

            //-- provider confirmation
            $sms_text = "العروسة يسعدها تبليغك بحجز موعد جديد  " . " يوم  " .  $_POST['day'] . " الموافق " . $_POST['date'] . "  من  " .  $user['user_name'] . " يمكنك التواصل معه على : " . $number;
            if (!empty($company['comp_tel']))
            {
                $compan_mobile = $company['comp_tel'];
                $ch = curl_init('http://api.cequens.com/cequens/api/v1/messaging');
                curl_setopt($ch, CURLOPT_POST, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, "messageText=$sms_text&senderName=$sender_name&messageType=text&recipients=$compan_mobile&access_token=$access_token");
                $response = curl_exec($ch);
                echo $response;
            }

        }

        curl_close($ch);

    }


    public function view_page(){
        $page_slug=urldecode($this->uri->segments[count($this->uri->segments)]);
        $page_slug=xss_clean($page_slug);


        $page_data=$this->pages_model->getPages(" where page_slug='$page_slug'");

        if(!(is_array($page_data)&&count($page_data))){
            show_404();
        }
        $page_data=$page_data[0];


        $this->data["page_data"]=$page_data;

        $this->data["meta_title"]=$page_data->page_meta_title;
        $this->data["meta_desc"]=$page_data->page_meta_desc;
        $this->data["meta_keywords"]=$page_data->page_meta_keywords;

        return $this->load->view("page_view",$this->data);
    }
}