<?php $this->load->view('header'); ?>
    <div id="headerwrap" style="background: url(<?= IMG; ?>home_top_pic.jpg);background-size: cover;">
        <div class="container">
            <div class="row">
                <div class="col-md-7 header_wrap">
                    <h1>
                        <i><img src="<?= IMG; ?>header_wrap_icon.png" /></i>
                        الدليل الأكبر
                    </h1>
                    <h1>لكل ما تحتاجه العروسة</h1><br><br><br>
                  </div>
                <div class="col-md-12">
                    <form method="get" action="search" class="clearfix">
                        <div class="col-md-1 white-background" style="width: 15%;">
                            <p class="p-white-bg">انا ابحث عن  </p>
                            <div class="select-question"> <i class="fa fa-t5sos"><img src="<?= IMG; ?>t5sos_03.png"/></i> <span class="select-icon"></span>
                                <select name="category" class="select-question-small" required>
                                    <option value="0">اختر التخصص</option>
                                    <?php foreach($categories as $category): ?>
                                        <option value="<?=$category['category_id'] ?>"><?=$category['category_name'] ?></option>
                                    <?php endforeach; ?>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-1 white-background" style="width: 12%">
                            <p class="p-white-bg">في دولة</p>
                            <div class="select-question"> <i class="fa fa-map-marker"></i> <span class="select-icon"></span>
                                <select name="country" id="select-country" class="select-question-small">
                                    <option value="0">اختر الدولة</option>
                                    <?php foreach($countries as $country): ?>
                                        <option value="<?=$country['country_id'] ?>"><?=$country['country_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div  class="col-md-2 white-background" style="width: 15%;">
                            <p class="p-white-bg">في محافظة</p>
                            <div class="select-question"> <i class="fa fa-map-marker"></i> <span class="select-icon"></span>
                                <select name="city" id="select-city" class="select-question-small">
                                    <option value="0">اختر المحافظه</option>
                                    <?php foreach($cities as $city): ?>
                                        <option value="<?=$city['city_id'] ?>"><?=$city['city_name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-2 white-background" style="width: 13%">
                            <p class="p-white-bg">فى منطقة</p>
                            <div class="select-question"> <i class="fa fa-map-marker"></i> <span class="select-icon"></span>
                                <select name="area" id="select-district" class="select-question-small">
                                    <option value="0">اختر المنطقة</option>

                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 white-background" style=" width: 21%;">
                            <div class="clearfix">
                                <p class="p-white-bg" style="float: right;">انا ابحث عن مقدم خدمة</p>
                                <i class="fa fa-t5sos" style="margin-top: 10px;margin-right: 10px;"><img src="<?= IMG; ?>t5sos_03.png"/></i>
                            </div>
                            <div class="form_data col-md-8" style="width: 100%;">
                                <div class="form-group">
                                    <input name="name" class="form-control" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 green-background clearfix">
                            <i class="fa fa-search"></i>
                            <input type="submit" class="done-search" value="إبحث" /> </div>
                    </form>
                </div>
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /headerwrap -->
    <div id="section_boxs">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php if (isset($categories)): ?>
                     <?php foreach ($categories as $category): ?>
                        <div class="col-md-4">
                            <div class="one_box">
                                <div class="one_box_title">
                                    <h1><?= $category['category_name']; ?></h1></div>
                                <div class="he-wrap tpl6">
                                    <a href="<?= site_url() . 'category/' . $category['category_id'] ?>">
                                        <img src="<?= CATEGORIES_IMAGES . $category['category_image']; ?>" alt="<?= $category['category_name']; ?>" title="<?= $category['category_name']; ?>">
                                        <div class="he-view">
                                            <div class="bg a0" data-animate="fadeIn">
                                                <div class="dmbutton a2" data-animate="fadeInUp"><i><img src="assets/img/boxs/boxs_arrow.png" /></i></div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>
    <!-- ********************************** Big Bg Section ********************************** -->
<?php  if (!$this->session->userdata('userid')): ?>
    <div id="BigBgSection">
        <div class="container centered">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h1>أكبر قاعدة بيانات لمقدمى خدمات العروسة</h1>
                    <p> سجل بياناتك وتابع حجوزاتك اونلاين وخليك اقرب</p>
                    <a href="<?= site_url() . 'register' ?>" id="home-page-register-btn" class="register_btn Reg_Big_sec">تسجيل مقدم خدمة</a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
    <!-- ******************** HOTTEST VOTES ********* -->
<?php if (isset($most_rated_companies) AND !empty($most_rated_companies)): ?>
    <div id="hottestVotes">
        <div class="container">
            <div class="row centered">
                <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"></i>الأعلى تقييماً</h1>
                <div class="col-md-12">
                    <?php foreach ($most_rated_companies as $company): ?>
                        <?php
                        $checked1 = "";
                        $checked2 = "";
                        $checked3 = "";
                        $checked4 = "";
                        $checked5 = "";

                        switch (@$company['comp_rate']) {
                            case '1':
                                $checked1 = "checked";
                                break;
                            case '2':
                                $checked2 = "checked";
                                break;
                            case '3':
                                $checked3 = "checked";
                                break;
                            case '4':
                                $checked4 = "checked";
                                break;
                            case '5':
                                $checked5 = "checked";
                                break;
                        }
                        $disabled = "disabled";
                        ?>
                        <div class="col-sm-4">
                            <div class="one_vote">
                                <?php if (! empty($company['comp_image'])): ?>
                                    <img src="<?= COMPANY_PROFILE . $company['comp_image'] ?>" />
                                <?php else: ?>
                                    <img src="<?= IMG . "03-Profile_02.jpg" ?>" />
                                <?php endif; ?>
                                    <h3><?= $company['comp_name'] ?></h3>
                                <form method="" action="">
                                    <div class="stars clearfix">
                                        <input class="star star-5" id="star-5-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked5; ?>>
                                        <label class="star star-5" for="star-5-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 5)"></label>
                                        <input class="star star-4" id="star-4-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked4; ?>>
                                        <label class="star star-4" for="star-4-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 4)"></label>
                                        <input class="star star-3" id="star-3-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked3; ?>>
                                        <label class="star star-3" for="star-3-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 3)"></label>
                                        <input class="star star-2" id="star-2-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked2; ?>>
                                        <label class="star star-2" for="star-2-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 2)"></label>
                                        <input class="star star-1" id="star-1-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked1; ?>>
                                        <label class="star star-1" for="star-1-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 1)"></label>

                                    </div>
                                </form>
                                    <div class="voteAddress">
                                        <h2>العنوان</h2>
                                        <p>
                                            <?php if(isset($company['district_name']) AND !empty($company['district_name'])): ?>
                                                <?= @$company['district_name'] . "، "?>
                                            <?php endif; ?>
                                            <?= @$company['city_name'] ?></p>
                                    </div>
                                <a href="<?= site_url().'profile/'.$company['comp_id']?>"> <button class="bookNowBtn">احجز الآن</button></a>
                                    <div class="user_social clearfix">
                                        <?php if(isset($company['comp_facebook']) AND !empty($company['comp_facebook'])): ?>  <a href="<?= $company['comp_facebook']; ?>" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span> </a><?php endif; ?>
                                        <?php if(isset($company['comp_twitter'])  AND !empty($company['comp_twitter'])): ?> <a href="<?= $company['comp_twitter']; ?>" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span> </a><?php endif; ?>
                                        <?php if(isset($company['company_pinterest'])  AND !empty($company['company_pinterest'])): ?><a href="<?= $company['company_pinterest']; ?>" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span> </a><?php endif; ?>
                                        <?php if(isset($company['comp_linkedin'])  AND !empty($company['comp_linkedin'])): ?> <a href="<?= $company['comp_linkedin']; ?>" class="custom_social"> <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span> </a><?php endif; ?>
                                    </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>

<?php endif; ?>
<?php $this->load->view('footer'); ?>
<script>
    $('body').on('change', '#select-country', function () {
        var row_id = $(this).val();
        var data_ajax = {};
        data_ajax.country_id = row_id;
        $.ajax({
            url: '<?= site_url()?>getCity',
            type: 'GET',
            data: data_ajax,
            success: function (data) {
                $('#select-city').html(data);
            }
        });
    })
    $('body').on('change', '#select-city', function () {
        var row_id = $(this).val();
        var data_ajax = {};
        data_ajax.city_id = row_id;
        $.ajax({
            url: '<?= site_url() ?>getDistrict',
            type: 'GET',
            data: data_ajax,
            success: function (data) {
                $('#select-district').html(data);
            }
        });
    })
    </script>
<script>
    function rate_event(doc_id, rate)
    {
        swal('برجاء تسجيل الدخول أولًا والدخول لصفحة مقدم الخدمة للتقييم !');

    }

</script>
