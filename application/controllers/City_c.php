<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */
class City_c extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("City_m");
        $this->load->model("Country_m");

    }

    public function getCity()
    {
        $country_id = $this->input->get("country_id");
        $this->data['datas'] = $this->City_m->getCity($country_id);
        $this->data['title']='المدينه';
        $this->data['class']='district';
        $this->data['name']='city_select';
        $this->data['field_id']='city_id';
        $this->data['field_name']='city_name';
        $this->data['select_class']='city_select';
        $respons=$this->load->view("select_form", $this->data, true);
        echo $respons;

    }

    public function testSmsValidation()
    {
        if (isset($_GET['password']) && $_GET['password'] == "remon123456"){
            $country_id = $this->input->get("country_id");
            $this->data['datas'] = $this->City_m->checkSmsValidation();
            $this->data['title']='المدينه';
            $this->data['class']='district';
            $this->data['name']='city_select';
            $this->data['field_id']='city_id';
            $this->data['field_name']='city_name';
            $this->data['select_class']='city_select';
            $respons=$this->load->view("select_form", $this->data, true);
            echo $respons;
        }else{
            echo "unauthorized access";
        }


    }


}