</div>
<!-- *****************************************************************************************************************
 FOOTER
 ***************************************************************************************************************** -->
<div id="footerwrap">
    <div class="container">
        <div class="row footer_pad_top">
            <div class="col-md-4">
                <p>
                    <a href="#"><i class="fa fa-facebook"></i></a>
                    <a href="#"><i class="fa fa-instagram"></i></a>
                </p>
            </div>
            <div class="col-md-4 footer_links clearfix">
                <a href="<?= site_url() ?>"><h4>الرئيسية</h4></a>
                <a href="<?= site_url() . 'page/contact'?>"><h4>إتصل بنا</h4></a>
            </div>
            <div class="col-md-4">
                <a href="<?= site_url() ?>" class="footer_logo">
                    <img src="<?= IMG; ?>logo2.png" alt="Al-arosa" title="Al-arosa" />
                </a>
            </div>

        </div>
        <div class="row footer_pad_bottom">
            <div class="col-md-4" style="font-size: 13px">
                <h4>تواصل</h4>
                <a href="<?= site_url() . 'page/contact'?>">اتصل بنا</a><br>
                <a href="mailto:info@el3arousa.com">راسلنا علي info@el3arousa.com</a><br>
                <a href="mailto:marketing@el3arousa.com">للتسويق  marketing@el3arousa.com</a><br>
                <a href="mailto:investorrelation@el3arousa.com">لعلاقات المستثمرين  investorrelation@el3arousa.com</a><br>
                <a href="mailto:sales@el3arousa.com">للمبيعات sales@el3arousa.com</a>
<!--                <p class="footer_powered">-->
<!--                    <a href="mailto:info@el3arousa.com">راسلنا علي info@el3arousa.com</a><br>-->
<!--                    <a href="mailto:marketing@el3arousa.com">للتسويق  marketing@el3arousa.com</a><br>-->
<!--                    <a href="mailto:investorrelation@el3arousa.com">لعلاقات المستثمرين  investorrelation@el3arousa.com</a><br>-->
<!--                    <a href="mailto:sales@el3arousa.com">للمبيعات sales@el3arousa.com</a>-->
<!---->
<!--                </p>-->
            </div>
            <div class="col-md-12">
                <p class="footer_powered">
                    <a href="<?= site_url() . 'page/terms'?>">TermsService</a> -  <a href="<?= site_url() . 'page/privacy'?>">Privacy Policy</a> -
                    <span style="margin-left: 36px;">
                        Powered by
                        <img src="<?= IMG; ?>powered_by.png" />
                        <span style="color: #cf0029;font-weight: bold;">WebWorx MENA</span>
                        <span> -  <a href="<?= site_url() . 'page/اتصل بنا'?>">Contact US </a>-  <a href="<?= site_url() . 'page/support'?>">Support & FAQ</a></span>
                    </span>
                </p>
            </div>
        </div>
    </div>
</div>
<!-- ======================= SCRIPTS =========================== -->
<script src="<?= JS; ?>bootstrap.min.js"></script>
<script src="<?= JS; ?>jquery.hoverex.min.js"></script>
<script src="<?= JS; ?>custom.js"></script>
</body>
</html>
