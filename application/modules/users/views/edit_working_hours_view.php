<?php $this->load->view("header"); ?>
    <!--  chart script file **include only this page**  -->
    <script src="<?= JS; ?>Chart.min.js"></script>
    <!--  chart script file **include only this page**  -->
    <div style="padding-top: 100px;">
        <div class="container">
            <div class="row centered">
                <div class="col-md-12">
                    <form class="" action="" method="post" enctype="multipart/form-data">
                    <?php $days = array(1=>'السبت', 2=> 'الأحد', 3=> 'الاثنين', 4 => 'الثلاثاء', 5 => 'الأربعاء', 6=> 'الخميس', 7=> 'الجمعة' ) ?>
                        <div class="user_profile clearfix">
                            <div class="col-md-12 userProfile">
                                <?php foreach ($days as $key=>$day): ?>
                                <div class="col-md-2">
                                    <p class="p_title"><?=$day ?></p>
                                </div>
                                <div class="col-md-2">
                                    <p class="p_title">من</p>
                                    <?php $value = 'day'.$key.'_'.'from1' ?>
                                    <select class="form-control" name="day<?=$key ?>_from1">
                                        <option value="">من</option>
                                        <option value="12-am" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 صباحاً</option>
                                        <option value="1-am" <?php if(@$working_hours->$value == "1-am") echo "selected";  ?>>1 صباحاً</option>
                                        <option value="2-am" <?php if(@$working_hours->$value == "2-am") echo "selected";  ?>>2 صباحاً</option>
                                        <option value="3-am" <?php if(@$working_hours->$value == "3-am") echo "selected";  ?>>3 صباحاً</option>
                                        <option value="4-am" <?php if(@$working_hours->$value == "4-am") echo "selected";  ?>>4 صباحاً</option>
                                        <option value="5-am" <?php if(@$working_hours->$value == "5-am") echo "selected";  ?>>5 صباحاً</option>
                                        <option value="6-am" <?php if(@$working_hours->$value == "6-am") echo "selected";  ?>>6 صباحاً</option>
                                        <option value="7-am" <?php if(@$working_hours->$value == "7-am") echo "selected";  ?>>7 صباحاً</option>
                                        <option value="8-am" <?php if(@$working_hours->$value == "8-am") echo "selected";  ?>>8 صباحاً</option>
                                        <option value="9-am" <?php if(@$working_hours->$value == "9-am") echo "selected";  ?>>9 صباحاً</option>
                                        <option value="10-am" <?php if(@$working_hours->$value == "10-am") echo "selected";  ?>>10 صباحاً</option>
                                        <option value="11-am" <?php if(@$working_hours->$value == "11-am") echo "selected";  ?>>11 صباحاً</option>
                                        <option value="12-pm" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 مساءً</option>
                                        <option value="1-pm" <?php if(@$working_hours->$value == "1-pm") echo "selected";  ?>>1 مساءً</option>
                                        <option value="2-pm" <?php if(@$working_hours->$value == "2-pm") echo "selected";  ?>>2 مساءً</option>
                                        <option value="3-pm" <?php if(@$working_hours->$value == "3-pm") echo "selected";  ?>>3 مساءً</option>
                                        <option value="4-pm" <?php if(@$working_hours->$value == "4-pm") echo "selected";  ?>>4 مساءً</option>
                                        <option value="5-pm" <?php if(@$working_hours->$value == "5-pm") echo "selected";  ?>>5 مساءً</option>
                                        <option value="6-pm" <?php if(@$working_hours->$value == "6-pm") echo "selected";  ?>>6 مساءً</option>
                                        <option value="7-pm" <?php if(@$working_hours->$value == "7-pm") echo "selected";  ?>>7 مساءً</option>
                                        <option value="8-pm" <?php if(@$working_hours->$value == "8-pm") echo "selected";  ?>>8 مساءً</option>
                                        <option value="9-pm" <?php if(@$working_hours->$value == "9-pm") echo "selected";  ?>>9 مساءً</option>
                                        <option value="10-pm" <?php if(@$working_hours->$value == "10-pm") echo "selected";  ?>>10 مساءً</option>
                                        <option value="11-pm" <?php if(@$working_hours->$value == "11-pm") echo "selected";  ?>>11 مساءً</option>

                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <p class="p_title">إلى</p>
                                    <?php $value = 'day'.$key.'_'.'to1' ?>
                                    <select class="form-control" name="day<?=$key ?>_to1">
                                        <option value="">إلى</option>
                                        <option value="12-am" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 صباحاً</option>
                                        <option value="1-am" <?php if(@$working_hours->$value == "1-am") echo "selected";  ?>>1 صباحاً</option>
                                        <option value="2-am" <?php if(@$working_hours->$value == "2-am") echo "selected";  ?>>2 صباحاً</option>
                                        <option value="3-am" <?php if(@$working_hours->$value == "3-am") echo "selected";  ?>>3 صباحاً</option>
                                        <option value="4-am" <?php if(@$working_hours->$value == "4-am") echo "selected";  ?>>4 صباحاً</option>
                                        <option value="5-am" <?php if(@$working_hours->$value == "5-am") echo "selected";  ?>>5 صباحاً</option>
                                        <option value="6-am" <?php if(@$working_hours->$value == "6-am") echo "selected";  ?>>6 صباحاً</option>
                                        <option value="7-am" <?php if(@$working_hours->$value == "7-am") echo "selected";  ?>>7 صباحاً</option>
                                        <option value="8-am" <?php if(@$working_hours->$value == "8-am") echo "selected";  ?>>8 صباحاً</option>
                                        <option value="9-am" <?php if(@$working_hours->$value == "9-am") echo "selected";  ?>>9 صباحاً</option>
                                        <option value="10-am" <?php if(@$working_hours->$value == "10-am") echo "selected";  ?>>10 صباحاً</option>
                                        <option value="11-am" <?php if(@$working_hours->$value == "11-am") echo "selected";  ?>>11 صباحاً</option>
                                        <option value="12-pm" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 مساءً</option>
                                        <option value="1-pm" <?php if(@$working_hours->$value == "1-pm") echo "selected";  ?>>1 مساءً</option>
                                        <option value="2-pm" <?php if(@$working_hours->$value == "2-pm") echo "selected";  ?>>2 مساءً</option>
                                        <option value="3-pm" <?php if(@$working_hours->$value == "3-pm") echo "selected";  ?>>3 مساءً</option>
                                        <option value="4-pm" <?php if(@$working_hours->$value == "4-pm") echo "selected";  ?>>4 مساءً</option>
                                        <option value="5-pm" <?php if(@$working_hours->$value == "5-pm") echo "selected";  ?>>5 مساءً</option>
                                        <option value="6-pm" <?php if(@$working_hours->$value == "6-pm") echo "selected";  ?>>6 مساءً</option>
                                        <option value="7-pm" <?php if(@$working_hours->$value == "7-pm") echo "selected";  ?>>7 مساءً</option>
                                        <option value="8-pm" <?php if(@$working_hours->$value == "8-pm") echo "selected";  ?>>8 مساءً</option>
                                        <option value="9-pm" <?php if(@$working_hours->$value == "9-pm") echo "selected";  ?>>9 مساءً</option>
                                        <option value="10-pm" <?php if(@$working_hours->$value == "10-pm") echo "selected";  ?>>10 مساءً</option>
                                        <option value="11-pm" <?php if(@$working_hours->$value == "11-pm") echo "selected";  ?>>11 مساءً</option>


                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <p class="p_title"> و </p>
                                </div>
                                <div class="col-md-2">
                                    <p class="p_title"> من</p>
                                    <?php $value = 'day'.$key.'_'.'from2' ?>
                                    <select class="form-control" name="day<?=$key ?>_from2">
                                        <option value="">من</option>
                                        <option value="12-am" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 صباحاً</option>
                                        <option value="1-am" <?php if(@$working_hours->$value == "1-am") echo "selected";  ?>>1 صباحاً</option>
                                        <option value="2-am" <?php if(@$working_hours->$value == "2-am") echo "selected";  ?>>2 صباحاً</option>
                                        <option value="3-am" <?php if(@$working_hours->$value == "3-am") echo "selected";  ?>>3 صباحاً</option>
                                        <option value="4-am" <?php if(@$working_hours->$value == "4-am") echo "selected";  ?>>4 صباحاً</option>
                                        <option value="5-am" <?php if(@$working_hours->$value == "5-am") echo "selected";  ?>>5 صباحاً</option>
                                        <option value="6-am" <?php if(@$working_hours->$value == "6-am") echo "selected";  ?>>6 صباحاً</option>
                                        <option value="7-am" <?php if(@$working_hours->$value == "7-am") echo "selected";  ?>>7 صباحاً</option>
                                        <option value="8-am" <?php if(@$working_hours->$value == "8-am") echo "selected";  ?>>8 صباحاً</option>
                                        <option value="9-am" <?php if(@$working_hours->$value == "9-am") echo "selected";  ?>>9 صباحاً</option>
                                        <option value="10-am" <?php if(@$working_hours->$value == "10-am") echo "selected";  ?>>10 صباحاً</option>
                                        <option value="11-am" <?php if(@$working_hours->$value == "11-am") echo "selected";  ?>>11 صباحاً</option>
                                        <option value="12-pm" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 مساءً</option>
                                        <option value="1-pm" <?php if(@$working_hours->$value == "1-pm") echo "selected";  ?>>1 مساءً</option>
                                        <option value="2-pm" <?php if(@$working_hours->$value == "2-pm") echo "selected";  ?>>2 مساءً</option>
                                        <option value="3-pm" <?php if(@$working_hours->$value == "3-pm") echo "selected";  ?>>3 مساءً</option>
                                        <option value="4-pm" <?php if(@$working_hours->$value == "4-pm") echo "selected";  ?>>4 مساءً</option>
                                        <option value="5-pm" <?php if(@$working_hours->$value == "5-pm") echo "selected";  ?>>5 مساءً</option>
                                        <option value="6-pm" <?php if(@$working_hours->$value == "6-pm") echo "selected";  ?>>6 مساءً</option>
                                        <option value="7-pm" <?php if(@$working_hours->$value == "7-pm") echo "selected";  ?>>7 مساءً</option>
                                        <option value="8-pm" <?php if(@$working_hours->$value == "8-pm") echo "selected";  ?>>8 مساءً</option>
                                        <option value="9-pm" <?php if(@$working_hours->$value == "9-pm") echo "selected";  ?>>9 مساءً</option>
                                        <option value="10-pm" <?php if(@$working_hours->$value == "10-pm") echo "selected";  ?>>10 مساءً</option>
                                        <option value="11-pm" <?php if(@$working_hours->$value == "11-pm") echo "selected";  ?>>11 مساءً</option>


                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <p class="p_title">إلى</p>
                                    <?php $value = 'day'.$key.'_'.'to2' ?>
                                    <select class="form-control" name="day<?=$key ?>_to2">
                                        <option value="">إلى</option>
                                        <option value="12-am" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 صباحاً</option>
                                        <option value="1-am" <?php if(@$working_hours->$value == "1-am") echo "selected";  ?>>1 صباحاً</option>
                                        <option value="2-am" <?php if(@$working_hours->$value == "2-am") echo "selected";  ?>>2 صباحاً</option>
                                        <option value="3-am" <?php if(@$working_hours->$value == "3-am") echo "selected";  ?>>3 صباحاً</option>
                                        <option value="4-am" <?php if(@$working_hours->$value == "4-am") echo "selected";  ?>>4 صباحاً</option>
                                        <option value="5-am" <?php if(@$working_hours->$value == "5-am") echo "selected";  ?>>5 صباحاً</option>
                                        <option value="6-am" <?php if(@$working_hours->$value == "6-am") echo "selected";  ?>>6 صباحاً</option>
                                        <option value="7-am" <?php if(@$working_hours->$value == "7-am") echo "selected";  ?>>7 صباحاً</option>
                                        <option value="8-am" <?php if(@$working_hours->$value == "8-am") echo "selected";  ?>>8 صباحاً</option>
                                        <option value="9-am" <?php if(@$working_hours->$value == "9-am") echo "selected";  ?>>9 صباحاً</option>
                                        <option value="10-am" <?php if(@$working_hours->$value == "10-am") echo "selected";  ?>>10 صباحاً</option>
                                        <option value="11-am" <?php if(@$working_hours->$value == "11-am") echo "selected";  ?>>11 صباحاً</option>
                                        <option value="12-pm" <?php if(@$working_hours->$value == "12-am") echo "selected";  ?>>12 مساءً</option>
                                        <option value="1-pm" <?php if(@$working_hours->$value == "1-pm") echo "selected";  ?>>1 مساءً</option>
                                        <option value="2-pm" <?php if(@$working_hours->$value == "2-pm") echo "selected";  ?>>2 مساءً</option>
                                        <option value="3-pm" <?php if(@$working_hours->$value == "3-pm") echo "selected";  ?>>3 مساءً</option>
                                        <option value="4-pm" <?php if(@$working_hours->$value == "4-pm") echo "selected";  ?>>4 مساءً</option>
                                        <option value="5-pm" <?php if(@$working_hours->$value == "5-pm") echo "selected";  ?>>5 مساءً</option>
                                        <option value="6-pm" <?php if(@$working_hours->$value == "6-pm") echo "selected";  ?>>6 مساءً</option>
                                        <option value="7-pm" <?php if(@$working_hours->$value == "7-pm") echo "selected";  ?>>7 مساءً</option>
                                        <option value="8-pm" <?php if(@$working_hours->$value == "8-pm") echo "selected";  ?>>8 مساءً</option>
                                        <option value="9-pm" <?php if(@$working_hours->$value == "9-pm") echo "selected";  ?>>9 مساءً</option>
                                        <option value="10-pm" <?php if(@$working_hours->$value == "10-pm") echo "selected";  ?>>10 مساءً</option>
                                        <option value="11-pm" <?php if(@$working_hours->$value == "11-pm") echo "selected";  ?>>11 مساءً</option>


                                    </select>
                                </div>
                                <?php endforeach;?>
                                <div class="col-md-3">
                                    <br>
                                </div>
                                <div class="col-md-2">
                                    <br>
                                    <input type="submit" name="submit" value="تحديث أوقات العمل" class="updateInput">
                                </div>
                        </div>



                    </form>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("footer");
