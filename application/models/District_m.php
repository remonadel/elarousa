<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */


class District_m extends CI_Model
{
    protected $table_name = "district";
    protected $primary_key = "district_id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function getDistrict($city_id)
    {
        return $this->db->get_where($this->table_name, array('city_id' => $city_id))->result();
    }


}