<?php $this->load->view("header"); ?>
    <!--  chart script file **include only this page**  -->
    <script src="<?= JS; ?>Chart.min.js"></script>
    <!--  chart script file **include only this page**  -->
    <div style="padding-top: 100px;">
        <div class="container">
            <div class="row centered">
                <div class="col-md-12">
                    <form class="" action="edit_profile_user" method="post" enctype="multipart/form-data">
                        <div style="position: relative;height: 200px;" class="div_profile">
                            <img class="profile_img" id="profile" src="<?= UPLOADS . 'user_profile/' . $user_data->user_image ?>"
                                 style="float: right;"/>
                            <input type="file" name="profile_img[]" id="profilePhoto"
                                   style="opacity: 0; position: absolute; height: 200px;cursor: pointer; ">
                        </div>
                        <div class="user_profile clearfix">
                            <div class="col-md-12 userProfile">
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">الإسم</p>
                                    <input type="text" name="user_name" class="form-control"
                                           value="<?= $user_data->user_name ?>" required autofocus/>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">البريد الإلكتروني</p>
                                    <input type="email" name="user_email" class="form-control"
                                           value="<?= $user_data->user_email ?>" required/>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">رقم الهاتف</p>
                                    <input type="tel" name="user_tel" class="form-control"
                                           value="<?= $user_data->user_tel ?>"
                                           required/>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">حسابات التواصل الإجتماعي</p>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="user_facebook" class="form-control"
                                               value="<?= $user_data->user_facebook ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-facebook fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="user_twitter" class="form-control"
                                               value="<?= $user_data->user_twitter ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-twitter fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="user_linkedin" class="form-control"
                                               value="<?= $user_data->user_linkedin ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-linkedin fa-stack-1x"></i></span></span>
                                    </div>
                                </div>


                                <div class="col-md-12 select">
                                    <div class="form-group">
                                        <label for="name">اختار البلد</label>
                                        <select class="form-control country_select" name="user_country_id">
                                            <option>البلد</option>
                                            <?php foreach ($countries as $key => $country): ?>
                                                <option <?php echo (($country->country_id == $user_data->user_country_id)?"selected":"") ?> value="<?= $country->country_id ?>"><?= $country->country_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="city"></div>
                                <input type="hidden" disabled class="city_selected" value="<?= $user_data->user_city_id ?>">
                                <input type="hidden" disabled class="district_selected" value="<?= $user_data->user_district_id ?>">


                                <div class="divs_company">
                                    <input type="submit" value="تحديث" class="updateInput">
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(".removeReservation").click(function () {
            $(this).parent().parent().remove();
        });

        //    upload images icon

        $(".file_button_container").click(function () {

            $("#hidden_upload_input").trigger('click');

        });


        $('#hidden_upload_input').on('change', function () {

            var val = $(this).val();

            $(this).siblings('#upload_file_name').text(val);

        });


        // profile image
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $('#profile').attr('src', event.target.result).appendTo(placeToInsertImagePreview).css({
                            'height': '200px',
                            'width': '200px',
                            'border-radius': '200px'
                        });
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#profilePhoto').on('change', function () {
            imagesPreview(this, 'div.div_profile');
        });


    </script>
    <script>
        //    chart script
        //        $(document).ready(function () {
        //            var randomScalingFactor = function () {
        //                return Math.round(Math.random() * 100)
        //            };
        //            var lineChartData = {
        //                labels: ["1Jan", "5Jan", "10Jan", "15Jan", "20Jan", "25Jan", "30Jan"],
        //                datasets: [
        //                    {
        //                        label: "Dashboard",
        //                        fillColor: "rgba(217, 4, 46, 0.1)",
        //                        strokeColor: "#d9042e",
        //                        pointColor: "#d9042e",
        //                        pointStrokeColor: "#fff",
        //                        pointHighlightFill: "#fff",
        //                        pointHighlightStroke: "#d9042e",
        //                        data: [600, 280, 450, 290, 1000, 500, 800]
        //                    }
        //                ]
        //            }
        //            var ctx = document.getElementById("line-chart").getContext("2d");
        //            window.myLine = new Chart(ctx).Line(lineChartData, {
        //                responsive: true,
        //                tooltipCornerRadius: 0
        //            });
        //        });
    </script>


    <script>
        $(function () {
            $('body').on('change', '.country_select', function () {
                var row_id = $(this).val();
                var data_ajax = {};
                data_ajax.country_id = row_id;
                var url = '<?= site_url('/'); ?>';
                console.log(url);
                $.ajax({
                    url: url + 'getCity',
                    type: 'GET',
                    data: data_ajax,
                    success: function (data) {
                        $('.city').html(data);
                    }
                });
            })

            $('body').on('change', '.city_select', function () {
                var row_id = $(this).val();
                var data_ajax = {};
                data_ajax.city_id = row_id;
                var url = '<?= site_url('/'); ?>';
                $.ajax({
                    url: url + 'getDistrict',
                    type: 'GET',
                    data: data_ajax,
                    success: function (data) {
                        $('.district').html(data);
                    }
                });
            })

            $('input:radio').change(function(){
                if ($(this).val()=='1')
                    $('.divs_company').show();
                else
                    $('.divs_company').hide();

            });

            var get_country_selected = $('.country_select').val();
            var city_selected = $('.city_selected').val();
            var district_selected = $('.district_selected').val();

            console.log(get_country_selected);
            console.log(city_selected);



            setTimeout(function () {
                if(get_country_selected > 0 && city_selected > 0)
                {
                    $('.country_select').trigger('change');

                    setTimeout(function () {
                        $('.city_select').val(city_selected);
                        if(district_selected > 0)
                        {
                            $('.city_select').trigger('change');
                            setTimeout(function () {
                                $('.district_select').val(district_selected);
                            },1000);
                        }
                    },1000);
                }
            },1000);

            $('body').on('blur', '.password2', function () {
                if ($('.password1').val()!=$('.password2').val()) {
                    $('.password2').focus()
                    $('#confirm_password').addClass('has-error')
                    $('#message_error').show()
                }
                else {
                    $('#confirm_password').removeClass('has-error')
                    $('#message_error').hide();
                }


            })

        });
    </script>

<?php $this->load->view("footer");