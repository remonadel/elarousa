<?php $this->load->view("header"); ?>

    <div id="headerwrap" style="background: url(<?= HEADER_IMG . rand(1, 12) . ".jpg" ?>) center center; background-size: cover;min-height: 470px;">
        <div class="container">
            <div class="row">
                <div class="col-md-12 header_wrap2">
                    <h3>هل نسيت كل مرورك؟</h3>
                </div>
            </div><!-- /row -->
        </div> <!-- /container -->
    </div>



    <div>
        <div class="container">

            <div class="row">
                <?php
                    if(isset($msg)){
                        echo $msg;
                    }
                ?>
            </div>

            <div class="row">
                <form action="<?=base_url("/reset_password/$user_type")?>" method="post">

                    <div class="form-group">
                        <label for="">الايميل</label>
                        <input type="email" name="email" class="form-control">
                    </div>

                    <button class="btn btn-primary">تابع</button>

                </form>
            </div>
        </div>
    </div>





<?php $this->load->view("footer"); ?>