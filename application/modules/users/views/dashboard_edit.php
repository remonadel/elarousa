<?php $this->load->view("header"); ?>
    <!--  chart script file **include only this page**  -->
    <script src="<?= JS; ?>Chart.min.js"></script>
    <!--  chart script file **include only this page**  -->
    <div style="padding-top: 100px;">
        <div class="container">
            <div class="row centered">
                <div class="col-md-12">
                    <form class="" action="edit_profile_comp" method="post" enctype="multipart/form-data">
                        <?php if (isset($error)):
                            echo $error;
                        elseif ($this->session->userdata("isactive") == 0):
                            echo "<div class='alert alert-danger'> حسابكم قيد الموافقة، برجاء مليء بيانات حسابك حتى يتم الموافقة عليه.</div>";
                        endif;
                        ?>

                        <div style="position: relative;height: 200px;" class="div_profile">
                            <?php if ( empty($company['comp_image'])): ?>
                                <img class="profile_img" id="profile" src="<?= UPLOADS . 'company_profile/' . $comp_data->comp_image ?>" style="float: right;"/>
                            <?php else: ?>
                                <img class="profile_img" id="profile" src="<?= IMG . "03-Profile_02.jpg" ?>" style="float: right;"/>

                            <?php endif; ?>

                            <input type="file" name="profile_img[]" id="profilePhoto"
                                   style="opacity: 0; position: absolute; height: 200px;cursor: pointer; ">
                        </div>
                        <div class="user_profile clearfix">
                            <div class="col-md-12 userProfile">
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">الإسم</p>
                                    <input type="text" name="comp_name" class="form-control"
                                           value="<?= $comp_data->comp_name ?>" required autofocus/>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">البريد الإلكتروني</p>
                                    <input type="email" name="comp_email" class="form-control"
                                           value="<?= $comp_data->comp_email ?>" required/>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">رقم الهاتف</p>
                                    <input type="tel" name="comp_tel" class="form-control"
                                           value="<?= $comp_data->comp_tel ?>" required/>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <p class="p_title">حسابات التواصل الإجتماعي</p>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="comp_facebook" class="form-control"
                                               value="<?= $comp_data->comp_facebook ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-facebook fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="comp_twitter" class="form-control"
                                               value="<?= $comp_data->comp_twitter ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-twitter fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="comp_linkedin" class="form-control"
                                               value="<?= $comp_data->comp_linkedin ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-linkedin fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled unstyled_new">
                                        <input type="text" name="comp_pinterest" class="form-control"
                                               value="<?= $comp_data->comp_pinterest ?>" placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                    class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                    class="fa fa-pinterest-p fa-stack-1x"></i></span></span>
                                    </div>
                                </div>

                                <div class="col-md-12 profile_fields">
                                    <div class="row">
                                        <div class="col-md-9">
                                        <p class="p_title">الخدمات</p>
                                        <?php foreach ($services as $key=>$value):?>
                                            <label class="checkbox-inline col-md-6" style="margin-right:0px;">
                                            <input <?php if (in_array($value->service_id, $comp_services_id)): ?>checked<?php endif;?> style="display: block;margin-right: 10px;margin-top: 10px;" type="checkbox" class="select_service" name="services[]" value="<?= $value->service_id ?>"><?=$value->service_name?></label>
                                            <input class="form-control" type="number" name="service_price_<?= $value->service_id ?>" placeholder="سعر الخدمة" value="<?=@$comp_services_prices[$value->service_id] ?>" style="margin-bottom: 9px;">

                                        <?php endforeach;?>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <?php $working_hours=json_decode($comp_data->working_hours)?>
                                <?php if ( !isset($working_hours)) @$working_hours->days = array()?>
                                <div class="col-md-12 profile_fields">
                                    <div class="row">
                                        <div class="col-md-4" style="font-size: 16px">
                                            <p class="p_title">ايام العمل</p>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left: 0px;margin-right: 0px;" type="checkbox" class="select_all" >اختر الكل</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left:0px;margin-right: 0px;" type="checkbox" class="select_service days-check"  name="days[]"<?php echo(in_array("السبت", $working_hours->days))?"checked":""?> value="السبت" >السبت</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left:  0px;margin-right: 0px;" type="checkbox" class="select_service days-check" name="days[]"<?php echo(in_array("الاحد", $working_hours->days))?"checked":""?> value="الاحد">الاحد</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left:  0px;margin-right: 0px;" type="checkbox" class="select_service days-check" name="days[]"<?php echo(in_array("الاثنين", $working_hours->days))?"checked":""?> value="الاثنين">الاثنين</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left : 0px;margin-right: 0px;" type="checkbox" class="select_service days-check" name="days[]"<?php echo(in_array("الثلاثاء", $working_hours->days))?"checked":""?> value="الثلاثاء">الثلاثاء</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left: 0px;margin-right: 0px;" type="checkbox" class="select_service days-check" name="days[]"<?php echo(in_array("الاربعاء", $working_hours->days))?"checked":""?> value="الاربعاء">الاربعاء</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left:  0px;margin-right: 0px;" type="checkbox" class="select_service days-check" name="days[]"<?php echo(in_array("الخميس", $working_hours->days))?"checked":""?> value="الخميس">الخميس</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-left:  0px;margin-right: 0px;" type="checkbox" class="select_service days-check" name="days[]"<?php echo(in_array("الجمعه", $working_hours->days))?"checked":""?> value="الجمعه">الجمعه</label>
                                            <?php $time=array(
                                                '12-am'=>'12 صباحا',
                                                '1-am'=>'1 صباحا',
                                                '2-am'=>'2 صباحا',
                                                '3-am'=>'3 صباحا',
                                                '4-am'=>'4 صباحا',
                                                '5-am'=>'5 صباحا',
                                                '6-am'=>'6 صباحا',
                                                '7-am'=>'7 صباحا',
                                                '8-am'=>'8 صباحا',
                                                '9-am'=>'9 صباحا',
                                                '10-am'=>'10 صباحا',
                                                '11-am'=>'11 صباحا',
                                                '12-pm'=>'12 مساءً',
                                                '1-pm'=>'1 مساءً',
                                                '2-pm'=>'2 مساءً',
                                                '3-pm'=>'3 مساءً',
                                                '4-pm'=>'4 مساءً',
                                                '5-pm'=>'5 مساءً',
                                                '6-pm'=>'6 مساءً',
                                                '7-pm'=>'7 مساءً',
                                                '8-pm'=>'8 مساءً',
                                                '9-pm'=>'9 مساءً',
                                                '10-pm'=>'10 مساءً',
                                                '11-pm'=>'11 مساءً'
                                            )?>
                                            <p class="p_title col-md-9">الفتره الاولى</p>
                                            <div class="col-md-5">
                                                <p class="p_title">من</p>
                                                <select class="form-control" name="timw1_from1">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php echo($key==$working_hours->timw1_from1)?"selected":""?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <p class="p_title">الى</p>
                                                <select class="form-control" name="timw1_from2" >
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php echo($key==$working_hours->timw1_from2)?"selected":""?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>

                                            <p class="p_title col-md-9">الفتره الثانيه</p>
                                            <div class="col-md-5">
                                                <p class="p_title">من</p>
                                                <select class="form-control" name="timw2_from1">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php echo($key==$working_hours->timw2_from1)?"selected":""?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <p class="p_title">الى</p>
                                                <select class="form-control" name="timw2_from2">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php echo($key==$working_hours->timw2_from2)?"selected":""?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>

                                <div class="col-md-12 select">
                                    <div class="form-group">
                                        <label for="name">اختار البلد</label>
                                        <select class="form-control country_select" name="comp_country_id">
                                            <option>البلد</option>
                                            <?php foreach ($countries as $key => $country): ?>
                                                <option <?php echo (($country->country_id == $comp_data->comp_country_id)?"selected":"") ?> value="<?= $country->country_id ?>"><?= $country->country_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="city">
                                <div class="col-md-12 select">
                                    <div class="form-group">
                                        <label for="name">اختار المدينة</label>
                                        <select class="form-control city_select" name="city_select">
                                            <option>المدينة</option>
                                            <?php foreach ($cities as $key => $city): ?>
                                                <option <?php echo (($city->city_id == $comp_data->comp_city_id)?"selected":"") ?> value="<?= $city->city_id ?>"><?= $city->city_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div class="district">
                                    <div class="col-md-12 select">
                                    <div class="form-group">
                                        <label for="name">اختار المنطقة</label>
                                        <select class="form-control district_select" name="district_select">
                                            <option>المنطقة</option>
                                            <?php foreach ($districts as $key => $district): ?>
                                                <option <?php echo (($district->district_id == $comp_data->comp_district_id)?"selected":"") ?> value="<?= $district->district_id ?>"><?= $district->district_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                    </div>
                                </div>
                                <div class="divs_company">
                                    <div class="col-md-12 categories_select">
                                        <div class="form-group">
                                            <label for="name">اختار التخصص</label>
                                            <select class="form-control" name="category_id">
                                                <option>التخصص</option>
                                                <?php foreach ($categories as $key => $category): ?>

                                                    <option <?php echo (($category->category_id == $comp_data->category_id)?"selected":"") ?>  value="<?= $category->category_id ?>"><?= $category->category_name ?></option>
                                                <?php endforeach; ?>

                                            </select>
                                        </div>
                                    </div>

                                <div class="portfolyoimages">

                                    <ul class="pimages-ul col-sm-12 photo_gallery_container">
                                        <?php if(!empty($slider_imgs) && count($slider_imgs)): ?>

                                            <?php foreach($slider_imgs as $key => $comp_slider): ?>
                                                <?php
                                                    $img_url = 'uploads/company_protofolio/'.$comp_slider->img_name;
                                                ?>
                                                <li class="pimages-li">
                                                    <span style="color: red;cursor: pointer;" data-img_id="<?= $comp_slider->img_id ?>" class="remove_comp_slider"><i class="fa fa-times-circle"></i></span>
                                                    <input type="hidden" name="old_comp_slider[]" value="<?= $comp_slider->img_name; ?>">
                                                    <img src="<?= site_url($img_url) ?>" style="height: 150px;width: 230px;">
                                                </li>

                                            <?php endforeach; ?>

                                        <?php endif; ?>

                                    </ul>

                                    <div id="upload" class="upload-btn" style="overflow: hidden;width: 100px;">
                                        <i class="file_button_container fa fa-plus-circle"></i>
                                        <div class="file_input_div">
                                            <input id="filePhoto" name="comp_slider[]" type="file" style="left: 30px !important;">
                                        </div>
                                    </div>

                                </div>
                                <input type="submit" value="تحديث" class="updateInput">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(".removeReservation").click(function () {
            $(this).parent().parent().remove();
        });

        //    upload images icon

        $(".file_button_container").click(function () {

            $("#hidden_upload_input").trigger('click');

        });


        $('#hidden_upload_input').on('change', function () {

            var val = $(this).val();

            $(this).siblings('#upload_file_name').text(val);

        });


        // Multiple images preview in browser
        var multipleImagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;
                var get_selected_files_input = $(input).clone();


                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function (event) {
                        var img = '<img style="height: 150px;width: 230px;" src="'+event.target.result+'">';
                        var append_li = '<li class="pimages-li">';
                        append_li += '<span style="color: red;cursor: pointer;" class="remove_comp_slider"><i class="fa fa-times-circle"></i></span>';
                        append_li += img;
                        append_li += '<div style="visibility: hidden;" class="display_hidden_input"></div>';
                        append_li += '</li>';
                        $('ul.photo_gallery_container').append(append_li);
                        var count_all_li = $('.photo_gallery_container li').length;

                        $('.photo_gallery_container li').eq(count_all_li - 1).find('.display_hidden_input').append(get_selected_files_input);
                        $('.file_input_div').html('<input id="filePhoto" name="comp_slider[]" type="file" style="left: 30px !important;">');
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('body').on('change','#filePhoto', function () {
            multipleImagesPreview(this, 'ul.photo_gallery_container');
        });

        // profile image
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $('#profile').attr('src', event.target.result).appendTo(placeToInsertImagePreview).css({
                            'height': '200px',
                            'width': '200px',
                            'border-radius': '200px'
                        });
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#profilePhoto').on('change', function () {
            imagesPreview(this, 'div.div_profile');
        });

        // check before submit if he select any of service or not if not send alert
//        $('.updateInput').click(function () {
//
//            var update_btn_element = $(this);
//            var service_items = update_btn_element.parents().find('.select_service');
//            var alert_flag = true;
//            $.each(service_items,function (ind,val) {
//
//                if($(this).is(":checked"))
//                {
//                    update_btn_element.parents().find('form').submit();
//                    alert_flag = false;
//
//                }
//
//            });
//
//            if(alert_flag)
//            {
//                alert('من فضلك إختار خدماتك اولا !!');
//                return false;
//            }
//
//        });

        // remove exist slider img
        $('body').on('click','.remove_comp_slider',function () {

            var confirm_msg = confirm('هل أنت متأكد ؟');
            if (confirm_msg)
            {

                var this_element = $(this);
                var img_id = $(this).attr('data-img_id');
                var url = '<?= site_url('/'); ?>';
                console.log(img_id);
                if(typeof(img_id) != "undefined" && img_id > 0 )
                {

                    var data_ajax = {};
                    data_ajax.img_id = img_id;
                    $.ajax({
                        url: url + 'company/delete_protofolio_img',
                        type: 'POST',
                        data: data_ajax,
                        success: function (data) {
                            this_element.parent().remove();
                        }
                    });

                }


            }

            return false;
        });

    </script>
    <script>
        //    chart script
//        $(document).ready(function () {
//            var randomScalingFactor = function () {
//                return Math.round(Math.random() * 100)
//            };
//            var lineChartData = {
//                labels: ["1Jan", "5Jan", "10Jan", "15Jan", "20Jan", "25Jan", "30Jan"],
//                datasets: [
//                    {
//                        label: "Dashboard",
//                        fillColor: "rgba(217, 4, 46, 0.1)",
//                        strokeColor: "#d9042e",
//                        pointColor: "#d9042e",
//                        pointStrokeColor: "#fff",
//                        pointHighlightFill: "#fff",
//                        pointHighlightStroke: "#d9042e",
//                        data: [600, 280, 450, 290, 1000, 500, 800]
//                    }
//                ]
//            }
//            var ctx = document.getElementById("line-chart").getContext("2d");
//            window.myLine = new Chart(ctx).Line(lineChartData, {
//                responsive: true,
//                tooltipCornerRadius: 0
//            });
//        });
    </script>


    <script>
        $(function () {
            $('body').on('change', '.country_select', function () {
                var row_id = $(this).val();
                var data_ajax = {};
                data_ajax.country_id = row_id;
                var url = '<?= site_url('/'); ?>';
                console.log(url);
                $.ajax({
                    url: url + 'getCity',
                    type: 'GET',
                    data: data_ajax,
                    success: function (data) {
                        $('.city').html(data);
                    }
                });
            })

            $('body').on('change', '.city_select', function () {
                var row_id = $(this).val();
                var data_ajax = {};
                data_ajax.city_id = row_id;
                var url = '<?= site_url('/'); ?>';
                $.ajax({
                    url: url + 'getDistrict',
                    type: 'GET',
                    data: data_ajax,
                    success: function (data) {
                        $('.district').html(data);
                    }
                });
            })

            $('input:radio').change(function(){
                if ($(this).val()=='1')
                    $('.divs_company').show();
                else
                    $('.divs_company').hide();

            });

            var get_country_selected = $('.country_select').val();
            var city_selected = $('.city_selected').val();
            var district_selected = $('.district_selected').val();

            console.log(get_country_selected);
            console.log(city_selected);



            setTimeout(function () {
                if(get_country_selected > 0 && city_selected > 0)
                {
                    $('.country_select').trigger('change');

                    setTimeout(function () {
                        $('.city_select').val(city_selected);
                        if(district_selected > 0)
                        {
                            $('.city_select').trigger('change');
                            setTimeout(function () {
                                $('.district_select').val(district_selected);
                            },1000);
                        }
                    },1000);
                }
            },1000);

            $('body').on('blur', '.password2', function () {
                if ($('.password1').val()!=$('.password2').val()) {
                    $('.password2').focus()
                    $('#confirm_password').addClass('has-error')
                    $('#message_error').show()
                }
                else {
                    $('#confirm_password').removeClass('has-error')
                    $('#message_error').hide();
                }


            })

        });

        $(".select_all").click(function () {
            if ($(".select_all").is(':checked')) {
                $(".days-check").prop("checked", true);
            } else {
                $(".days-check").prop("checked", false);
            }
        });
    </script>

<?php $this->load->view("footer");