<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */
class Reservations_c extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Reservation_m");
    }

    public function insert_res()
    {
        $date=array();
        $date['comp_id'] = $this->input->post("com_id");
        $date['user_id'] = $this->input->post("user_id");
        $date['time'] = $this->input->post("time");
        $date['date'] = $this->input->post("date");
        $date['day'] = $this->input->post("day");
        $date['services_id'] = $this->input->post("services_id");
        $date['total_price'] = $this->input->post("price");
        $date['created_at'] = date('Y-m-d H:i:s');

        $reslut=$this->Reservation_m->save($date);
        echo $reslut;
    }

    public function first_reminder()
    {
        $result = $this->Reservation_m->get_all_for_first_reminder();
        foreach ($result as $reservation){
            $arr = explode('_', $reservation['time']);
            $arr2 = explode(' ', $arr[0]);
           // print_r($arr2);
            if ($arr2[1] == "صباحا" AND $arr2[0] != 12)
                $time = $arr2[0];
            else if ($arr2[1] == "صباحا" AND $arr2[0] == 12)
                $time = 00;
            else if ($arr2[1] == "مساءً" AND $arr2[0] != 12)
                $time = $arr2[0] + 12;
            else
                $time = 12;

            $server_hour = date('H');
            $diff = abs($time - $server_hour);

            if ($diff < 11 AND $diff >= 10) {
                $this->send_reminder($reservation);
                $this->Reservation_m->update_reminder_status($reservation['res_id'], 1);
            }
            if ($diff < 4 AND $diff >= 3) {
                $this->send_reminder($reservation);
                $this->Reservation_m->update_reminder_status($reservation['res_id'], 2);
            }

        }

    }


    public function send_reminder($reservation_data)
    {
        $ch = curl_init('http://api.cequens.com/cequens/api/v1/signIn');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "userName=REC&apiKey=cf1e2323-9c2f-492b-97db-0bfb96ac3f4d");
        $response = curl_exec($ch);
        $response = json_decode($response, true);
        $access_token = $response['data']['access_token'];

        $sms_text = "العروسة تذكرك بموعد حجزك مع " . $reservation_data['comp_name'] . " يوم  " .  $reservation_data['day'] . "الموافق"  . $reservation_data['date'] . "الموعد" . $reservation_data['time'];
        if (!empty($reservation_data['lat']) AND !empty($reservation_data['lng']))
        {
            $lat = $reservation_data['lat'];
            $lng = $reservation_data['lng'];
            $sms_text .= " على العنوان التالى " . "https://www.google.com/maps?daddr=$lat,$lng&amp;ll";
        }
        if (!empty($company['comp_tel']))
        {
            $sms_text .= " هاتف: " . $reservation_data['comp_tel'];
        }

        $sender_name = "Al3arousa";
        $number = $reservation_data['user_tel'];

        if (!empty($number))
        {
            $ch = curl_init('http://api.cequens.com/cequens/api/v1/messaging');
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, "messageText=$sms_text&senderName=$sender_name&messageType=text&recipients=$number&access_token=$access_token");
            $response = curl_exec($ch);
            echo $response;
        }



        curl_close($ch);

    }


    public function SendSMS(){

        $client = new SoapClient('https://smsvas.vlserv.com/KannelSending/service.asmx');

        $userName = "El3arousa";

        $Password = "mebRYpepTe";

        $SMSText = "text";

        $SMSLang = "e";

        $SMSSender = "El3arousa";

        $SMSReceiver = "01229309501";

        $result = $client->SendSMS(array(

            "Username" => $userName,

            "Password" => $Password,

            "SMSText" => $SMSText,

            "SMSLang" => $SMSLang,

            "SMSSender" => $SMSSender,

            "SMSReceiver" => $SMSReceiver));

        $response_arr = objectToArray($result);

        echo "return_code= " . str_replace(";", "", $response_arr);

        function objectToArray($d)

        {

            if (is_object($d))

            {$d = get_object_vars($d);}

            if (is_array($d))

            {return array_map(__FUNCTION__, $d);}

            else {return $d;}

        }
    }


}