<?php $this->load->view('header'); ?>

<!-- *****************************************************************************************************************
 HEADERWRAP
 ***************************************************************************************************************** -->
<div id="headerwrap" style="background: url(<?= HEADER_IMG . rand(1, 12) . ".jpg" ?>) center center; background-size: cover;min-height: 470px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 header_wrap2">
                <h1><i><img src="<?= IMG ?>header_wrap_icon.png" /></i><?= $service['category_name'] ?> </h1>
            </div>
        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /headerwrap -->

<div id="searchResult">
    <div class="container">
        <div class="row centered">

            <div class="col-md-12">
        <?php if (isset($companies) AND !empty($companies)): ?>
            <?php foreach ($companies AS $company): ?>
                <?php
                switch (@$company['comp_rate']) {
                    case '1':
                        $checked1 = "checked";
                        break;
                    case '2':
                        $checked2 = "checked";
                        break;
                    case '3':
                        $checked3 = "checked";
                        break;
                    case '4':
                        $checked4 = "checked";
                        break;
                    case '5':
                        $checked5 = "checked";
                        break;
                }
                $disabled = "disabled";
                ?>
                <div class="col-sm-3">
                    <div class="one_vote">
                        <?php if (! empty($company['comp_image'])): ?>
                            <img src="<?= COMPANY_PROFILE . $company['comp_image'] ?>" />
                        <?php else: ?>
                            <img src="<?= IMG . "03-Profile_02.jpg" ?>" />
                        <?php endif; ?>
                            <h3><?= $company['comp_name']?></h3>
                        <form method="" action="">
                            <div class="stars clearfix">
                                <input class="star star-5" id="star-5-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked5; ?>>
                                <label class="star star-5" for="star-5-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 5)"></label>
                                <input class="star star-4" id="star-4-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked4; ?>>
                                <label class="star star-4" for="star-4-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 4)"></label>
                                <input class="star star-3" id="star-3-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked3; ?>>
                                <label class="star star-3" for="star-3-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 3)"></label>
                                <input class="star star-2" id="star-2-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked2; ?>>
                                <label class="star star-2" for="star-2-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 2)"></label>
                                <input class="star star-1" id="star-1-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked1; ?>>
                                <label class="star star-1" for="star-1-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 1)"></label>
                            </div>
                        </form>
                            <div class="voteAddress">
                                <h2>العنوان</h2>
                                <?php if(isset($company['district_name']) AND !empty($company['district_name'])): ?>
                                    <?= @$company['district_name'] . "، "?>
                                <?php endif; ?>
                                <?= @$company['city_name'] ?></p>
                            </div>
                            <a href="<?= site_url().'profile/'.$company['comp_id']?>"> <button class="bookNowBtn">احجز الآن</button></a>
                            <div class="user_social clearfix">
                                <?php if(isset($company['comp_facebook']) AND !empty($company['comp_facebook'])): ?>
                                <a href="<?=$company['comp_facebook'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                                <?php if(isset($company['comp_twitter']) AND !empty($company['comp_twitter'])): ?>
                                <a href="<?=$company['comp_twitter'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                                <?php if(isset($company['company_pinterest']) AND !empty($company['company_pinterest'])): ?>
                                <a href="<?=$company['company_pinterest'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                                <?php if(isset($company['comp_linkedin']) AND !empty($company['comp_linkedin'])): ?>
                                <a href="<?=$company['comp_linkedin'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                            </div>
                            </div>

                    </div>
                <?php endforeach; ?>
                <div class="clear"></div>
                <div class="paging">
                    <ul class="pagination">
                        <?php if (isset($pagination)) echo $pagination;?>
                    </ul>
                </div>
        <?php else: ?>
            لا يوجد شركات فى هذا التخصص حالياً
    <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<!---->
<!--<!-- *****************************************************************************************************************-->
<!-- Big Bg Section-->
<!-- ***************************************************************************************************************** -->
<!--<div id="BigBgSection">-->
<!--    <div class="container centered">-->
<!--        <div class="row">-->
<!--            <div class="col-lg-8 col-lg-offset-2">-->
<!--                <h1>أكبر قاعدة بيانات لأفضل خبراء تجميل</h1>-->
<!--                <p>أكبر قاعدة بيانات لأفضل خبراء التجمل سجل بياناتك وتابع حجوزاتك اونلاين وخليك اقرب</p>-->
<!--                <a href="" class="register_btn Reg_Big_sec">تسجيل خبير تجميل</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->
<!--<!-- *****************************************************************************************************************-->
<!-- HOTTEST VOTES-->
<!-- ***************************************************************************************************************** -->
<!--<div id="hottestVotes">-->
<!--    <div class="container">-->
<!--        <div class="row centered">-->
<!--            <h1><i><img src="--><?//= IMG ?><!--header_wrap_icon.png"></i>الأعلى تقييماً</h1>-->
<!--            <div class="col-md-12">-->
<!--                <div class="col-sm-3">-->
<!--                    <div class="one_vote">-->
<!--                        <form method="" action="">-->
<!--                            <img src="--><?//= IMG ?><!--votes/1.jpg" />-->
<!--                            <h3>خبيرة المكياج مي حجازي</h3>-->
<!--                            <div class="stars clearfix">-->
<!--                                <input class="star star-5" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-5"></label>-->
<!--                                <input class="star star-4" name="star" type="radio" disabled="" checked="">-->
<!--                                <label class="star" for="star-4"></label>-->
<!--                                <input class="star star-3" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-3"></label>-->
<!--                                <input class="star star-2" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-2"></label>-->
<!--                                <input class="star star-1" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-1"></label>-->
<!--                            </div>-->
<!--                            <div class="voteAddress">-->
<!--                                <h2>العنوان</h2>-->
<!--                                <p>القاهرة الجديدة ، مصر</p>-->
<!--                                <p class="voteAddress_num">01234567891 - 01234567891</p>-->
<!--                            </div>-->
<!--                            <button class="bookNowBtn">احجز الآن</button>-->
<!--                            <div class="user_social clearfix">-->
<!--                                <a href="https://www.facebook.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.twitter.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.pinterest.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.linkedin.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                    </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--                <div class="col-sm-3">-->
<!--                    <div class="one_vote">-->
<!--                        <form method="" action="">-->
<!--                            <img src="--><?//= IMG ?><!--votes/1.jpg" />-->
<!--                            <h3>خبيرة المكياج مي حجازي</h3>-->
<!--                            <div class="stars clearfix">-->
<!--                                <input class="star star-5" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-5"></label>-->
<!--                                <input class="star star-4" name="star" type="radio" disabled="" checked="">-->
<!--                                <label class="star" for="star-4"></label>-->
<!--                                <input class="star star-3" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-3"></label>-->
<!--                                <input class="star star-2" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-2"></label>-->
<!--                                <input class="star star-1" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-1"></label>-->
<!--                            </div>-->
<!--                            <div class="voteAddress">-->
<!--                                <h2>العنوان</h2>-->
<!--                                <p>القاهرة الجديدة ، مصر</p>-->
<!--                                <p class="voteAddress_num">01234567891 - 01234567891</p>-->
<!--                            </div>-->
<!--                            <button class="bookNowBtn">احجز الآن</button>-->
<!--                            <div class="user_social clearfix">-->
<!--                                <a href="https://www.facebook.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.twitter.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.pinterest.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.linkedin.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                    </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--                <div class="col-sm-3">-->
<!--                    <div class="one_vote">-->
<!--                        <form method="" action="">-->
<!--                            <img src="--><?//= IMG ?><!--votes/2.jpg" />-->
<!--                            <h3>خبيرة المكياج سلمى عادل</h3>-->
<!--                            <div class="stars clearfix">-->
<!--                                <input class="star star-5" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-5"></label>-->
<!--                                <input class="star star-4" name="star" type="radio" disabled="" checked="">-->
<!--                                <label class="star" for="star-4"></label>-->
<!--                                <input class="star star-3" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-3"></label>-->
<!--                                <input class="star star-2" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-2"></label>-->
<!--                                <input class="star star-1" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-1"></label>-->
<!--                            </div>-->
<!--                            <div class="voteAddress">-->
<!--                                <h2>العنوان</h2>-->
<!--                                <p>القاهرة الجديدة ، مصر</p>-->
<!--                                <p class="voteAddress_num">01234567891 - 01234567891</p>-->
<!--                            </div>-->
<!--                            <button class="bookNowBtn">احجز الآن</button>-->
<!--                            <div class="user_social clearfix">-->
<!--                                <a href="https://www.facebook.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.twitter.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.pinterest.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.linkedin.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                    </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--                <div class="col-sm-3">-->
<!--                    <div class="one_vote">-->
<!--                        <form method="" action="">-->
<!--                            <img src="--><?//= IMG ?><!--votes/3.jpg" />-->
<!--                            <h3>خبيرة المكياج يمنى شافعي</h3>-->
<!--                            <div class="stars clearfix">-->
<!--                                <input class="star star-5" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-5"></label>-->
<!--                                <input class="star star-4" name="star" type="radio" disabled="" checked="">-->
<!--                                <label class="star" for="star-4"></label>-->
<!--                                <input class="star star-3" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-3"></label>-->
<!--                                <input class="star star-2" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-2"></label>-->
<!--                                <input class="star star-1" name="star" type="radio" disabled="">-->
<!--                                <label class="star" for="star-1"></label>-->
<!--                            </div>-->
<!--                            <div class="voteAddress">-->
<!--                                <h2>العنوان</h2>-->
<!--                                <p>القاهرة الجديدة ، مصر</p>-->
<!--                                <p class="voteAddress_num">01234567891 - 01234567891</p>-->
<!--                            </div>-->
<!--                            <button class="bookNowBtn">احجز الآن</button>-->
<!--                            <div class="user_social clearfix">-->
<!--                                <a href="https://www.facebook.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.twitter.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.pinterest.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                                <a href="https://www.linkedin.com" class="custom_social">-->
<!--                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>-->
<!--                                </a>-->
<!--                            </div>-->
<!--                    </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!---->

<?php $this->load->view('footer'); ?>
<script>
    $('body').on('change', '#select-city', function () {
        var row_id = $(this).val();
        var data_ajax = {};
        data_ajax.city_id = row_id;
        $.ajax({
            url: '<?= site_url()?>getDistrict',
            type: 'GET',
            data: data_ajax,
            success: function (data) {
                $('#select-district').html(data);
            }
        });
    })
    </script>
<script>
    $(".select_all").click(function () {
        var allCheckboxsInside = $(this).parent().parent().find('.checkInput');
        allCheckboxsInside.prop("checked", !allCheckboxsInside.prop("checked"));
    });
</script>
<script>
    function rate_event(doc_id, rate)
    {
        swal('برجاء تسجيل الدخول أولًا والدخول لصفحة مقدم الخدمة للتقييم !');

    }

</script>