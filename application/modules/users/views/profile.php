<?php $this->load->view("header"); ?>
<!--fancybox files-->
<link rel="stylesheet" type="text/css" href="<?= CSS; ?>jquery.fancybox.css" media="screen"/>
<script type="text/javascript" src="<?= JS; ?>jquery.fancybox.js"></script>
<!--fancybox files-->
<div id="headerwrap" style="background: url(<?= HEADER_IMG . rand(1, 12) . ".jpg" ?>) center center; background-size: cover;min-height: 470px;">
    <div class="container">
        <div class="row">
            <script>
                function rate_event(doc_id, rate)
                {
                    <?php if ($this->session->userdata('userid')): ?>
                    $.post("<?= site_url(); ?>users/insert_rates", {company_id: doc_id, rate: rate}, function (data) {
                    });
                    <?php else: ?>
                    swal('برجاء تسجيل الدخول أولًا !');
                    <?php $disabled = "disabled"; ?>
                    <?php endif; ?>
                }

            </script>
            <div class="col-md-12 header_wrap2">
                <h3 ><?= $comp_data->comp_name ?></h3>
                <?php if (!$this->session->userdata('userid')): ?>
                    <?php
                    switch (@$rating_avg) {
                        case '1':
                            $checked1 = "checked";
                            break;
                        case '2':
                            $checked2 = "checked";
                            break;
                        case '3':
                            $checked3 = "checked";
                            break;
                        case '4':
                            $checked4 = "checked";
                            break;
                        case '5':
                            $checked5 = "checked";
                            break;
                    }
                    ?>
                <?php elseif ($this->session->userdata('userid')): ?>
                    <?php
                    switch (@$user_rate[0]['rate']) {
                        case '1':
                            $checked1 = "checked";
                            break;
                        case '2':
                            $checked2 = "checked";
                            break;
                        case '3':
                            $checked3 = "checked";
                            break;
                        case '4':
                            $checked4 = "checked";
                            break;
                        case '5':
                            $checked5 = "checked";
                            break;
                    }
                    ?>
                <?php endif; ?>
                <form class="stars clearfix">
                    <input class="star star-5" id="star-5-<?= $comp_data->comp_id; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked5; ?>>
                    <label class="star star-5" for="star-5-<?= $comp_data->comp_id; ?>" onclick="rate_event(<?= $comp_data->comp_id; ?>, 5)"></label>
                    <input class="star star-4" id="star-4-<?= $comp_data->comp_id; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked4; ?>>
                    <label class="star star-4" for="star-4-<?= $comp_data->comp_id; ?>" onclick="rate_event(<?= $comp_data->comp_id; ?>, 4)"></label>
                    <input class="star star-3" id="star-3-<?= $comp_data->comp_id; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked3; ?>>
                    <label class="star star-3" for="star-3-<?= $comp_data->comp_id; ?>" onclick="rate_event(<?= $comp_data->comp_id; ?>, 3)"></label>
                    <input class="star star-2" id="star-2-<?= $comp_data->comp_id; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked2; ?>>
                    <label class="star star-2" for="star-2-<?= $comp_data->comp_id; ?>" onclick="rate_event(<?= $comp_data->comp_id; ?>, 2)"></label>
                    <input class="star star-1" id="star-1-<?= $comp_data->comp_id; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked1; ?>>
                    <label class="star star-1" for="star-1-<?= $comp_data->comp_id; ?>" onclick="rate_event(<?= $comp_data->comp_id; ?>, 1)"></label>
                </form>
            </div>
        </div><!-- /row -->
    </div> <!-- /container -->
</div>


<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 work-pro">
                <?php if (isset($slider_imgs) AND !empty($slider_imgs)):?>
                <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"/></i>أعمالي</h1>
                <?php endif;?>
                <div id="appended_items">
                    <div class="load_more_imgs_div">
                    <?php foreach($slider_imgs as $key => $img): ?>
                        <?php
                            if($key>4){
                                break;
                            }
                        ?>
                        <a class="fancybox-buttons work-pro-ma" rel="fancybox-buttons" href="<?= UPLOADS . "company_protofolio/".$img->img_name ; ?>" title="<?= $comp_data->comp_name?>">
                            <img src="<?= UPLOADS . "company_protofolio/".$img->img_name; ?>"/>
                        </a>
                        <?php
                            unset($slider_imgs[$key]);
                        ?>
                    <?php endforeach; ?>


                    </div>
                    <!-- Remon Please Append Items Here -->
                    <div class="clear"></div>
                </div>
            </div>
            <div class="clear"></div>
            <?php if(count($slider_imgs) > 4): ?>
                <div class="loadmore-btn">
                    <button class="updateInput load_more_imgs" data-loadimgs="<?php echo htmlspecialchars(json_encode($slider_imgs), ENT_QUOTES, 'UTF-8')?>">المزيد من الاعمال</button>
                </div>
            <?php endif; ?>

        </div>
    </div>
</div>

<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 work-pro">
                <?php if (isset($comp_services_data) AND !empty($comp_services_data)):?>
                    <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"/></i>الخدمات</h1>
                <?php endif;?>
                <?php if (isset($comp_services_data)):?>
                <div id="appended_items">
                    <div class="load_more_imgs_div">
                        <?php foreach($comp_services_data as $key => $value): ?>
                            <div class="col-md-4">
                                <div class="one_box">
                                    <div class="one_box_title">
                                        <h1 style="color: #FFFEEF"><?php echo $value->service_name?></h1>
                                    </div>
                                    <div class="he-wrap tpl6">
                                            <img src="<?php echo SERVICES_IMAGES . $value->service_image?>" alt="<?php echo $value->service_name?>" title="<?php echo $value->service_name?>">
                                    </div>
                                    <div class="one_box_title">
                                        <h1 style="color: #FFFEEF"><?php echo $value->service_price?></h1>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- Remon Please Append Items Here -->
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</div>

<?php $time=array(
    '12-am'=>'12 صباحا',
    '1-am'=>'1 صباحا',
    '2-am'=>'2 صباحا',
    '3-am'=>'3 صباحا',
    '4-am'=>'4 صباحا',
    '5-am'=>'5 صباحا',
    '6-am'=>'6 صباحا',
    '7-am'=>'7 صباحا',
    '8-am'=>'8 صباحا',
    '9-am'=>'9 صباحا',
    '10-am'=>'10 صباحا',
    '11-am'=>'11 صباحا',
    '12-pm'=>'12 مساءً',
    '1-pm'=>'1 مساءً',
    '2-pm'=>'2 مساءً',
    '3-pm'=>'3 مساءً',
    '4-pm'=>'4 مساءً',
    '5-pm'=>'5 مساءً',
    '6-pm'=>'6 مساءً',
    '7-pm'=>'7 مساءً',
    '8-pm'=>'8 مساءً',
    '9-pm'=>'9 مساءً',
    '10-pm'=>'10 مساءً',
    '11-pm'=>'11 مساءً'
)?>
<div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 work-pro">
                <?php if (isset($working_hours->days) AND !empty($working_hours->days)):?>
                    <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"/></i>ايام العمل</h1>
                <?php endif;?>
                <?php if(isset($working_hours)): ?>
                <div id="appended_items">
                    <div class="load_more_imgs_div" >
                        <?php foreach ($working_hours->days as $key=>$value):?>
                            <div class="col-md-4"style="padding-bottom: 8px;">
                                <div class="one_box" >
                                    <div class="one_box_title">
                                        <h1 style="color: #FFFEEF"><?php echo $value ?></h1>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>
                    <!-- Remon Please Append Items Here -->
                    <div class="clear"></div>
                </div>
                <?php endif; ?>
            </div>

            <div class="clear"></div>
        </div>
    </div>
</div>


    <div>
        <div class="container">
            <div class="row">
                <?php if (isset($working_hours) AND !empty($working_hours->days)):?>
                <div class="col-md-12 work-pro">

                        <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"/></i>فترات العمل </h1>

                    <div id="appended_items">
                        <div class="load_more_imgs_div" >
                            <div class="col-md-4"style="padding-bottom: 8px;">
                                <div class="one_box" >
                                    <div class="one_box_title">
                                        <?php echo @$time[$working_hours->timw1_from1]." الى ".$time[$working_hours->timw1_from2]; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="load_more_imgs_div" >
                            <div class="col-md-4"style="padding-bottom: 8px;">
                                <div class="one_box" >
                                    <div class="one_box_title">
                                        <?php echo @$time[$working_hours->timw2_from1]." الى ".$time[$working_hours->timw2_from2]; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Remon Please Append Items Here -->
                        <div class="clear"></div>
                    </div>
                </div>
                <?php endif;?>

                <div class="clear"></div>
            </div>
        </div>
    </div>








    <div>
    <div class="container">
        <div class="row">
            <div class="contact-with-her">
                <h1><i><img src="<?= IMG; ?>header_wrap_icon.png"/></i>التواصل</h1>
                <div>
                    <div class="cwh-6">
                        <?php if (!empty($comp_data->comp_email)):?>
                            <div class="col-md-12" style="text-align: center;">
                                <span style="color: #fff;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x "></i><i class="fa fa-envelope fa-stack-1x" style="color: #451722"></i></span></span>
                                <p class="p_desc p_desccenter" style="text-align: center;"><?= $comp_data->comp_email ?></p>
                            </div>
                        <?php endif;?>
                        <div class="clear"></div>
                    </div>
                    <?php if (!empty($comp_data->comp_facebook)):?>
                        <div class="col-md-3" style="text-align: center;">
                            <a href="<?= $comp_data->comp_facebook ?>" target="_blank"><span style="color: #fff;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x "></i><i class="fa fa-facebook fa-stack-1x" style="color: #451722"></i></span></span></a>
                        </div>
                    <?php endif;?>
                    <?php if (!empty($comp_data->comp_twitter)):?>
                        <div class="col-md-3" style="text-align: center;">
                            <a href="<?= $comp_data->comp_twitter ?>" target="_blank">  <span style="color: #fff;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x "></i><i class="fa fa-twitter fa-stack-1x" style="color: #451722"></i></span></span></a>
                        </div>
                    <?php endif;?>
                    <?php if (!empty($comp_data->comp_linkedin)):?>
                        <div class="col-md-3" style="text-align: center;">
                            <a href="<?= $comp_data->comp_linkedin ?>" target="_blank"> <span style="color: #fff;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x "></i><i class="fa fa-linkedin fa-stack-1x" style="color: #451722"></i></span></span></a>
                        </div>
                    <?php endif;?>
                    <?php if (!empty($comp_data->comp_pinterest)):?>
                        <div class="col-md-3" style="text-align: center;">
                            <a href="<?= $comp_data->comp_pinterest ?>" target="_blank"> <span style="color: #fff;"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x "></i><i class="fa fa-pinterest-p fa-stack-1x" style="color: #451722"></i></span></span></a>
                        </div>
                    <?php endif;?>
                    <div class="clear"></div>
                </div>
            </div>


                <div class="rate-her">
                    <h3>يمكنك الحجز من هنا</h3>
                    <br />
                    <?php if ($this->session->userdata('userid') && $this->session->userdata('user_type') == "user"):?>
                        <div class="stars clearfix">
                            <button class="res_button btn btn-danger btn-lg" data-toggle="modal" style="margin-right: -15px; width: 125%" data-target=".bd-example-modal-lg">احجز</button>
                        </div>
                    <?php else: ?>
                        <div class="stars clearfix">
                            <button class="res_button btn btn-danger btn-lg" data-toggle="modal" style="margin-right: -15px; width: 125%" data-target=".bd-example-modal-lg" disabled>سجل دخولك للحجز</button>
                        </div>
                    <?php endif; ?>
                    <div class="string" style="display: none">
                        <h3>تم الحجز سيتم إرسال رسالة تأكيدية علي هاتفك!</h3>
                    </div>

                </div>

            <br /><br /><br />
            <?php if (!empty($comp_data->lat) AND !empty($comp_data->lng)): ?>
            <div class="map-border">

                    <label for="">موقعى علي الخريطة</label>

                <div class="map-border">
                    <div id="map"></div>
                </div>

            </div>
        <?php endif; ?>




            <!-- Large modal -->

            <div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">اختر موعد الحجز</h5>
                            <button type="button" style="margin-top: -18px;" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>

                        <div class="modal-body">
                            <h3 style="color: red" class="modal-error"></h3>
                            <div class="form-group">
                                <div class="col-md-12 form-inline">
                                    <label class="col-md-12">
اختار التاريخ                                        <input type="date" required class="form-control reservation_data">
                                    </label>
                                 </div>
                                <div class="col-md-12 form-inline">
                                    <div class="row">
                                        <h3>الفتره</h3>
                                        <div class="col-md-6 form-inline">
                                            <label>
                                                <input type="radio"  name="timeRadio" value="<?php echo $time[$working_hours->timw1_from1]. "_". $time[$working_hours->timw1_from2]; ?>" class="radioTime" style="margin-left: 10px;">
                                                <?php echo $time[$working_hours->timw1_from1]." الى ".$time[$working_hours->timw1_from2]; ?>
                                            </label>
                                        </div>
                                        <div class="col-md-6 form-inline">
                                            <label>
                                                <input type="radio"  name="timeRadio" value="<?php echo $time[$working_hours->timw2_from1]. "_" .$time[$working_hours->timw2_from2]; ?>" class="radioTime" style="margin-left: 10px;">
                                                <?php echo $time[$working_hours->timw2_from1]." الى ".$time[$working_hours->timw2_from2]; ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" value="<?=$comp_data->comp_id?>" id="com_id">
                                <input type="hidden" value="<?=$this->session->userdata('userid')?>" id="user_id">
                                <div class="col-md-12 form-inline">
                                    <div class="row">
                                        <h3>اليوم</h3>
                                        <?php foreach ($working_hours->days as $key=>$value):?>
                                        <div class="col-md-12 form-inline">
                                            <label>
                                                <input type="radio"  name="dayRadio" value="<?php echo $value ?>" class="reservation_data_class" style="margin-left: 10px;">
                                                <?php echo $value ?>
                                            </label>
                                        </div>
                                       <?php endforeach;?>
                                    </div>
                                </div>

                                <?php if(isset($comp_services_data)): ?>
                                <div class="col-md-12 form-inline">
                                    <div class="row">
                                        <h3>الخدمات</h3>
                                        <?php foreach($comp_services_data as $key => $value): ?>
                                            <div class="col-md-12 form-inline">
                                                <div class="checkbox-inline col-md-6">
                                                    <label>
                                                        <?php echo $value->service_name . "  ".$value->service_price." جنيه"?>
                                                        <input style="display:block;float: right; margin-top: -22px; "type="checkbox" name="services[]" data-price="<?php echo $value->service_price?>" value="<?php echo $value->service_id ?>">
                                                    </label>
                                                </div>
                                            </div>
                                        <?php endforeach;?>
                                    </div>
                                </div>
                                <?php endif; ?>




                            </div>
                        </div>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">اغلاق</button>
                            <button type="button" class="btn btn-primary save_res">حجز</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--

 Add fancyBox
<link rel="stylesheet" type="text/css" href="http://projects.xcodeapps.net/al-aroosa/assets/js/jquery.fancybox.css">
<link rel="stylesheet" type="text/css" href="http://projects.xcodeapps.net/al-aroosa/assets/js/thumbs.css">
<link rel="stylesheet" type="text/css" href="http://projects.xcodeapps.net/al-aroosa/assets/js/core.css">
<link rel="stylesheet" type="text/css" href="http://projects.xcodeapps.net/al-aroosa/assets/js/fullscreen.css">
<link rel="stylesheet" type="text/css" href="http://projects.xcodeapps.net/al-aroosa/assets/js/slideshow.css">

<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/jquery.fancybox.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/thumbs.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/core.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/fullscreen.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/slideshow.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/guestures.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/hash.js"></script>
<script type="text/javascript" src="http://projects.xcodeapps.net/al-aroosa/assets/js/media.js"></script>-->

<script>
    function initMap() {
        var uluru = {lat: <?php echo @$comp_data->lat ; ?>, lng: <?php echo @$comp_data->lng ; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    $(document).ready(function () {
        $('.fancybox-buttons').fancybox({
            openEffect: 'elastic',
            closeEffect: 'elastic',
            prevEffect: 'none',
            nextEffect: 'none',
            closeBtn: true,
            helpers: {
                title: {
                    type: 'inside'
                },
                buttons: {}
            },
            afterLoad: function () {
                this.title = '';
            },
        });


        $(".load_more_imgs").click(function () {

            var imgs=$(this).attr("data-loadimgs");
            imgs=JSON.parse(imgs);

            var iteration=0;
            $.each(imgs,function (i,img) {
                if(iteration>4){
                    return false;
                }

                var html='<a class="fancybox-buttons work-pro-ma" rel="fancybox-buttons" href="<?= site_url("uploads/company_protofolio/") ; ?>'+img.img_name+'profileimg03.jpg" title="'+img.img_title+'">';
                    html+='<img src="<?= site_url("uploads/company_protofolio/"); ?>'+img.img_name+'"/>';
                html+='</a>';
                console.log(html);

                $(".load_more_imgs_div").append(html);

                delete imgs[i];
                iteration++;
            });


            if(JSON.stringify(imgs)=="{}"){
                $(this).hide();
            }
            else{
                $(this).attr("data-loadimgs",JSON.stringify(imgs));
            }

            return false;

        });
    });
</script>
<script>
    $(function () {
        $('body').on('click', '.save_res', function () {

            var data_ajax = {};
            data_ajax.com_id = $('#com_id').val();
            data_ajax.user_id = $('#user_id').val();
            data_ajax.time = $('.radioTime:checked').val();
            data_ajax.date = $('.reservation_data').val();
            data_ajax.day = $('.reservation_data_class:checked').val();
            var val = [];
            var price=0;
            $(':checkbox:checked').each(function(i){
                val[i] = $(this).val();
                price+=$(this).data('price');
            });
            data_ajax.price=price;
            data_ajax.services_id=JSON.stringify(val);

            if (data_ajax.date != "" && data_ajax.time != "" && data_ajax.services_id !="[]" && data_ajax.day != "" &&  data_ajax.time != undefined && data_ajax.day != undefined)
            {
                $.ajax({
                    url: "<?= site_url()?>" + 'resarvation',
                    type: 'POST',
                    data: data_ajax,
                    success: function (data) {
                        $('.modal').modal('hide');
                        $('.res_button').hide();
                        $('.string').show();

                    }
                });

                $.ajax({
                    url: "<?= site_url()?>" + 'sms_confirmation',
                    type: 'POST',
                    data: data_ajax
                });
            }
            else
            {
                $('.modal-error').html("يرجي اختيار جميع البيانات");
            }

        })
    })

</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCN0nJmqgTxrTo9Idz45wI9r-4-4pNvRKM&callback=initMap"></script>
<?php $this->load->view("footer"); ?>