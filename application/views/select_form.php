<?php
/**
 * Created by PhpStorm.
 * User: todary
 * Date: 28/04/17
 * Time: 03:36 م
 */
?>
<div class="col-md-12">
    <div class="form-group">
        <label for="name"> <?php echo $title?> </label>
        <select class="form-control <?php echo $select_class?>" name="<?php echo $name?>" <?php if($name == "city_select") echo "required"?>>
            <option value=""><?php echo $title?></option>
            <?php if(!empty($datas)):?>
            <?php foreach ($datas as $key => $data): ?>
                <option value="<?= $data->{$field_id} ?>"><?= $data->{$field_name} ?></option>
            <?php endforeach; ?>
            <?php else:?>
                <option value=""> ﻻ يوجد   </option>
            <?php endif;?>

        </select>
    </div>
</div>
<div class="<?php echo $class?>" ></div>