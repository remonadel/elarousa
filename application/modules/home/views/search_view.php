<?php $this->load->view('header'); ?>

<!-- *****************************************************************************************************************
 HEADERWRAP
 ***************************************************************************************************************** -->
<div id="headerwrap" style="background: url(<?= HEADER_IMG . rand(1, 12) . ".jpg" ?>) center center; background-size: cover;min-height: 470px;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 header_wrap2">
                <h1><i><img src="<?= ASSETS ?>img/header_wrap_icon.png" /></i><?=$search_text ?></h1>
            </div>
        </div><!-- /row -->
    </div> <!-- /container -->
</div><!-- /headerwrap -->

<div id="searchResult">
    <div class="container">
        <div class="row centered">
            <div class="col-md-3">
                <div class="result_filter" id="service_filter">
                    <div class="result_filter_head clearfix">
                        <p>الخدمة</p>
                        <p class="select_all">الكل</p>
                    </div>
                    <?php $i=1; foreach ($services as $makeup_service): ?>
                    <div class="checkedRow clearfix">
                        <div class="right_filter clearfix">
                            <input type="checkbox" class="checkInput service" value="<?=$makeup_service['service_id']?>" name="service[]" id="input<?=$i?>" />
                            <label for="input<?=$i?>"></label>
                            <label class="checkbox_name"> <?= $makeup_service['service_name'] ?></label>
                        </div>
                        <div class="left_filter"><span><?=$makeup_service['count'] ?></span></div>
                    </div>
                    <?php $i++; endforeach; ?>

                </div>
                <div class="result_filter" id="city_filter">
                    <div class="result_filter_head clearfix">
                        <p>المحافظة</p>
                    </div>
                    <?php $j=1; foreach ($cities as $city): ?>
                    <div class="checkedRow clearfix">
                        <div class="right_filter clearfix">
                            <input type="checkbox" class="checkInput city_checkbox" value="<?= $city['city_id']?>" id="inputOne<?=$j?>" />
                            <label for="inputOne<?=$j?>"></label>
                            <label class="checkbox_name"><?=$city['city_name'] ?></label>
                        </div>
                        <div class="left_filter"><span><?= $city['count'] ?></span></div>
                    </div>
                    <?php $j++; endforeach; ?>

                </div>
                <div class="result_filter" id="area_filter">
                    <div class="result_filter_head clearfix">
                        <p>المنطقة</p>
                    </div>
                    <?php $l=1; foreach ($districts as $district): ?>
                    <div class="checkedRow clearfix">
                        <div class="right_filter clearfix">
                            <input type="checkbox" class="checkInput" value="<?= $district['district_id']?>" id="inputOne1<?=$l?>" />
                            <label for="inputOne1<?=$l?>"></label>
                            <label class="checkbox_name"> <?= $district['district_name'] ?></label>
                        </div>
                        <div class="left_filter"><span><?=$district['count']?></span></div>
                    </div>
                    <?php $l++; endforeach; ?>

                </div>
            </div>

            <?php if (isset($companies) AND !empty($companies)): ?>
            <div class="col-md-9">
                <?php foreach ($companies as $company): ?>
                    <?php
                    $checked1 = "";
                    $checked2 = "";
                    $checked3 = "";
                    $checked4 = "";
                    $checked5 = "";
                    switch (@$company['comp_rate']) {
                        case '1':
                            $checked1 = "checked";
                            break;
                        case '2':
                            $checked2 = "checked";
                            break;
                        case '3':
                            $checked3 = "checked";
                            break;
                        case '4':
                            $checked4 = "checked";
                            break;
                        case '5':
                            $checked5 = "checked";
                            break;
                    }
                    $disabled = "disabled";
                    ?>
                <div class="col-sm-4">
                    <div class="one_vote">
                        <?php if (! empty($company['comp_image'])): ?>
                            <img src="<?= COMPANY_PROFILE . $company['comp_image'] ?>" />
                            <?php else: ?>
                            <img src="<?= IMG . "03-Profile_02.jpg" ?>" />
                        <?php endif; ?>
                            <h3><?= $company['comp_name']?></h3>
                        <form method="" action="">
                            <div class="stars clearfix">
                                <input class="star star-5" id="star-5-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked5; ?>>
                                <label class="star star-5" for="star-5-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 5)"></label>
                                <input class="star star-4" id="star-4-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked4; ?>>
                                <label class="star star-4" for="star-4-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 4)"></label>
                                <input class="star star-3" id="star-3-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked3; ?>>
                                <label class="star star-3" for="star-3-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 3)"></label>
                                <input class="star star-2" id="star-2-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked2; ?>>
                                <label class="star star-2" for="star-2-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 2)"></label>
                                <input class="star star-1" id="star-1-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked1; ?>>
                                <label class="star star-1" for="star-1-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 1)"></label>
                            </div>
                        </form>
                            <div class="voteAddress">
                                <h2>العنوان</h2>
                                <p>
                                <?php if(isset($company['district_name']) AND !empty($company['district_name'])): ?>
                                    <?= @$company['district_name'] . "، "?>
                                <?php endif; ?>
                                <?= @$company['city_name'] ?></p>
                            </div>
                            <a href="<?= site_url().'profile/'.$company['comp_id']?>"> <button class="bookNowBtn">احجز الآن</button></a>
                            <div class="user_social clearfix">
                                <?php if(isset($company['comp_facebook']) AND !empty($company['comp_facebook'])): ?>
                                    <a href="<?=$company['comp_facebook'] ?>" class="custom_social">
                                        <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>
                                    </a>
                                <?php endif; ?>
                                <?php if(isset($company['comp_twitter']) AND !empty($company['comp_twitter'])): ?>
                                    <a href="<?=$company['comp_twitter'] ?>" class="custom_social">
                                        <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>
                                    </a>
                                <?php endif; ?>
                                <?php if(isset($company['company_pinterest']) AND !empty($company['company_pinterest'])): ?>
                                    <a href="<?=$company['company_pinterest'] ?>" class="custom_social">
                                        <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>
                                    </a>
                                <?php endif; ?>
                                <?php if(isset($company['comp_linkedin']) AND !empty($company['comp_linkedin'])): ?>
                                    <a href="<?=$company['comp_linkedin'] ?>" class="custom_social">
                                        <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>
                                    </a>
                                <?php endif; ?>
                            </div>
                    </div>

                </div>

        <?php endforeach; ?>
                <div class="clear"></div>
                <div class="paging">
                    <ul class="pagination">
                        <?php if (isset($pagination)): echo $pagination; endif;?>
                    </ul>
                </div>
            </div>

            <?php else: ?>
            لا يوجد نتائج لبحثك من فضلك قم بالمحاولة مرة آخري!
            <?php endif; ?>
        </div>
    </div>
</div>

<!-- *****************************************************************************************************************
 Big Bg Section
 ***************************************************************************************************************** -->
<?php  if (!$this->session->userdata('userid')): ?>
    <div id="BigBgSection">
        <div class="container centered">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <h1>أكبر قاعدة بيانات لمقدمى خدمات العروسة</h1>
                    <p> سجل بياناتك وتابع حجوزاتك اونلاين وخليك اقرب</p>
                    <a href="<?= site_url() . 'register' ?>" id="home-page-register-btn" class="register_btn Reg_Big_sec">تسجيل مقدم خدمة</a>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<!-- *****************************************************************************************************************
 HOTTEST VOTES
 ***************************************************************************************************************** -->
<?php if (isset($top_rated_companies) AND !empty($top_rated_companies)): ?>
<div id="hottestVotes">
    <div class="container">
        <div class="row centered">
            <h1><i><img src="<?= ASSETS ?>img/header_wrap_icon.png"></i>الأعلى تقييماً</h1>
            <div class="col-md-12">
                <?php foreach ($top_rated_companies as $company): ?>
                <?php
                $checked1 = "";
                $checked2 = "";
                $checked3 = "";
                $checked4 = "";
                $checked5 = "";
                switch (@$company['comp_rate']) {
                    case '1':
                        $checked1 = "checked";
                        break;
                    case '2':
                        $checked2 = "checked";
                        break;
                    case '3':
                        $checked3 = "checked";
                        break;
                    case '4':
                        $checked4 = "checked";
                        break;
                    case '5':
                        $checked5 = "checked";
                        break;
                }
                $disabled = "disabled";
                ?>
                <div class="col-sm-3">
                    <div class="one_vote">

                        <img src="<?= COMPANY_PROFILE . $company['comp_image'] ?>" />
                        <h3><?= $company['comp_name']?></h3>
                            <form method="" action="">
                                <div class="stars clearfix">
                                    <input class="star star-5" id="star-5-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked5; ?>>
                                    <label class="star star-5" for="star-5-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 5)"></label>
                                    <input class="star star-4" id="star-4-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked4; ?>>
                                    <label class="star star-4" for="star-4-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 4)"></label>
                                    <input class="star star-3" id="star-3-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked3; ?>>
                                    <label class="star star-3" for="star-3-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 3)"></label>
                                    <input class="star star-2" id="star-2-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked2; ?>>
                                    <label class="star star-2" for="star-2-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 2)"></label>
                                    <input class="star star-1" id="star-1-<?= $company['comp_id']; ?>" name="star" type="radio" <?= @$disabled . " " . @$checked1; ?>>
                                    <label class="star star-1" for="star-1-<?= $company['comp_id']; ?>" onclick="rate_event(<?= $company['comp_id']; ?>, 1)"></label>
                                </div>
                            </form>
                            <div class="voteAddress">
                                <h2>العنوان</h2>
                                <p><?= @$company['district_name'] . "، ". $company['city_name'] ?></p>
                            </div>
                        <a href="<?= site_url().'profile/'.$company['comp_id']?>"> <button class="bookNowBtn">احجز الآن</button></a>
                            <div class="user_social clearfix">
                                <?php if(isset($company['comp_facebook']) AND !empty($company['comp_facebook'])): ?>
                                <a href="<?= $company['comp_facebook'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                                <?php if(isset($company['comp_twitter']) AND !empty($company['comp_twitter'])): ?>
                                <a href="<?= $company['comp_twitter'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-twitter fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                                <?php if(isset($company['company_pinterest']) AND !empty($company['company_pinterest'])): ?>
                                <a href="<?= $company['company_pinterest'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-pinterest-p fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                                <?php if(isset($company['comp_linkedin']) AND !empty($company['comp_linkedin'])): ?>
                                <a href="<?= $company['comp_linkedin'] ?>" class="custom_social">
                                    <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-linkedin fa-stack-1x"></i></span>
                                </a>
                                <?php endif; ?>
                            </div>
                    </div>
                    </form>
                </div>
            <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php endif; ?>
<script>
    $(".select_all").click(function () {
        var allCheckboxsInside = $(this).parent().parent().find('.checkInput');
        allCheckboxsInside.prop("checked", !allCheckboxsInside.prop("checked"));
    });

    var service = getParameterByName('service')
    if (service != null)
    {
        services = service.split('-');
        $.each(services, function( index, value ) {
            $('#service_filter input:checkbox[value="' + value + '"]').prop('checked',true);
        });
    }

    var city = getParameterByName('city');
    if (city != null)
    {
        $('#city_filter input:checkbox[value="' + city + '"]').prop('checked',true);

    }
    var area = getParameterByName('area');
    if (area != null)
    {
        $('#area_filter input:checkbox[value="' + area + '"]').prop('checked',true);

    }
    $('#service_filter').click(function() {
        var allVals = [];
        $('#service_filter :checked').each(function() {
            allVals.push($(this).val());
        });
        var implodedArray = allVals.join('-');
        var url = document.URL;
        url = removeURLParameter(url,'service');
        window.location.replace( url + '&service=' + implodedArray);
    });

    $('#city_filter').click(function() {
        var Val = [];
        $('#city_filter :checked').each(function() {
            Val = ($(this).val());
        });
        var url = document.URL;
        url = removeURLParameter(url,'city');
        window.location.replace( url + '&city=' + Val);
    });

    $('#area_filter').click(function() {
        var Val = [];
        $('#area_filter :checked').each(function() {
            Val = ($(this).val());
        });
        var url = document.URL;
        url = removeURLParameter(url,'area');
        window.location.replace( url + '&area=' + Val);
    });


    function removeURLParameter(url, parameter) {
        //prefer to use l.search if you have a location/link object
        var urlparts= url.split('?');
        if (urlparts.length>=2) {

            var prefix= encodeURIComponent(parameter)+'=';
            var pars= urlparts[1].split(/[&;]/g);

            //reverse iteration as may be destructive
            for (var i= pars.length; i-- > 0;) {
                //idiom for string.startsWith
                if (pars[i].lastIndexOf(prefix, 0) !== -1) {
                    pars.splice(i, 1);
                }
            }

            url= urlparts[0]+'?'+pars.join('&');
            return url;
        } else {
            return url;
        }
    }

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }

</script>
<script>
    function rate_event(doc_id, rate)
    {
        swal('برجاء تسجيل الدخول أولًا والدخول لصفحة مقدم الخدمة للتقييم !');

    }

</script>

<?php $this->load->view('footer'); ?>
