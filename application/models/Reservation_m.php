<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */


class Reservation_m extends CI_Model
{
    protected $table_name = "reservations";
    protected $primary_key = "res_id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function save($data)
    {
        $this->db->insert($this->table_name,$data);
        return $this->db->insert_id();
    }

    public function getReservation($user_id)
    {
        return $this->db->get_where($this->table_name, array('user_id' => $user_id))->result();
    }

    public function get_all_for_first_reminder()
    {
        $this->db->where('first_reminder', 0);
        $this->db->where("DATE(date) = CURDATE()");
        $this->db->join('company', 'company.comp_id = reservations.comp_id', 'left');
        $this->db->join('users', 'users.user_id = reservations.user_id', 'left');
        $query = $this->db->get('reservations');
        return $query->result_array();
    }

    public function update_reminder_status($id, $reminder_no)
    {
        $this->db->where('res_id', $id);
        if ($reminder_no == 1){
            $this->db->update($this->table_name, array('first_reminder' => 1));
        }elseif ($reminder_no == 2){
            $this->db->update($this->table_name, array('second_reminder' => 1));
        }
    }


    public function getCompanyReservations($cond = "")
    {

        return $this->db->query(" 
          select 
          reserve.*,u.*,c.* 
          
          from reservations as reserve 
          left JOIN users as u on (reserve.user_id = u.user_id)
          left JOIN company as c on (reserve.comp_id = c.comp_id)
          $cond
          ")->result();

    }

    public function delete($res_id)
    {
        $this->db->query("delete from $this->table_name where  `res_id` = $res_id ");
    }





}