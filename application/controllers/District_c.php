<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */
class District_c extends CI_Controller
{

    public function __construct(){
        parent::__construct();
        $this->load->model("City_m");
        $this->load->model("District_m");
    }

    public function getDistrict() {
        $city_id = $this->input->get("city_id");
        $this->data['datas'] = $this->District_m->getDistrict($city_id);
        $this->data['title']='اختر المنطقة';
        $this->data['class']='district';
        $this->data['name']='district_select';
        $this->data['field']='district';
        $this->data['field_id']='district_id';
        $this->data['field_name']='district_name';
        $this->data['select_class']='district_select';
        $respons=$this->load->view("select_form", $this->data, true);
        echo $respons;
    }


}