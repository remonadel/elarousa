<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= IMG; ?>favicon.png">
    <title><?= (!isset($meta_title)?" العروسة | الدليل الأكبر لكل ما تحتاجه العروسة":( "العروسة | " . $meta_title)); ?></title>
    <meta name="keywords" content="<?= (!isset($meta_keywords)?"العروسة،خبراء تجميل، عرائس، مكياج،ميك اب ،خبراء تصفيف، شعر، تصفيف، تصفيف شعرك اب ،خبراء تصفيف، شعر، تصفيف، تصفيف شعر" : $meta_keywords); ?>"/>
    <meta name="description" content="<?= (!isset($meta_description)?" الدليل الأكبر لكل ما تحتاجه العروسة ":$meta_description); ?>">

    <!-- CSS -->
    <link href="<?= CSS; ?>bootstrap.css" rel="stylesheet">
    <link href="<?= CSS; ?>bootstrap-rtl.min.css" rel="stylesheet">
    <!-- Custom styles and there is custom.css file included in style.css -->
    <link href="<?= CSS; ?>style.css" rel="stylesheet">
    <link href="<?= CSS; ?>font-awesome.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?= CSS; ?>sweetalert.css" type="text/css" />

    <script src="<?= JS; ?>sweetalert.min.js"></script>
    <script src="<?= JS; ?>jquery.min.js"></script>
</head>
<body>
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container nav-container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?= site_url();?>">
                <img src="<?= IMG; ?>logo.png" alt="Al-arosa" title="Al-arosa" style="height: 100%" />
            </a>
        </div>
        <div class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="<?= site_url()  ?>">الرئيسية</a></li>
                <li><a href="<?= site_url() . 'page/about'?>" >عن الموقع</a></li>
                <li><a href="<?= site_url() . 'page/contact'?>" >إتصل بنا</a></li>
                <!--<li><a href="<?= site_url() . 'login'  ?>">دخول</a></li>-->
                <?php  if ($this->session->userdata('userid')): ?>
                    <?php  if ($this->session->userdata("user_type") == 'company'): ?>
                    <li> <a href="#" tabindex="0" data-container="body" data-toggle="popover" data-placement="bottom"
                            data-content="<?php if ($this->session->userdata("isactive") == 1): ?><a  href='<?= site_url() . 'profile/' . $this->session->userdata('userid') ?>' class='register_btn Reg_Big_sec'>الصفحة الشخصية</a><?php endif; ?>
                            <a  href='<?= site_url() . 'edit_profile_comp' ?>' class='register_btn Reg_Big_sec'>تعديل الصفحة الشخصية </a>
                            <a  href='<?= site_url() . 'reservations' ?>' class='register_btn Reg_Big_sec'>عرض الحجوزات  </a>
                            <a  href='<?= site_url() . 'logout' ?>' class='register_btn Reg_Big_sec'>تسجيل خروج</a>
                            "><?=$this->session->userdata('name') ?></a></li>
                    <?php else: ?>
                        <li> <a href="#" tabindex="0" data-container="body" data-toggle="popover" data-placement="bottom"
                                data-content="
                            <a  href='<?= site_url() . 'edit_profile_user' ?>' class='register_btn Reg_Big_sec'>تعديل الصفحة الشخصية </a>
                            <a  href='<?= site_url() . 'my_reservations' ?>' class='register_btn Reg_Big_sec'>حجوزاتى </a>
                            <a  href='<?= site_url() . 'logout' ?>' class='register_btn Reg_Big_sec'>تسجيل خروج</a>
                            "><?=$this->session->userdata('name') ?></a></li>
                    <?php endif; ?>

                <?php else: ?>
                    <li> <a href="#" tabindex="0" data-container="body" data-toggle="popover" data-placement="bottom"
                            data-content="<a  href='<?= site_url() . 'company_login' ?>' class='register_btn Reg_Big_sec'> مقدم خدمة</a>
                            <a href='<?= site_url() . 'user_login' ?>' class='register_btn Reg_Big_sec'> مستخدم</a>
                            ">دخول</a></li>
                <?php endif; ?>

            </ul>
        </div><!--/.nav-collapse -->
        <div class="header_social clearfix">
            <a href="https://www.facebook.com" class="custom_social">
                <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-facebook fa-stack-1x"></i></span>
            </a>
            <a href="https://www.instagram.com" class="custom_social">
                <span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x icon-background"></i><i class="fa fa-instagram fa-stack-1x"></i></span>
            </a>

            <?php  if (!$this->session->userdata('userid')): ?>
            <a tabindex="0" class="register_btn" data-container="body" data-toggle="popover" data-placement="bottom"
               data-content="<a href='<?= site_url() . 'register' ?>' class='register_btn Reg_Big_sec'>تسجيل مقدم خدمة</a>
                                  <a href='<?= site_url() . 'user_register' ?>' class='register_btn Reg_Big_sec'>تسجيل مستخدم</a>
                                ">تسجيل</a>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="wrapper">

    <!-- ads views -->
    <?= $this->load->view('160x600_banners'); ?>
    <?= $this->load->view('leaderboard'); ?>
    <!-- ads views -->

<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '1186216541507694',
            xfbml: true,
            version: 'v2.9'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
