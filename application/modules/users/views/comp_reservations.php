<?php $this->load->view("header"); ?>

    <link rel="stylesheet" href="//cdn.datatables.net/1.10.15/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>


    <div style="padding-top: 100px; min-height: 500px">
        <div class="container">
            <div class="row centered">
                <div class="col-md-12">

                    <?php if(count($reservations)): ?>

                        <table id="myTable" class="table table-responsive">

                            <thead>
                                <td>#</td>
                                <td>اسم المستخدم</td>
                                <td>وقت الحجز</td>
                                <td>يوم الحجز</td>
                                <td>تاريخ الحجز</td>
                                <td>تاريخ الانشاء</td>
                                <td>مسح</td>
                            </thead>
                            <tbody>
                            <?php foreach($reservations as $key => $reservation): ?>
                                <tr id="row_<?= $reservation->res_id ?>">
                                    <td><?= $key + 1; ?></td>
                                    <td><?= $reservation->user_name; ?></td>
                                    <td><?= $reservation->time; ?></td>
                                    <td><?= $reservation->day; ?></td>
                                    <td><?= $reservation->date; ?></td>
                                    <td><?= $reservation->created_at; ?></td>
                                    <td>
                                        <button class="btn btn-danger remove_reservation" data-id="<?= $reservation->res_id ?>">مسح</button>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>

                        <?php else: ?>
                        <div class="alert alert-info">لا توجد حجوزات حتي الأن</div>
                    <?php endif; ?>

                </div>
            </div>
        </div>
    </div>

<?php $this->load->view("footer"); ?>
    <script>
        $(function () {

            $('#myTable').DataTable();


            $('body').on('click', '.remove_reservation', function () {
                var id = $(this).attr('data-id');
                if(typeof(id) != "undefined" && id > 0)
                {
                    var this_element = $(this);
                    this_element.attr("disabled","disabled");
                    var row_id = $('#row_'+id);
                    var url = '<?= site_url('/'); ?>';
                    $.ajax({
                        url: url + 'remove_reservation',
                        type: 'POST',
                        data: {'id':id},
                        success: function (data) {
                            data = JSON.parse(data);
                            if(data.success == "success")
                            {
                                row_id.hide();
                                alert(" تم المسح بنجاح ");
                            }
                            else{
                                this_element.removeAttr("disabled");
                                alert(data.success);
                            }
                        }
                    });
                }


            });

        });
    </script>

