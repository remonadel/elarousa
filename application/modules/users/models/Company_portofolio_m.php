<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: todary
 * Date: 4/27/17
 * Time: 10:18 AM
 */


class Company_portofolio_m extends CI_Model
{
    protected $table_name = "company_portofolio";
    protected $primary_key = "img_id";
    protected $fields = "";
    protected $order_by = "";


    public function __construct()
    {
        parent::__construct();
    }

    public function save($data,$user_id)
    {
        $count=0;
        if (is_array($data))
        {
            foreach ($data as $key =>$value )
            {
                $this->db->insert($this->table_name,array(
                    'img_name'=>$value,
                    'comp_id' =>$user_id
                ));
                $count++;

            }
            if ($count==count($data))
                return true;
            else
                return false;

        }
        else
        {
            $this->db->insert($this->table_name,array(
                'img_name'=>$data,
                'comp_id' =>$user_id
            ));
            return $this->db->insert_id();
        }
    }


    public function getData($comp_id)
    {
        return $this->db->query("SELECT * FROM `$this->table_name` where `comp_id` = $comp_id")->result();
    }

    public function getImage($image_id)
    {
        return $this->db->query("SELECT * FROM `$this->table_name` where `img_id` = $image_id")->result();
    }


    public function save_new($data)
    {
        $this->db->insert($this->table_name, $data);
        return $this->db->insert_id();
    }


    public function update($id,$data)
    {
        $this->db->where($this->primary_key, $id);
        $this->db->update($this->table_name, $data);
    }


    public function delete($img_id)
    {
        $this->db->query("delete from $this->table_name where  `img_id` = $img_id ");
    }


}