<?php $this->load->view("header"); ?>

<div style="padding-top: 100px;">
    <div class="container">
        <div class="row centered">
            <div class="col-md-12">
                <form class="" action="register" method="post" enctype="multipart/form-data">
                    <h1 class="page_title"><i><img src="<?= IMG ?>header_wrap_icon.png"></i>أضف شركتك</h1>
                    <p class="page_desc">قم بتعبئة البيانات التالية بدقة واحصل على حساب مجاني في العروسة وادخل مباشرةً
                        إلى لوحة التحكم الخاصة بك وأضف المعلومات المطلوبة لتفعيل حسابك</p>
                    <?php if (isset($error)):
                        echo $error;
                    endif;
                    ?>
                    <div class="user_data">
                        <h1 class="page_title">أضف صورة شخصية</h1>
                        <div style="position: relative;height: 200px;" class="div_profile">
                            <img src="<?= IMG ?>avatar.png" style="position: absolute" id="profile"/>
                            <input type="file" name="profile" id="profilePhoto"
                                   style="opacity: 0; position: absolute; height: 200px;cursor: pointer;    width: 100%; " required>
                        </div>
                        <div class="form_data">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">الإسم</label>
                                    <input type="text" name="name" value="<?= @htmlspecialchars(trim($_POST['name'])) ?>" class="form-control" required autofocus/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">البريد الإلكتروني</label>
                                    <input type="email" name="email" value="<?= @htmlspecialchars(trim($_POST['email'])) ?>" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">اسم المستخدم</label>
                                    <input type="text" name="username" value="<?= @htmlspecialchars(trim($_POST['username'])) ?>" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">الرقم السرى</label>
                                    <input type="password" name="password" class="form-control password1" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group" id="confirm_password">
                                    <label for="name">تاكيد الرقم السرى</label>
                                    <div class="alert alert-danger" id="message_error" style="display: none">
                                        <strong>خطاء!</strong> الرقم التاكيدي غلط.
                                    </div>
                                    <input type="password" name="password" class="form-control password2 " required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">رقم الهاتف</label>
                                    <input type="tel" name="tel" value="<?= @htmlspecialchars(trim($_POST['tel'])) ?>" class="form-control" required/>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="name">حسابات التواصل الاجتماعي</label>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" value="<?= @htmlspecialchars(trim($_POST['social_facebook'])) ?>" name="social_facebook" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-facebook fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" value="<?= @htmlspecialchars(trim($_POST['social_twitter'])) ?>" name="social_twitter" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-twitter fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" value="<?= @htmlspecialchars(trim($_POST['social_linkedin'])) ?>" name="social_linkedin" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-linkedin fa-stack-1x"></i></span></span>
                                    </div>
                                    <div class="input-group input-group-unstyled">
                                        <input type="text" <?= @htmlspecialchars(trim($_POST['comp_pinterest'])) ?> name="comp_pinterest" class="form-control"
                                               placeholder="URL://"/>
                                        <span class="input-group-addon"><span class="fa-stack fa-lg"><i
                                                        class="fa fa-circle fa-stack-2x icon-background"></i><i
                                                        class="fa fa-pinterest-p fa-stack-1x"></i></span></span>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12 select">
                                <div class="form-group">
                                    <label for="name">اختار البلد</label>
                                    <select class="form-control country_select" name="country_select" required>
                                        <option value="">البلد</option>
                                        <?php foreach ($countries as $key => $country): ?>

                                            <option value="<?= $country->country_id ?>" <?php if (@$_POST['country_select'] == $country->country_id) echo "selected" ?>><?= $country->country_name ?></option>
                                        <?php endforeach; ?>

                                    </select>
                                </div>
                            </div>
                            <div class="city">
                            <?php if (isset($cities)): ?>
                                <div class="col-md-12 select">
                                    <div class="form-group">
                                        <label for="name">اختار المدينة</label>
                                        <select class="form-control city_select" name="city_select">
                                            <option>المدينة</option>
                                            <?php foreach ($cities as $key => $city): ?>
                                                <option <?php echo (($city->city_id == $_POST['city_select'])?"selected":"") ?> value="<?= $city->city_id ?>"><?= $city->city_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
                            <?php endif; ?>
                            </div>

                            <?php if (isset($districts)): ?>
                                <div class="col-md-12 select">
                                    <div class="form-group">
                                        <label for="name">اختار المنطقة</label>
                                        <select class="form-control district_select" name="district_select">
                                            <option>المنطقة</option>
                                            <?php foreach ($districts as $key => $district): ?>
                                                <option <?php echo (($district->district_id == $_POST['district_select'])?"selected":"") ?> value="<?= $district->district_id ?>"><?= $district->district_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <div class="divs_company">
                                <div class="col-md-12 categories_select">
                                    <div class="form-group">
                                        <label for="name">اختار التخصص</label>
                                        <select class="form-control" name="category_select" required>
                                            <option value="">التخصص</option>
                                            <?php foreach ($categories as $key => $category): ?>

                                                <option value="<?= $category->category_id ?>" <?php if (@$_POST['category_select'] == $category->category_id) echo "selected" ?>><?= $category->category_name ?></option>
                                            <?php endforeach; ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <div class="row">
                                        <div class="col-md-9" style="font-size: 16px">
                                            <p class="p_title">الخدمات</p>
                                            <?php foreach ($services as $key=>$value):?>
                                                <label class="checkbox-inline col-md-6" style="margin-right:0px;">
                                                    <input  style="display: block;margin-left: 10px;margin-top: -10px;" <?php if (@in_array($value->service_id, $_POST['services'])): ?>checked<?php endif;?> type="checkbox" class="select_service" name="services[]" value="<?= $value->service_id ?>" ><?=$value->service_name?>
                                                    <input class="form-control" type="number" value="<?= @$_POST['service_price_'.$value->service_id]?>" name="service_price_<?= $value->service_id ?>" placeholder="سعر الخدمة" style="margin-bottom: 9px;">
                                                </label>

                                            <?php endforeach;?>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 profile_fields">
                                    <div class="row">
                                        <div class="col-md-9" style="font-size: 16px">
                                            <p class="p_title">ايام العمل</p>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_all" >اختر الكل</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("السبت", $_POST['days']))?"checked":""?>  value="السبت">السبت</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("الاحد", $_POST['days']))?"checked":""?>  value="الاحد">الاحد</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("الاثنين", $_POST['days']))?"checked":""?>  value="الاثنين">الاثنين</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("الثلاثاء", $_POST['days']))?"checked":""?>  value="الثلاثاء">الثلاثاء</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("الاربعاء", $_POST['days']))?"checked":""?>  value="الاربعاء">الاربعاء</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("الخميس", $_POST['days']))?"checked":""?>  value="الخميس">الخميس</label>
                                            <label class="checkbox-inline col-md-9" style="margin-right:0px;">
                                                <input  style="display: block;margin-right: 5px;margin-top: -10px;" type="checkbox" class="select_service days-check" name="days[]" <?php echo(@in_array("الجمعه", $_POST['days']))?"checked":""?>  value="الجمعه">الجمعه</label>
                                            <?php $time=array(
                                                '12-am'=>'12 صباحا',
                                                '1-am'=>'1 صباحا',
                                                '2-am'=>'2 صباحا',
                                                '3-am'=>'3 صباحا',
                                                '4-am'=>'4 صباحا',
                                                '5-am'=>'5 صباحا',
                                                '6-am'=>'6 صباحا',
                                                '7-am'=>'7 صباحا',
                                                '8-am'=>'8 صباحا',
                                                '9-am'=>'9 صباحا',
                                                '10-am'=>'10 صباحا',
                                                '11-am'=>'11 صباحا',
                                                '12-pm'=>'12 مساءً',
                                                '1-pm'=>'1 مساءً',
                                                '2-pm'=>'2 مساءً',
                                                '3-pm'=>'3 مساءً',
                                                '4-pm'=>'4 مساءً',
                                                '5-pm'=>'5 مساءً',
                                                '6-pm'=>'6 مساءً',
                                                '7-pm'=>'7 مساءً',
                                                '8-pm'=>'8 مساءً',
                                                '9-pm'=>'9 مساءً',
                                                '10-pm'=>'10 مساءً',
                                                '11-pm'=>'11 مساءً'
                                            )?>
                                            <p class="p_title col-md-9">الفتره الاولى</p>
                                            <div class="col-md-5">
                                                <p class="p_title">من</p>
                                                <select class="form-control" name="timw1_from1">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php if (@$_POST['timw1_from1'] == $key) echo "selected" ?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <p class="p_title">الى</p>
                                                <select class="form-control" name="timw1_from2">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php if (@$_POST['timw1_from2'] == $key) echo "selected" ?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>

                                            <p class="p_title col-md-9">الفتره الثانيه</p>
                                            <div class="col-md-5">
                                                <p class="p_title">من</p>
                                                <select class="form-control" name="timw2_from1">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?>  <?php if (@$_POST['timw2_from1'] == $key) echo "selected" ?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                            <div class="col-md-5">
                                                <p class="p_title">الى</p>
                                                <select class="form-control" name="timw2_from2">
                                                    <option value="">من</option>
                                                    <?php foreach ($time as $key=>$value): ?>
                                                        <option value=<?php echo $key?> <?php if (@$_POST['timw2_from2'] == $key) echo "selected" ?>><?php echo $value?></option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-3"></div>
                                    </div>
                                </div>
                                <div class="col-md-12 portofolio">
                                    <h1 class="page_title" style="margin-top: 40px;">أضف صور لأعمالك (PORTOFOLIO)</h1>
                                    <div class="row uploader gallery">

                                    </div>
                                    <div id="upload">
                                        <span id="upload_file_name">No file selected</span>
                                        <i class="file_button_container fa fa-plus-circle"></i>
                                        <input type="file" name="images[]" id="filePhoto" multiple/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <hr>

                                <label for="">حدد مكانك علي الخريطة</label>
                                <?php
                                    echo generate_map_helper(0,0);
                                ?>
                            </div>
                            <div class="col-md-12">

                                    <label class="checkbox-inline col-md-9" style="margin-right:0px;    font-size: 14px;">
                                    <input  style="float:right;display: block; margin-top: -11px;" type="checkbox" required>
                                        أوافق علي الشروط والأحكام
                                    </label>


                            </div>
                            <div class="col-md-12">
                                <!-- HTML Example by Way2Tutorial.com -->
                                <div style="height:100px;width:100%;border:solid 2px orange;overflow:scroll;overflow-x:hidden;overflow-y:scroll;margin-bottom: 10px;">
                                    <p style="height:200px;">
                                        شروط الخدمة
                                        <br>                                        مقدمي الخدمة<br>
                                        يحق لموقع el3arousa.com قبول أو رفض إضافة أي ملف مقدم الخدمة بناء على معايير التقييم الداخلية. يحق لموقع el3arousa.com إنهاء وإزالة أي ملف شخصي ومحفظة لمقدم الخدمة في حالة خرق الشروط والأحكام. سوف يقوم موقع el3arousa.com
                                    <span style="color: red;text-decoration: underline">بتحصيل  10٪ عمولة  كمصاريف تسويق وادارة عن كل حجز على جميع الوظائف التي يتم دفعها و دفعها من خلال الموقع الإلكتروني مع العلم أن أول ثلاثة أشهر مجانا  </span>

                                        <br>
                                        <br>
                                    المحتوى الخاص بك.<br>
                                    في هذه البنود والشروط القياسية لموقع الويب، يعني "المحتوى الخاص بك" أي نص صوتي أو فيديو أو صور أو مواد أخرى تختار عرضها على هذا الموقع. من خلال عرض المحتوى الخاص بك، فإنك تمنح El3arousa.com ترخيصا غير حصري وغير قابل للإلغاء في جميع أنحاء العالم غير قابل للإلغاء، لاستخدامه، وتكييفه، ونشره، وتوزيعه في أي وجميع وسائل الإعلام. يجب أن يكون المحتوى الخاص بك لك ويجب ألا يكون غزو حقوق طرف ثالث. El3arousa.com تحتفظ بالحق في إزالة أي من المحتوى الخاص بك من هذا الموقع في أي وقت دون إشعار.
                                    </p>
                                </div>

                            </div>
                            <input type="submit" value="تسجيل مقدم خدمة" class="registerInput">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(".file_button_container").click(function () {
        $("#hidden_upload_input").trigger('click');
    });

    $('#hidden_upload_input').on('change', function () {
        var val = $(this).val();
        $(this).siblings('#upload_file_name').text(val);
    })
</script>

<script>

    $(function () {
        // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $($.parseHTML('<img class="col-md-4">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#filePhoto').on('change', function () {
            imagesPreview(this, 'div.gallery');
        });
    });

    $(function () {
        // Multiple images preview in browser
        var imagesPreview = function (input, placeToInsertImagePreview) {

            if (input.files) {
                var filesAmount = input.files.length;

                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();

                    reader.onload = function (event) {
                        $('#profile').attr('src', event.target.result).appendTo(placeToInsertImagePreview).css({
                            'height': '200px',
                            'width': '200px',
                            'border-radius': '200px'
                        });
                    }

                    reader.readAsDataURL(input.files[i]);
                }
            }

        };

        $('#profilePhoto').on('change', function () {
            imagesPreview(this, 'div.div_profile');
        });
    });

</script>

<script>
    $(function () {
        $('body').on('change', '.country_select', function () {
            var row_id = $(this).val();
            var data_ajax = {};
            data_ajax.country_id = row_id;
            $.ajax({
                url: '<?= site_url()?>getCity',
                type: 'GET',
                data: data_ajax,
                success: function (data) {
                    $('.city').html(data);
                }
            });
        })

        $('body').on('change', '.city_select', function () {
            var row_id = $(this).val();
            var data_ajax = {};
            data_ajax.city_id = row_id;
            $.ajax({
                url: '<?= site_url()?>getDistrict',
                type: 'GET',
                data: data_ajax,
                success: function (data) {
                    $('.district').html(data);
                }
            });
        })

            $('input:radio').change(function(){
                if ($(this).val()=='1')
                    $('.divs_company').show();
                else
                    $('.divs_company').hide();

            });


        $('body').on('blur', '.password2', function () {
            if ($('.password1').val()!=$('.password2').val()) {
                $('.password1').focus()
                $('#confirm_password').addClass('has-error')
                $('#message_error').show()
            }
            else {
                $('#confirm_password').removeClass('has-error')
                $('#message_error').hide();
            }


        })

    });

    $(".select_all").click(function () {
        if ($(".select_all").is(':checked')) {
            $(".days-check").prop("checked", true);
        } else {
            $(".days-check").prop("checked", false);
        }
    });
</script>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker3').datetimepicker({
            format: 'LT'
        });
    });
</script>

<?php $this->load->view("footer"); ?>
