<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

    private $country_id;

    public function __construct()
    {
        parent::__construct();
        $code = $this->session->userdata('user_address')['country_code'];
        if ($code == "EG") $this->country_id = 1;
        elseif ($code == "AE") $this->country_id = 2;
        elseif ($code == "SA") $this->country_id = 3;
        elseif ($code == "KW") $this->country_id = 4;
        else $this->country_id = 1;

        $country = @intval(htmlspecialchars(trim($_GET['country'])));
        if (!empty($country) && $country!=0)
            $this->country_id = $country;

    }


    public function get_services_by_category_id($id)
    {
        if (!empty($id)) $this->db->where('category_id', $id);
        $query = $this->db->get('services');
        return $query->result_array();
    }

    public function get_service_companies_count($id)
    {
        $this->db->where('company.comp_country_id', $this->country_id);
        $this->db->where('company_services.service_id', $id);
        $this->db->join('company_services', 'company_services.company_id = company.comp_id', 'left');
        $query = $this->db->get('company');
        return $query->num_rows();
    }

    public function get_category_companies_count($id)
    {
        $this->db->where('company.comp_country_id', $this->country_id);
        $this->db->where('company.comp_active', 1);
        $this->db->where('company.category_id', $id);
        $query = $this->db->get('company');
        return $query->num_rows();
    }

    public function get_cities_for_current_country()
    {
        $this->db->where('country_id', $this->country_id);
        $query = $this->db->get('cities');
        return $query->result_array();
    }

    public function get_districts_for_current_city($city_id)
    {
        if (!empty($city_id))
        $this->db->where('city_id', $city_id);
        $query = $this->db->get('district');
        return $query->result_array();
    }

    public function get_city_companies_count($id)
    {
        $this->db->where('comp_city_id', $id);
        $this->db->where('company.comp_active', 1);
        $query = $this->db->get('company');
        return $query->num_rows();
    }

    public function get_area_companies_count($id)
    {
        $this->db->where('comp_district_id', $id);
        $this->db->where('company.comp_active', 1);
        $query = $this->db->get('company');
        return $query->num_rows();
    }

    public function get_category_companies($id, $limit, $offset)
    {
        $this->db->where('company.comp_country_id', $this->country_id);
        $this->db->where('company.category_id', $id);
        $this->db->where('company.comp_active', 1);
        $this->db->join('district', 'district.district_id = company.comp_district_id', 'left');
        $this->db->join('cities', 'cities.city_id = company.comp_city_id', 'left');
        $query = $this->db->get('company', $limit, $offset);
        return $query->result_array();
    }


    public function get_most_rated_companies()
    {
        $sql = "SELECT DISTINCT company.*, cities.*  FROM company INNER JOIN categories ON company.category_id = categories.category_id
                    INNER JOIN cities ON company.comp_city_id = cities.city_id
                    WHERE company.comp_active = 1
                    ORDER BY comp_rate DESC limit 3";

        $query = $this->db->query($sql);
        return $query->result_array();
    }


    public function get_companies($limit, $offset, $query)
    {
        if (empty($query))
        {
            $sql = "SELECT * FROM company INNER JOIN categories ON company.category_id = categories.category_id
                    INNER JOIN cities ON company.comp_city_id = cities.city_id
                    AND company.comp_country_id = $this->country_id
                    AND company.comp_active = 1
                    ORDER BY comp_id desc limit $offset , $limit ";

        }
        else
        {
            $sql = "SELECT DISTINCT company.*";
            if (strpos($query, 'company.comp_city_id') !== false) {
                $sql .=   ", cities.*";
            }
            if (strpos($query, 'company.comp_district_id') !== false) {
                $sql .=   ", district.*";
            }
            $sql .=   " FROM company";
            if (strpos($query, 'company.category_id') !== false) {
                $sql .=   " INNER JOIN categories ON company.category_id = categories.category_id";
            }
            if (strpos($query, 'company.comp_city_id') !== false) {
                $sql .=   " INNER JOIN cities ON company.comp_city_id = cities.city_id";
            }
            if (strpos($query, 'company.comp_district_id') !== false) {
                $sql .=   " INNER JOIN district ON company.comp_district_id = district.district_id";
            }
            if (strpos($query, 'company_services.service_id') !== false) {
                $sql .=   " INNER JOIN company_services ON company.comp_id = company_services.company_id";
            }
            $sql .= "  WHERE $query 
                    AND company.comp_active = 1
                    AND company.comp_country_id = $this->country_id
                    ORDER BY comp_id desc  limit $offset , $limit ";
        }
        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_top_rated_companies($query)
    {

        if (empty($query))
        {
            $sql = "SELECT * FROM company INNER JOIN categories ON company.category_id = categories.category_id
                    INNER JOIN cities ON company.comp_city_id = cities.city_id
                    WHERE company.comp_active = 1
                    AND company.comp_country_id = $this->country_id
                    ORDER BY comp_rate desc limit 4 ";

        }
        else
        {
            $sql = "SELECT DISTINCT company.*";
            if (strpos($query, 'company.comp_city_id') !== false) {
                $sql .=   ", cities.*";
            }
            if (strpos($query, 'company.comp_district_id') !== false) {
                $sql .=   ", district.*";
            }
            $sql .=   " FROM company";
            if (strpos($query, 'company.category_id') !== false) {
                $sql .=   " INNER JOIN categories ON company.category_id = categories.category_id";
            }
            if (strpos($query, 'company.comp_city_id') !== false) {
                $sql .=   " INNER JOIN cities ON company.comp_city_id = cities.city_id";
            }
            if (strpos($query, 'company.comp_district_id') !== false) {
                $sql .=   " INNER JOIN district ON company.comp_district_id = district.district_id";
            }
            if (strpos($query, 'company_services.service_id') !== false) {
                $sql .=   " INNER JOIN company_services ON company.comp_id = company_services.company_id";
            }
            $sql .= "  WHERE $query 
                    AND company.comp_active = 1
                    AND company.comp_country_id = $this->country_id
                    ORDER BY comp_rate desc  limit 4";
        }

        $query = $this->db->query($sql);
        return $query->result_array();
    }

    public function get_search_count($query)
    {
        if (empty($query))
        {
            $sql = "SELECT  * FROM company INNER JOIN categories ON company.category_id = categories.category_id
                    INNER JOIN cities ON company.comp_city_id = cities.city_id
                    WHERE company.comp_active = 1
                    AND company.comp_country_id = $this->country_id";

        }
        else
        {
            $sql = "SELECT DISTINCT company.*";
            if (strpos($query, 'company.comp_city_id') !== false) {
                $sql .=   ", cities.*";
            }
            if (strpos($query, 'company.comp_district_id') !== false) {
                $sql .=   ", district.*";
            }
            $sql .=   " FROM company";
            if (strpos($query, 'company.category_id') !== false) {
                $sql .=   " INNER JOIN categories ON company.category_id = categories.category_id";
            }
            if (strpos($query, 'company.comp_city_id') !== false) {
                $sql .=   " INNER JOIN cities ON company.comp_city_id = cities.city_id";
            }
            if (strpos($query, 'company.comp_district_id') !== false) {
                $sql .=   " INNER JOIN district ON company.comp_district_id = district.district_id";
            }
            if (strpos($query, 'company_services.service_id') !== false) {
                $sql .=   " INNER JOIN company_services ON company.comp_id = company_services.company_id";
            }
            $sql .= "  WHERE $query 
                    AND company.comp_active = 1
                    AND company.comp_country_id = $this->country_id";

        }
        $query = $this->db->query($sql);
        return $query->num_rows();
    }

    public function get_company_by_id($company_id)
    {
        $this->db->join('district', 'district.district_id = company.comp_district_id', 'left');
        $this->db->join('cities', 'cities.city_id = company.comp_city_id', 'left');
        $this->db->join('categories', 'categories.category_id = company.category_id', 'left');
        $this->db->where('comp_id', $company_id);
        $query = $this->db->get('company');
        $users = $query->row_array();

        if (count($users) && !empty($users) && isset($users)) {
            return $users;//if user exist
        } else {

            return false;//if user not exist
        }
    }



}


/* End of file sections_model.php */
/* Location: ./application/modules/sections/models/sections_model.php */