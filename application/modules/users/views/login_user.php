<?php $this->load->view("header"); ?>

    <script>

        FB.getLoginStatus(function(response) {
            console.log(response.status);
            if (response && response.status === 'connected') {
                FB.logout(function(response) {

                });
            }
        });

        // This is called with the results from from FB.getLoginStatus().
        function statusChangeCallback(response) {
            console.log('statusChangeCallback');
            console.log(response);
            // The response object is returned with a status field that lets the
            // app know the current login status of the person.
            // Full docs on the response object can be found in the documentation
            // for FB.getLoginStatus().
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                testAPI();
            } else {
                // The person is not logged into your app or we are unable to tell.
                document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
            }
        }

        // This function is called when someone finishes with the Login
        // Button.  See the onlogin handler attached to it in the sample
        // code below.
        function checkLoginState() {
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });
        }

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '1186216541507694',
                cookie     : true,  // enable cookies to allow the server to access
                                    // the session
                xfbml      : true,  // parse social plugins on this page
                version    : 'v2.8' // use graph api version 2.8
            });

            // Now that we've initialized the JavaScript SDK, we call
            // FB.getLoginStatus().  This function gets the state of the
            // person visiting this page and can return one of three states to
            // the callback you provide.  They can be:
            //
            // 1. Logged into your app ('connected')
            // 2. Logged into Facebook, but not your app ('not_authorized')
            // 3. Not logged into Facebook and can't tell if they are logged into
            //    your app or not.
            //
            // These three cases are handled in the callback function.

            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });

        };

        // Load the SDK asynchronously
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));

        // Here we run a very simple test of the Graph API after login is
        // successful.  See statusChangeCallback() for when this call is made.

        function testAPI() {
            console.log('Welcome!  Fetching your information.... ');

            FB.login(
                function (response) {
                    if (response.authResponse) {
                        console.log('Welcome!  Fetching your information.... ');
                        FB.api('/me?fields=id,name,email,gender,first_name,last_name', function (response) {
                            console.info(response);
                            console.log('Good to see you, ' + response.email + '.');

                            $.ajax({
                                url: $(".url_class").val() + "user_facebook_login",
                                type: 'POST',
                                data: {'name': response.name, 'email': response.email, 'id': response.id, 'provider': 'facebook'},
                                success: function (data) {
                                    var return_data = JSON.parse(data);
                                    window.location = return_data.url;
                                }
                            });

                        });
                    } else {
                        console.log('User cancelled login or did not fully authorize.');
                    }
                },
                {scope: 'email'}
            );
        }

    </script>
    <input type="hidden" class="url_class" value="<?= base_url() ?>">

    <div style="padding-top: 100px;">
        <div class="container">
            <div class="row centered">
                <div class="col-md-12">
                    <div class="col-md-6 login-page">
                        <?php if (isset($error)):
                            echo $error;
                        endif;
                        ?>
                        <form class="" action="user_login" method="post">
                            <div class="user_profile clearfix">
                                <div class="col-md-12 userProfile form_data" style="width: 100%;">
                                    <h1 class="page_title_center"><i><img src="assets/img/header_wrap_icon.png"></i>تسجيل الدخول</h1>
                                    <fb:login-button scope="public_profile,email" onlogin="checkLoginState();">
                                    </fb:login-button>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="name">اسم المستخدم</label>
                                            <input name="username" class="form-control" required="" autofocus="" type="text">
                                        </div>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label for="name">كلمة المرور</label>
                                            <input name="password" class="form-control" required="" autofocus="" type="password">
                                        </div>
                                    </div>
                                    <div class="col-md-8 forget-password8">
                                        <a href=""><p class="forget-password">نسيت كلمة المرور</p></a>
                                    </div>
                                    <div class="clear"></div>
                                    <input type="submit" value="تسجيل الدخول" class="registerInput">
                                    <div class="col-md-12 new-register">
                                        <div class="col-md-6">
                                            <a href="<?= site_url() . 'user_register' ?>"><p class="forget-password">تسجيل مستخدم</p></a>
                                        </div>
                                        <div class="col-md-6">
                                            <a href="<?= site_url() . 'register'?>"><p class="forget-password">تسجيل مقدم خدمة</p></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(".removeReservation").click(function () {
            $(this).parent().parent().remove();
        });
    </script>
<?php $this->load->view("footer"); ?>