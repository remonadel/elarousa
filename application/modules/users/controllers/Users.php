<?php

defined('BASEPATH') OR exit('No direct script access allowed');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

class Users extends CI_Controller
{
    public $data = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model("Company_model");
        $this->load->model("User_model");
        $this->load->model("Company_services_m");

        $this->load->model("Company_portofolio_m");
        $this->load->model("Country_m");
        $this->load->model("City_m");
        $this->load->model("District_m");
        $this->load->model("Categories_m");
        $this->load->model("Services_m");
        $this->load->model("Reservation_m");

    }

    public function login()
    {

        $output["meta_title"] = " تسجيل دخول مقدم خدمة ";

        if ($this->session->userdata('isactive')&&$this->session->userdata('isverified')) {
            redirect(ROOT . 'profile', 'refresh');
        }
        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $username = $this->input->post("username");
            $password = $this->input->post("password");

            $validation_rules = (array(
                "username" => array(
                    "field" => "username",
                    "rules" => "xss_clean|trim|required"
                ),
                "password" => array(
                    "field" => "password",
                    "rules" => "xss_clean|trim|required"
                )
            ));

            $this->load->library("form_validation");
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $user_check = $this->Company_model->login($username, $password);
                $user_id = $this->session->userdata('userid');
                if (isset($user_id) AND $this->session->userdata('isactive') && $this->session->userdata('isverified')) {
                    redirect(ROOT . 'profile/' . $user_id, 'refresh');
                } elseif ($user_check == 2){
                    $output["error"] = "  <div class='alert alert-danger'>جاري مراجعه حسابكم و سيتم التفعيل في خلال 48 ساعه عن طريق رساله سوف تصلك علي البريد  الالكتروني الذي قمت بالتسجيل به .  من خلال الضغط علي الرابط الموجود في البريد الالكتروني حتي تتمكن من الدخول الي الموقع. </div>";
                } elseif ($user_check == 3){
                    $output["error"] = "  <div class='alert alert-danger'>يرجى تفعيل بريدكم الإكترونى من خلال رسالة التفعيل التى تم إرسالها. ومعاودة الدخول مرة آخرى. </div>";
                } elseif ($user_check == 4){
                    $output["error"] = "  <div class='alert alert-danger'> يرجى تفعيل بريدكم الإلكترونى من خلال رسالة التفعيل التى تم إرسالها. كما يجرى الآن مراجعة حسابكم وسيتم تفعيله في خلال 48 ساعة.</div>";
                } else{
                    $output["error"] = "  <div class='alert alert-danger'>يوجد خطأ باسم المستخدم أو كلمة السر أو كليهما. برجاء التحقق ومعاودة المحاولة. </div>";
                    $this->load->view('login',$output);
                }
            } else {
                $output["error"] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";
            }

        }

        $this->load->view('login',$output);
    }

    public function check_post($arr_keys, $method = "post")
    {
        $data = [];
        foreach ($arr_keys as $item) {
            $data[$item] = "";
            if ($this->input->$method($item)) {
                $data[$item] = $this->input->$method($item);
            }
        }

        return $data;
    }


    public function register()
    {
        $output["meta_title"] = " تسجيل مقدم خدمة ";

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
          //  print_r($_POST);exit;
            $save_data = $this->check_post([
                "name", "email", "tel", "social_facebook", "social_twitter",
                "social_linkedin", "comp_pinterest", "username", "password", "country_select",
                "city_select", "district_select", "category_select",
                "map_lat","days","services", "map_lng","timw1_from1","timw1_from2","timw2_from1","timw2_from2"
            ]);
            $register_name = $save_data["name"];
            $register_email = $save_data["email"];
            $register_tel = $save_data["tel"];
            $register_social_facebook = $save_data["social_facebook"];
            $register_social_twitter = $save_data["social_twitter"];
            $register_social_linkedin = $save_data["social_linkedin"];
            $register_comp_interest = $save_data["comp_pinterest"];
            $register_username = $save_data["username"];
            $register_password = $save_data["password"];
            $register_country_id = $save_data["country_select"];
            $register_city_id = $save_data["city_select"];
            $register_district_id = $save_data["district_select"];
            $register_category_id = $save_data["category_select"];
            $register_lat = $save_data["map_lat"];
            $register_lng = $save_data["map_lng"];
            $register_working_hours=json_encode([
                "days"=>$save_data["days"],
                "timw1_from1"=>$save_data["timw1_from1"],
                "timw1_from2"=>$save_data["timw1_from2"],
                "timw2_from1"=>$save_data["timw2_from1"],
                "timw2_from2"=>$save_data["timw2_from2"],

            ]);


            $validation_rules = (array(
                "email" => array(
                    "field" => "email",
                    "rules" => "xss_clean|trim|required|valid_email|is_unique[company.comp_email]"
                ),
                "register_username" => array(
                    "field" => "username",
                    "rules" => "xss_clean|trim|required|is_unique[company.username]"
                ),
                "working_days" => array(
                    "field" => "days[]",
                    "rules" => "required"
                ),
                "services" => array(
                    "field" => "services[]",
                    "rules" => "required"
                ),
                "register_password" => array(
                    "field" => "password",
                    "rules" => "xss_clean|trim|required"
                )
            ));

            $this->load->library("form_validation");
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $profie_image = '';
                if (!empty($_FILES['profile']['name'])) {
                    $profie_image = $this->Common_model->upload_files(UPLOADS_PATH . '/company_profile', "company", $_FILES['profile']);

                }

                $company_id = $this->Company_model->save(array(
                    "comp_name" => $register_name,
                    "username" => $register_username,
                    "comp_image" => $profie_image,
                    "comp_country_id" => $register_country_id,
                    "comp_city_id" => $register_city_id,
                    "comp_district_id" => $register_district_id,
                    "password" => $this->Company_model->hash($register_password),
                    "comp_email" => $register_email,
                    "comp_tel" => $register_tel,
                    "comp_facebook" => $register_social_facebook,
                    "comp_twitter" => $register_social_twitter,
                    "comp_linkedin" => $register_social_linkedin,
                    "comp_pinterest" => $register_comp_interest,
                    "working_hours" =>$register_working_hours,
                    "comp_des" => '',
                    "category_id" => $register_category_id,
                    "lat" => $register_lat,
                    "lng" => $register_lng,

                ));


                $_FILES['images']['name'] = array_diff($_FILES['images']['name'], array(""));
                if ($company_id > 0 ) {

                    // submit and then send verification

                    $random_verification_token = md5(date('Y-m-d H:i:s').$company_id.rand(0,9999));

                    // send email
                    $email_subject = " El3arousa | تفعيل بريدك الإلكترونى ";
                    $email_body = " <h1 style='text-align: center'>للتفعيل اضغط علي اللينك بالاسفل</h1> ";
                    $verification_url = ROOT .('active_my_account?email='.$register_email.'&token='.$random_verification_token);
                    $email_body .= " <a style='font-size:21px;background-color:green;color:#fff;padding:5px 5px;text-decoration: none;' href='".$verification_url."'>تفعيل </a> ";

                    $this->_send_email_to_user($email_subject,$email_body,$register_email);

                    // update company data to set verification token to him
                    $this->Company_model->update($company_id,[
                        "verification_token" => $random_verification_token,
                        "is_sent_verification" => 1,
                    ]);

                    $services = $save_data["services"];
                    $service_prices = array();
                    foreach ($services as $service){
                        $service_prices[] = $_POST['service_price_'.$service];
                    }

                    if (count($services > 0)){
                        $this->Company_model->add_company_service($company_id,$services, $service_prices);
                    }

                    if (count($_FILES['images']['name']) > 0){
                        $images = $this->Common_model->upload_files(UPLOADS_PATH . 'company_protofolio', "company", $_FILES['images']);
                        $return_id = $this->Company_portofolio_m->save($images, $company_id);
                    }
                }
                $output["error"] = "  <div class='alert alert-danger'>جاري مراجعه حسابكم و سيتم التفعيل في خلال 48 ساعه عن طريق رساله سوف تصلك علي البريد  الالكتروني الذي قمت بالتسجيل به .  من خلال الضغط علي الرابط الموجود في البريد الالكتروني حتي تتمكن من الدخول الي الموقع. </div>";
                $this->load->view('login', $output);

            } else {

                if (isset($_POST['city_select'])) $output['cities'] = $this->City_m->getCity($_POST['country_select']);
                if (isset($_POST['district_select'])) $output['districts'] = $this->District_m->getDistrict($_POST['city_select']);

                $output["error"] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";
                $output['services'] = @$this->Services_m->getServices();
                $output['countries'] = $this->Country_m->getCountry();
                $output['categories'] = $this->Categories_m->getCategories();
                $this->load->view('register_makeup_artist', $output);

            }


        } else {
            $this->data['services'] = @$this->Services_m->getServices();
            $this->data['countries'] = $this->Country_m->getCountry();
            $this->data['categories'] = $this->Categories_m->getCategories();
            $this->load->view('register_makeup_artist', $this->data);
        }
    }

    public function profile($id)
    {
        $data['comp_data'] =  $this->Company_model->get_company_by_id($id);

        if (!$id OR !$data['comp_data']) redirect(site_url());
        elseif ($data['comp_data']->comp_active == 0 AND !isset($_GET['admin'])) redirect(site_url());

        $this->data['comp_data'] = $this->Company_model->get_company_by_id($id);

        if ($this->session->userdata('userid')) {
            $this->data['user_rate'] = $this->Common_model->select_rates($id, $this->session->userdata('userid'));
        }

        $comp_services = $this->Company_services_m->getServices($id);
        if (count($comp_services))
        {
            $comp_services_ids = convert_inside_obj_to_arr($comp_services,"service_id");
            $comp_services_ids = implode(',',$comp_services_ids);
            $comp_services_data = $this->Company_services_m->getServicesData($id);
            if (count($comp_services_data))
            {
                $comp_services_data_names = convert_inside_obj_to_arr($comp_services_data,'service_name');
                $comp_services_data_names = implode(',',$comp_services_data_names);
                $this->data["meta_keywords"] = $this->data['comp_data']->comp_name . "،". $comp_services_data_names;
                $this->data["meta_description"] = $this->data['comp_data']->comp_name . " " . $this->data['comp_data']->comp_des." ".$comp_services_data_names . " " .
                    $this->data['comp_data']->category_name . " فى " . $this->data['comp_data']->city_name ;
            }
            else{
                $this->data["meta_keywords"] = $this->data['comp_data']->comp_name;
                $this->data["meta_description"] = $this->data['comp_data']->comp_name . " " . $this->data['comp_data']->category_name . " فى " .
                    $this->data['comp_data']->city_name ;
            }
            $this->data["comp_services_data"] = $comp_services_data;
        }
        $this->data['rating_avg'] = $this->average($this->Common_model->get_company_rates($id));
        $this->data["meta_title"] = $this->data['comp_data']->comp_name;
        $this->data['slider_imgs'] = $this->Company_portofolio_m->getData($this->data['comp_data']->comp_id);


        $this->data['working_hours'] = @json_decode($this->data['comp_data']->working_hours);


        $this->load->view('profile', $this->data);
    }

    private function prepareTime($time){
        $fromDayOne=1;
        $fromDayoTwo=1;
        $toDayOne=1;
        $toDayoTwo=1;
        $timeFrom=array();
        $timeTo=array();
        if (isset($time))
        {
            foreach ($time as $key=>$value)
            {
                if ($key==("day".$fromDayOne."_from1"))
                {
                    $timeFrom[]=$value;
                    $fromDayOne++;
                }
                if ($key==("day".$fromDayoTwo."_from2"))
                {
                    $timeFrom[]=$value;
                    $fromDayoTwo++;
                }
                if ($key==("day".$toDayOne."_to1"))
                {
                    $timeTo[]=$value;
                    $toDayOne++;
                }
                if ($key==("day".$toDayoTwo."_to2"))
                {
                    $timeTo[]=$value;
                    $toDayoTwo++;
                }
            }
            $timeCompany=array();
            for( $i = 0; $i<14; $i++ ) {
                if (!empty($timeFrom[$i])){
                    $timeCompany[]=$timeFrom[$i]." الى ".$timeTo[$i];
                }
                else{
                    $timeCompany[]="";
                }
            }

        }
        else
        {
            $timeCompany = array();
        }

        return $timeCompany;
    }

    public function register_user()
    {

        $output["meta_title"] = " تسجيل مستخدم ";

        $this->data["meta_title"] = $output["meta_title"];

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $save_data = $this->check_post([
                "name", "email", "tel", "social_facebook", "social_twitter",
                "social_linkedin", "username", "password", "country_select",
                "city_select", "district_select"
            ]);
            $register_name = $save_data["name"];
            $register_email = $save_data["email"];
            $register_tel = $save_data["tel"];
            $register_social_facebook = $save_data["social_facebook"];
            $register_social_twitter = $save_data["social_twitter"];
            $register_social_linkedin = $save_data["social_linkedin"];
            $register_username = $save_data["username"];
            $register_password = $save_data["password"];
            $register_country_id = $save_data["country_select"];
            $register_city_id = $save_data["city_select"];
            $register_district_id = $save_data["district_select"];


            $validation_rules = (array(
                "email" => array(
                    "field" => "email",
                    "rules" => "xss_clean|trim|required|valid_email|is_unique[users.user_email]"
                ),
                "register_username" => array(
                    "field" => "username",
                    "rules" => "xss_clean|trim|required|is_unique[users.username]"
                ),
                "register_password" => array(
                    "field" => "password",
                    "rules" => "xss_clean|trim|required"
                )
            ));

            $this->load->library("form_validation");
            $this->form_validation->set_rules($validation_rules);

            if ($this->form_validation->run()) {
                $profie_image = '';
                if (!empty($_FILES['profile']['name'])) {
                    $profie_image = $this->Common_model->upload_files(UPLOADS_PATH . 'user_profile', "user", $_FILES['profile']);

                }

                $random_verification_token = md5(date('Y-m-d H:i:s').rand(0,1239999));

                // send email
                $email_subject = " تفعيل موقع عروسه ";
                $email_body = " <h1 style='text-align: center'>للتفعيل اضغط علي اللينك بالاسفل</h1> ";
                $verification_url = ROOT .('active_user_account?email='.$register_email.'&token='.$random_verification_token);
                $email_body .= " <a style='font-size:21px;background-color:green;color:#fff;padding:5px 5px;text-decoration: none;' href='".$verification_url."'>تفعيل </a> ";

                $this->_send_email_to_user($email_subject,$email_body,$register_email);


                $user_id = $this->User_model->save(array(
                    "user_name" => $register_name,
                    "username" => $register_username,
                    "user_image" => $profie_image,
                    "user_country_id" => $register_country_id,
                    "user_city_id" => $register_city_id,
                    "user_district_id" => $register_district_id,
                    "password" => $this->User_model->hash($register_password),
                    "user_email" => $register_email,
                    "user_tel" => $register_tel,
                    "user_facebook" => $register_social_facebook,
                    "user_twitter" => $register_social_twitter,
                    "user_linkedin" => $register_social_linkedin,
                    "verification_token" => $random_verification_token,
                    "is_sent_verification" => 1,
                ));

                $output["error"] = "<div class='alert alert-danger'> برجاء متابعة بريدك وتفعيل حسابك حتى تتمكن من الدخول></div>";
                $this->load->view('login_user');

            } else {
                $output["error"] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";
                $this->load->view('register_user', $output);

            }


        } else {
            $this->data['countries'] = $this->Country_m->getCountry();
            $this->load->view('register_user', $this->data);
        }
    }

    public function login_user()
    {

        $output["meta_title"] = " تسجيل دخول مستخدم ";

        if ($this->session->userdata('userid')) {
            redirect(site_url());
        }

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $username = $this->input->post("username");
            $password = $this->input->post("password");

            $validation_rules = (array(
                "username" => array(
                    "field" => "username",
                    "rules" => "xss_clean|trim|required"
                ),
                "password" => array(
                    "field" => "password",
                    "rules" => "xss_clean|trim|required"
                )
            ));

            $this->load->library("form_validation");
            $this->form_validation->set_rules($validation_rules);
            if ($this->form_validation->run()) {
                $user_check = $this->User_model->login($username, $password);
                $user_id = $this->session->userdata('userid');
                if (isset($user_id)) {
                    redirect(site_url());
                }
                $output["error"] = "<div class='alert alert-danger'>خطأ في الايميل او كلمة السر برجاء تسجيل الاشتراك اذا كنت عضو جديد</div>";
                $this->load->view('login_user', $output);
            } else {
                $output["error"] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";
                $this->load->view('login_user', $output);
            }

        } else {


            $this->load->view('login_user' , $output);
        }
    }

    public function user_facebook_login()
    {
        $output = [];


        if (is_array($_POST) && count($_POST))
        {

            $user = $_POST;

            $login_email = $user['email']; // facebook id
            $login_name = $user['name'];
            $login_fb_id = $user['id'];

            $user_check = $this->User_model->get_user_with_conditions("
                    where login_provider = 'facebook'
                    and facebook_id = '$login_fb_id'
                ");

            if (isset($user_check) && is_object($user_check) && !empty($user_check)) {

                // set login
                $user_id = $user_check->user_id;
            }
            else{

                // add it and set login

                $user_id = $this->User_model->save( array(
                    "user_name" => $login_name,
                    "user_email" => $login_email ,
                    "facebook_id" => $login_fb_id ,
                    "login_provider" => 'facebook' ,
                    "is_sent_verification" => 1 ,
                    "is_verified" => 1
                ));

                $user_check = $this->User_model->get_user_with_conditions("
                        where user_id = $user_id
                    ");

            }

            $session_data = array(
                'userid' => $user_id,
                'name' => $user_check->user_name,
                'loggedin' => TRUE,
                'user_type' => 'user',
                'provider' => 'facebook'
            );
            $this->session->set_userdata($session_data);

        }

        $output["url"] = site_url();
        echo json_encode($output);
        return;

    }

    private function average($avr_array)
    {
        if($avr_array)
        {
            $rate = array();
            foreach($avr_array as $value)
            {
                foreach($value as $key => $number)
                {
                    (!isset($res[$key])) ? $res[$key] = $number : $res[$key] += $number;
                }
            }
            $average =  round($res['rate'] / count($avr_array));
            return "$average";
        }
        else
        {
            return "0";
        }
    }

    public function insert_rates()
    {
        $user_id = $this->session->userdata('userid');
        $company_id = $this->input->post('company_id');
        $rate = $this->input->post('rate');
        $select_rates = $this->Common_model->select_rates($company_id, $user_id);
        if($select_rates >= 1)
        {
            $this->Common_model->update_rates($company_id, $user_id, $rate);
            $rating_avg = $this->average($this->Common_model->get_company_rates($company_id));
            $this->Common_model->update_total_rate($company_id, $rating_avg);
            echo "update";
        }
        else
        {

            $this->Common_model->insert_into_rates($company_id, $user_id, $rate);
            $rating_avg = $this->average($this->Common_model->get_company_rates($company_id));
            $this->Common_model->update_total_rate($company_id, $rating_avg);
            echo "insert";
        }

    }


    public function company_facebook_login()
    {
        $output = [];


        if (is_array($_POST) && count($_POST))
        {

            $user = $_POST;

            $login_email = $user['email']; // facebook id
            $login_name = $user['name'];
            $login_fb_id = $user['id'];

            $user_check = $this->Company_model->get_company_with_conditions("
                    where login_provider = 'facebook'
                    and facebook_id = '$login_fb_id'
                ");


            if (isset($user_check) && is_object($user_check) && !empty($user_check)) {

                // set login
                $comp_id = $user_check->comp_id;
            }
            else{

                // add it and set login

                $comp_id = $this->Company_model->save( array(
                    "comp_name" => $login_name,
                    "comp_email" => $login_email ,
                    "facebook_id" => $login_fb_id ,
                    "login_provider" => 'facebook' ,
                    "is_sent_verification" => 1 ,
                    "is_verified" => 1,
                    "comp_active" => 1,
                ));

                $user_check = $this->Company_model->get_company_with_conditions("
                        where comp_id = $comp_id
                    ");

            }

            $session_data = array(
                'userid' => $comp_id,
                'name' => $user_check->comp_name,
                'loggedin' => TRUE,
                'user_type' => 'company',
                'provider' => 'facebook'
            );
            $this->session->set_userdata($session_data);

        }

        $output["url"] = site_url() . 'edit_profile_comp';
        echo json_encode($output);
        return;

    }

    public function logout()
    {
        $this->session->sess_destroy();
        redirect(base_url());

    }

    public function get_comp_reservations()
    {

        $user_type = $this->session->userdata("user_type");
        if($user_type != "company")
        {
            redirect(ROOT , 'refresh');
        }

        $comp_data = $this->Company_model->get_user_company();
        $this->data['comp_data'] = $comp_data;

        $this->data["reservations"] = $this->Reservation_m->getCompanyReservations(" 
            where reserve.comp_id = $comp_data->comp_id
            order by reserve.created_at desc
         ");

        $this->load->view('comp_reservations', $this->data);
    }

    public function user_reservations()
    {

        $user_type = $this->session->userdata("user_type");
        if($user_type == "company")
        {
            redirect(ROOT , 'refresh');
        }

        $this->data["reservations"] = $this->Reservation_m->getCompanyReservations(" 
            where reserve.user_id = ".$this->session->userdata('userid')."
            order by reserve.created_at desc
         ");

        $this->load->view('user_reservations', $this->data);
    }

    public function remove_reservation()
    {

        if ($this->input->server('REQUEST_METHOD') == 'POST')
        {

            $output["success"] = "";

            $res_id = $_POST['id'];
            $check = $this->Reservation_m->getCompanyReservations(" 
                where reserve.res_id = $res_id
             ");
            if (!count($check))
            {
                $output["success"] = "هذا الحجز غير موجود !!";
                echo json_encode($output);
                return;
            }

            $this->Reservation_m->delete($res_id);
            $output["success"] = "success";
            echo json_encode($output);
            return;
        }

    }

    public function edit_profile()
    {

        $user_type = $this->session->userdata("user_type");
        if($user_type != "company")
        {
            redirect(ROOT , 'refresh');
        }

        $comp_data = $this->Company_model->get_user_company();
        $this->data['comp_data'] = $comp_data;

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $save_data = $this->check_post([
                "comp_name", "comp_email", "comp_tel", "comp_facebook", "comp_twitter",
                "comp_linkedin","services","old_comp_slider","comp_pinterest"
//                , "username", "password"
                , "comp_country_id","city_select", "district_select"
                , "category_id","days","timw1_from1","timw1_from2","timw2_from1","timw2_from2"
            ]);

            $save_data["working_hours"]=json_encode([
                "days"=>$save_data["days"],
                "timw1_from1"=>$save_data["timw1_from1"],
                "timw1_from2"=>$save_data["timw1_from2"],
                "timw2_from1"=>$save_data["timw2_from1"],
                "timw2_from2"=>$save_data["timw2_from2"],

            ]);

            unset($save_data["days"]);
            unset($save_data["timw1_from1"]);
            unset($save_data["timw1_from2"]);
            unset($save_data["timw2_from1"]);
            unset($save_data["timw2_from2"]);
            $services = $save_data["services"];

            if (isset($save_data["city_select"]))
            {
                $save_data["comp_city_id"] = $save_data["city_select"];
                unset($save_data["city_select"]);
            }
            if (isset($save_data["district_select"]))
            {
                $save_data["comp_district_id"] = $save_data["district_select"];
                unset($save_data["district_select"]);
            }

            $validation_rules = (array(
                "working_days" => array(
                    "field" => "days[]",
                    "rules" => "required"
                ),
                "services" => array(
                    "field" => "services[]",
                    "rules" => "required"
                ),
                "mobile" => array(
                    "field" => "comp_tel",
                    "rules" => "xss_clean|trim|required"
                )
            ));

            $this->load->library("form_validation");
            $this->form_validation->set_rules($validation_rules);

            $service_prices = array();
            if (isset($services) && is_array($services)) {
                foreach ($services as $service){
                    $service_prices[] = $_POST['service_price_'.$service];
                }
            }

            // check if $services not empty array
            if ($this->form_validation->run()) {

               // just edit data
                $this->__update_basic_info($comp_data, $services, $service_prices, $save_data);
                $this->data["error"] = "<div class='alert alert-danger'> تم تحديث بياناتكم بنجاح</div>";
            }
            else{
                $this->data["error"] = "<div class='alert alert-danger'>" . validation_errors() . "</div>";
            }


        }


        $this->data['slider_imgs'] = $this->Company_portofolio_m->getData($comp_data->comp_id);
        $this->data['services'] = @$this->Services_m->getServices();
        $comp_services = @$this->Company_services_m->getServices($comp_data->comp_id);
        $comp_services_prices = array();
        foreach ($comp_services as $comp_service){
            $comp_services_prices[$comp_service->service_id] = $comp_service->service_price;
        }
        $this->data['comp_services'] = $comp_services;
        $this->data['comp_services_prices'] = $comp_services_prices;
        @$this->Company_services_m->delete_company_services($this->data['comp_services']);
        $this->data['countries'] = $this->Country_m->getCountry();
        $this->data['cities'] = $this->City_m->getCity($comp_data->comp_country_id);
        $this->data['districts'] = $this->District_m->getDistrict($comp_data->comp_city_id);
        $this->data['categories'] = $this->Categories_m->getCategories();


        if (isset($this->data['comp_services'])) {
            $id_company_service = array();
            foreach ($this->data['comp_services'] as $key => $value) {
                $id_company_service[] = $value->service_id;
            }
        }
        $this->data['comp_services_id'] = $id_company_service;
        $this->load->view('dashboard_edit', $this->data);

    }

    public function edit_working_hours()
    {
        $data = array();
        $company = $this->Company_model->get_user_company();
        $data['working_hours'] = @json_decode($company->working_hours);
        if (isset($_POST['submit']))
        {
            $working_hours = json_encode($_POST);
            $this->Company_model->update($this->session->userdata('userid'), ['working_hours' => $working_hours]);
            redirect(site_url() . 'edit_working_hours');
        }
        $this->load->view('edit_working_hours_view', $data);
    }

    private function __update_basic_info($comp_data, $services, $service_prices, $save_data)
    {

        $this->data['slider_imgs'] = $this->Company_portofolio_m->getData($comp_data->comp_id);

        // upload company slider if uploaded

        $final_comp_slider = [];

        // at first get old slider imsg if exist
        if (!empty($slider_imgs) && count($slider_imgs))
        {
            $final_comp_slider = array_merge($final_comp_slider,$slider_imgs);
        }

        if (isset($save_data['old_comp_slider']))
        {
            unset($save_data['old_comp_slider']);
        }

        // second upload new imgs if exist
        $comp_slider = $this->cms_upload($user_id=0,$files = 'comp_slider',COMPANY_PROTOFOLIO_PATH,$ext_arr=array(),$return_only_name=true,$absoulte_upload_path="");
        if (!empty($comp_slider) && count($comp_slider))
        {
            $final_comp_slider = array_merge($final_comp_slider,$comp_slider);
        }

        // save on Company_portofolio_m
        if (count($final_comp_slider))
        {
            foreach($final_comp_slider as $key => $slider_img)
            {
                $this->Company_portofolio_m->save_new([
                    'img_name' => $slider_img,
                    'comp_id' => $comp_data->comp_id
                ]);
            }
        }


        // update company profile if uploaded
        $profile_img = $this->cms_upload($user_id=0,$files = 'profile_img',COMPANY_PROFILE_PATH,$ext_arr=array(),$return_only_name=true,$absoulte_upload_path="");

        if (!empty($profile_img))
        {
            $img=$this->Company_model->getImage($this->session->userdata('userid'));
            $save_data["comp_image"] = "$profile_img[0]";
        }

        // edit his service on company_services

        // first delete old if exist
        $this->Company_services_m->delete_old_company_services($comp_data->comp_id);

        // add new updated services from request

        if (is_array($services) && !empty($services)){
            foreach($services as $key => $service_id)
            {
                $this->Company_services_m->save([
                    "company_id" => $comp_data->comp_id,
                    "service_id" => intval($service_id),
                    "service_price" => @intval($service_prices[$key])
                ]);
            }
        }


        // edit basic data for user
        unset($save_data["services"]);
        $this->Company_model->update($comp_data->comp_id,$save_data);
        unlink(COMPANY_PROFILE_PATH. $img[0]->comp_image);

    }

    public function delete_protofolio_img()
    {
        if ($this->input->server('REQUEST_METHOD') == 'POST') {

            $img_id = intval($_POST['img_id']);
            $img=$this->Company_portofolio_m->getImage($img_id);
            unlink(COMPANY_PROTOFOLIO_PATH. $img[0]->img_name);
            if ($img_id > 0)
            {
                $this->Company_portofolio_m->delete($img_id);
            }

        }
    }

    public function edit_profile_user()
    {

        $user_type = $this->session->userdata("user_type");
        if($user_type != "user")
        {
            redirect(ROOT , 'refresh');
        }

        $user_data = $this->User_model->get_user_data();
        $this->data['user_data'] = $user_data;

        if ($this->input->server('REQUEST_METHOD') == 'POST') {
            $save_data = $this->check_post([
                "user_name", "user_email", "user_tel", "user_facebook", "user_twitter",
                "user_linkedin"
//                , "username", "password"
                , "user_country_id","city_select", "district_select"

            ]);


            if (isset($save_data["city_select"]))
            {
                $save_data["user_city_id"] = $save_data["city_select"];
                unset($save_data["city_select"]);
            }
            if (isset($save_data["district_select"]))
            {
                $save_data["user_district_id"] = $save_data["district_select"];
                unset($save_data["district_select"]);
            }
//            var_dump($save_data);
//            die();

            $validation_rules = (array(
                "edit_email" => array(
                    "field" => "email",
                    "rules" => "xss_clean|trim|required|valid_email|is_unique[users.user_email]"
                )
            ));

            $this->load->library("form_validation");
            $this->form_validation->set_rules($validation_rules);


            // just edit data

            $this->__update_user_basic_info($user_data, $save_data);

            redirect(ROOT , 'refresh');

        }



        $this->data['countries'] = $this->Country_m->getCountry();

        $this->load->view('user_profile_edit', $this->data);

    }


    private function __update_user_basic_info($user_data, $save_data)
    {

        // update user profile if uploaded
        $profile_img = $this->cms_upload($user_id=0,$files = 'profile_img','uploads/user_profile',$ext_arr=array(),$return_only_name=true,$absoulte_upload_path="");

        if (!empty($profile_img))
        {
            $save_data["user_image"] = "$profile_img[0]";
        }

        // edit his service on company_services
        $this->User_model->update($user_data->user_id,$save_data);
    }


    public function active_my_account()
    {

        if (count($_GET) && isset($_GET["email"])  && isset($_GET["token"]))
        {
            $email = $_GET["email"];
            $token = $_GET["token"];
//            var_dump($_GET);

            $comp_data = $this->Company_model->get_company_with_conditions( " 
                where comp_email = '$email' AND verification_token = '$token'
            ");

            if ($comp_data == false)
            {
                redirect(ROOT, 'refresh');
            }

            $this->Company_model->update($comp_data->comp_id,[
                "is_verified" => 1
            ]);

            // send email
            $email_subject = " تفعيل موقع عروسه ";
            $email_body = " شكرا لك لقد تم تفعيل الاكونت الخاص بك :) ";

            $this->_send_email_to_user($email_subject,$email_body,$comp_data->comp_email);

            redirect(ROOT, 'refresh');

        }
        else{
            redirect(ROOT, 'refresh');
        }
    }

    public function active_user_account()
    {

        if (count($_GET) && isset($_GET["email"])  && isset($_GET["token"]))
        {
            $email = $_GET["email"];
            $token = $_GET["token"];
//            var_dump($_GET);

            $user_data = $this->User_model->get_user_with_conditions( " 
                where user_email = '$email' AND verification_token = '$token'
            ");



            if ($user_data == false)
            {
                redirect(ROOT, 'refresh');
            }

            $this->User_model->update($user_data->user_id,[
                "is_verified" => 1
            ]);


            // send email
            $email_subject = " تفعيل موقع عروسه ";
            $email_body = " شكرا لك لقد تم تفعيل الاكونت الخاص بك :) ";

            $this->_send_email_to_user($email_subject,$email_body,$user_data->user_email);

            redirect(ROOT, 'refresh');

        }
        else{
            redirect(ROOT, 'refresh');
        }
    }

    private function _send_email_to_user($subject,$body,$email) {
        $to      = $email;
        $message = $body;
        $headers = 'From: noreply@el3arousa.com' . "\r\n" .
            'Reply-To: noreply@el3arousa.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($to, $subject, $message, $headers);
    }

    function cms_upload($user_id=0,$files,$folder,$ext_arr=array(),$return_only_name=false,$absoulte_upload_path="")
    {
        $uploaded=array();
        //check if folder_exist
        $CI=&get_instance();

        //upload
        if (!empty($_FILES[$files])) {
            $_FILES[$files]['name']=array_diff($_FILES[$files]['name'], array(""));
            foreach ($_FILES[$files]['name'] as $key => $name) {

                $ext = pathinfo($name, PATHINFO_EXTENSION);
                if (in_array($ext, array("jpeg","png","jpg","JPEG","PNG","JPG"))||(count($ext_arr)>0&&  in_array($ext, $ext_arr))) {
                    //encrypt $name
                    $name=md5($name."random_strings2017".date('Y-m-d H:i:s').$user_id.$key.time()).".".$ext;
                    if ($_FILES[$files]['error'][$key]==0&&move_uploaded_file($_FILES[$files]['tmp_name'][$key],$folder.'/'.$name)) {
                        if ($return_only_name==false) {
                            $uploaded[]= "$folder/".$name;
                        }else{
                            $uploaded[]= $name;
                        }
                    }
                    else
                    {
                        $uploaded[]=$_FILES[$files]['error'][$key];
                    }
                }
                else
                {
                    return "not allowed type";
                }
            }//end foreach

        }
        else{
            $error_msg="No files are selected";
        }
        if (!empty($error_msg)) {
            return $error_msg;
        }
        return $uploaded;
    }

}